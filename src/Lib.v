From Coq Require Import String List Reals Basics.
From Qunity Require Import MapsAux Syntax Types Tuples.
Import ListNotations.

Open Scope string_scope.
Open Scope qunit_scope.

Definition x := "x". Opaque x.
Definition xs := "xs". Opaque xs.
Definition y := "y". Opaque y.

(* Identity function (expression) *)
Definition qid := \ x ->> x.

Lemma has_type_qid :
  forall T a, |-- qid : T ->> (a) T.
Proof.
  intros T a. apply has_fun_type_abs; eauto using pat, has_type_var.
Qed.

(* A convenient way to convert gates to functional gate application *)
Definition app_gate := flip expr_gate.

Lemma has_type_app_gate :
  forall a g, |-- app_gate g : Bit ->> (a) Bit.
Proof.
  intros a g Gamma e He. eauto using has_type.
Qed.

(* Pauli-X gate application *)
Definition pauli_x :=
  app_gate (gate_u PI 0 PI).
(* Pauli-Y gate application *)
Definition pauli_y :=
  app_gate (gate_u PI (PI / 2) (PI / 2)).
(* Pauli-Z gate application *)
Definition pauli_z :=
  app_gate (gate_u 0 0 PI).

(* Hadamard gate application *)
Definition hadamard :=
  app_gate (gate_u (PI / 2) 0 PI).

(* Retrieve the first element of a (quantum) pair *)
Definition first := \ (| x, y |) ->> x.

Lemma has_type_first :
  forall T1 T2, |-- first : T1 <*> T2 ->> (mixed) T1.
Proof.
  intros T1 T2. apply has_fun_type_abs; repeat constructor.
  intros x' Hx Hy. inversion Hx. inversion Hy. subst. discriminate.
Qed.

(* Retrieve the second element of a (quantum) pair *)
Definition second := \ (| x, y |) ->> y.

Lemma has_type_second :
  forall T1 T2, |-- second : T1 <*> T2 ->> (mixed) T2.
Proof.
  intros T1 T2. apply has_fun_type_abs; repeat constructor.
  intros x' Hx Hy. inversion Hx. inversion Hy. subst. discriminate.
Qed.

(* Swap the two elements of a (quantum) pair *)
Definition swap := \ (| x, y |) ->> (| y, x |).

Lemma has_type_swap :
  forall T1 T2, |-- swap : T1 <*> T2 ->> (pure) (T2 <*> T1).
Proof.
  intros T1 T2. apply has_fun_type_abs; repeat constructor.
  - intros x' Hx Hy. inversion Hx. inversion Hy. subst. discriminate.
  - simpl. rewrite <- map_union_update.
    econstructor; eauto using has_type.
    intros x' v v' Hy Hx. apply update_Some_eq in Hy as [Hy _], Hx as [Hx _].
    subst. discriminate.
Qed.

(* Apply f0 or f1 to y, depending on x *)
Definition qmux (f0 f1 : expr -> expr) :=
  \ (| x, y |) ->> qmatch x with
                   | #0 =>> (| #0, f0 y |)
                   | #1 =>> (| #1, f1 y |).

Lemma has_type_qmux :
  forall f0 f1 T T' a,
  |-- f0 : T ->> (pure) T' ->
  |-- f1 : T ->> (pure) T' ->
  |-- qmux f0 f1 : Bit <*> T ->> (a) (Bit <*> T').
Proof.
  intros f0 f1 T T' a H0 H1. apply has_fun_type_abs; repeat constructor.
  - intros x' Hx Hy. inversion Hx. inversion Hy. subst. discriminate.
  - simpl. rewrite <- map_union_update_2; auto.
    eapply has_type_match; eauto using pat, ForallOrdPairs, ortho, has_type_var.
    + intros x' v v' Hx Hy.
      apply update_Some_eq in Hx as [Hx _].
      apply update_Some_eq in Hy as [Hy _].
      subst. discriminate.
    + intros x'. repeat constructor; intros Hc; inversion Hc; inversion H2.
    + repeat constructor; simpl.
      * replace (y |-> T) with (map_union empty (y |-> T));
        eauto using map_union_empty_1, has_type, no_conflict_empty_1.
      * replace (y |-> T) with (map_union empty (y |-> T));
        eauto using map_union_empty_1, has_type, no_conflict_empty_1.
Qed.

(* A "controlled operation" as they are typically defined in circuits *)
Definition ctrl f := qmux qid f.

Lemma has_type_ctrl :
  forall f T a,
  |-- f : T ->> (pure) T ->
  |-- ctrl f : Bit <*> T ->> (a) (Bit <*> T).
Proof.
  intros f T a H. apply has_type_qmux; auto using has_type_qid.
Qed.

(* A CNOT (controlled not) gate application *)
Definition cnot := ctrl pauli_x.

Lemma has_type_cnot :
  |-- cnot : Bit <*> Bit ->> (pure) (Bit <*> Bit).
Proof. apply has_type_ctrl, has_type_app_gate. Qed.

Notation "#|+>" := (hadamard #0).

Lemma has_type_plus_pure :
  forall a, |- #|+> : (a) Bit.
Proof.
  intros []; eauto using has_type.
Qed.

Lemma has_type_plus_mixed :
  forall Gamma, Gamma |- #|+> : (mixed) Bit.
Proof. eauto using has_type. Qed.

Lemma has_type_plus :
  forall Gamma a T,
  Gamma |- #|+> : (a) T <-> T = Bit /\ (Gamma = empty \/ a = mixed).
Proof.
  intros Gamma a T. split.
  - intros H. inversion H. inversion H5. inversion H10; subst; auto.
  - intros [Ht [Hg | Ha]]; subst; eauto using has_type, has_type_plus_pure.
Qed.

Notation "#|->" := (hadamard #1).

Lemma has_type_minus_pure :
  forall a, |- #|-> : (a) Bit.
Proof.
  intros []; eauto using has_type.
Qed.

Lemma has_type_minus_mixed :
  forall Gamma, Gamma |- #|-> : (mixed) Bit.
Proof. eauto using has_type. Qed.

Lemma has_type_minus :
  forall Gamma a T,
  Gamma |- #|-> : (a) T <-> T = Bit /\ (Gamma = empty \/ a = mixed).
Proof.
  intros Gamma a T. split.
  - intros H. inversion H. inversion H5. inversion H10; subst; auto.
  - intros [Ht [Hg | Ha]]; subst; eauto using has_type, has_type_minus_pure.
Qed.

(* "Duplicate" anything via entanglement *)
Definition dup := \ x ->> (| x, x |).

Lemma has_type_dup :
  forall T a, |-- dup : T ->> (a) (T <*> T).
Proof.
  intros T a. apply has_fun_type_abs; auto using pat.
  simpl.
  replace (x |-> T) with (map_union (x |-> T) (x |-> T))
                    by apply map_union_idempotent.
  apply has_type_pair; auto using has_type_var.
  intros x' T1 T2 H1 H2.
  apply update_Some_eq in H1 as [_ H1], H2 as [_ H2]. congruence.
Qed.

(* Convenient syntax for quantum tuples *)
Definition qcons := expr_pair.

Infix "::" := qcons.

(* A traditional functional "map" function in the quantum setting *)
Fixpoint qmap f n :=
  match n with
  | 0 =>
      \ () ->> ()
  | S n' =>
      \ x :: xs ->> f x :: qmap f n' xs
  end.

Lemma has_type_qmap :
  forall f T T' n a,
  |-- f : T ->> (a) T' ->
  |-- qmap f n : tuple T n ->> (a) tuple T' n.
Proof.
  intros f T T' n a H.
  induction n as [| n IH]; simpl; apply has_fun_type_abs; simpl; auto using pat.
  - apply has_type_unit.
  - repeat constructor. intros x' Hx Hs. inversion Hx. inversion Hs.
    subst. discriminate.
  - rewrite <- map_union_update_2; auto.
    apply has_type_pair; auto using has_type_var.
    intros x' T1 T2 H1 H2. apply update_Some_eq in H1 as [H1 _], H2 as [H2 _].
    subst. discriminate.
Qed.
