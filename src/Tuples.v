From Qunity Require Import Syntax.

(* Tuple types *)

(* Tuple types defined in terms of other types *)
Fixpoint tuple T n :=
  match n with
  | 0 => Unit
  | S n' => T <*> tuple T n'
  end.
