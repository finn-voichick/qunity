From Coq Require Import Basics Bool Psatz.
From Coquelicot Require Export Complex.
From Qunity Require Import BoolAux ListAux RealAux.
From VFA Require Import Perm.
Import Complex (C).
Import BoolAux (eqb_refl).

Set Bullet Behavior "Strict Subproofs".
Set Implicit Arguments.

(* Useful properties involving complex numbers *)

Open Scope R_scope.
Open Scope C_scope.

Hint Rewrite <- RtoC_plus RtoC_mult RtoC_minus RtoC_opp : csimpl.
Hint Rewrite 
  Cplus_assoc Cplus_0_l Cplus_0_r Cplus_opp_r Cmult_1_l Cmult_1_r
  Cmult_0_l Cmult_0_r Cmult_assoc Cmult_plus_distr_l Cmult_plus_distr_r
  Cmod_0 Cmod_1 :
  csimpl.

(* Simplify an expression involving complex numbers *)
Ltac csimpl :=
  repeat (autorewrite with csimpl in *; rsimpl).

(* Whether a complex number represents a real *)
Inductive is_real : C -> Prop :=
  is_real_RtoC (a : R) : is_real a.

Lemma RtoC_iff :
  forall a b, RtoC a = RtoC b <-> a = b.
Proof.
  intros a b. split.
  - apply RtoC_inj.
  - intros H. subst. reflexivity.
Qed.

Lemma real_iff :
  forall z, is_real z <-> z = Re z.
Proof.
  intros z. split.
  - intros []. reflexivity.
  - intros H. rewrite H. constructor.
Qed.

Lemma real_iff_im_0 :
  forall z, is_real z <-> Im z = 0.
Proof.
  intros [a b]. simpl. split; intros H.
  - inversion H. reflexivity.
  - subst. constructor.
Qed.

Lemma real_iff_conj :
  forall z, is_real z <-> z = Cconj z.
Proof.
  intros [a b]. unfold Cconj. simpl. split.
  - intros H. inversion_clear H. csimpl.
  - intros H. injection H as. rsimpl. subst. constructor.
Qed.

Lemma real_plus :
  forall z z', is_real z -> is_real z' -> is_real (z + z').
Proof.
  intros z z' H H'. inversion_clear H. inversion_clear H'. csimpl.
Qed.

(*
Definition nonnegative_c z :=
  is_real z /\ 0 <= Re z.

Lemma nonnegative_plus :
  forall z z', nonnegative_c z -> nonnegative_c z' -> nonnegative_c (z + z').
Proof.
  intros z z' [Hr Hp] [Hr' Hp']. inversion Hr. inversion Hr'. subst.
  split; csimpl.
  lra.
Qed.
 *)

(* true if z = z', otherwise false *)
Definition eqb_C z z' := if Ceq_dec z z' then true else false.

Infix "=?" := eqb_C (at level 70) : C_scope.

Lemma eqb_C_correct :
  reflect2 eq eqb_C.
Proof.
  intros z z'. unfold eqb_C. destruct (Ceq_dec z z'); auto using reflect.
Qed.

#[export]
Hint Resolve eqb_C_correct : bdestruct.

Lemma eqb_C_equiv :
  eqb_equiv eqb_C.
Proof.
  eapply Equivalence_eqb_equiv.
  - apply eqb_C_correct.
  - apply eq_equivalence.
Qed.

Lemma cmult_copp :
  forall z z', -z * z' = -(z * z').
Proof.
  intros [a b] [a' b']. unfold Copp, Cmult. rsimpl.
Qed.

Lemma conj_real :
  forall a : R, Cconj a = a.
Proof.
  intros a. unfold Cconj. csimpl.
Qed.

Lemma conj_idemp :
  forall z, Cconj (Cconj z) = z.
Proof.
  intros [a b]. unfold Cconj. csimpl.
Qed.

Lemma conj_additive :
  forall z z', Cconj (z + z') = Cconj z + Cconj z'.
Proof.
  intros [a b] [a' b']. unfold Cconj. csimpl.
Qed.

Lemma conj_mult_distr :
  forall z z', Cconj (z * z') = Cconj z * Cconj z'.
Proof.
  intros [a b] [a' b']. unfold Cconj, Cmult. csimpl.
Qed.

Hint Rewrite
  RtoC_iff cmult_copp conj_real conj_idemp
  conj_additive conj_mult_distr :
  csimpl.

Lemma Cmult_neg_1 :
  forall z, -1 * z = -z.
Proof.
  intros z. rewrite IZR_NEG, RtoC_opp. csimpl.
Qed.

Lemma Cmod_correct :
  forall z, Cmod z = sqrt (Re (Cconj z * z)%C).
Proof.
  intros [a b]. unfold Cmod, Cconj. csimpl.
Qed.

Lemma conj_mod_squared :
  forall z, (Cconj z * z)%C = Rsqr (Cmod z).
Proof.
  intros [a b]. unfold Cconj, Rsqr, Cmod, Cmult. csimpl.
  rewrite sqrt_sqrt; try nra. csimpl.
  replace (b * a)%R with (a * b)%R by lra. csimpl.
Qed.

Lemma cplus_all_real_l :
  forall z (a b : R), a = z + b :> C -> a = (Re z + b)%R.
Proof.
  unfold Cplus. intros [z1 z2] a b H. csimpl. injection H as H1 H2. assumption.
Qed.

Lemma cplus_all_real_r :
  forall z (a b : R), a = b + z :> C -> a = (b + Re z)%R.
Proof.
  unfold Cplus. intros [z1 z2] a b H. csimpl. injection H as H1 H2. assumption.
Qed.

Section set.

  Variable A : Type.

  (* Sum complex numbers over f(e) for all e in b *)
  Fixpoint csum f (b : set A) : C :=
    match b with
    | [] => 0
    | e :: b' => f e + csum f b'
    end.

  Lemma rsum_csum :
    forall (f : A -> R) b,
    csum f b = rsum f b.
  Proof.
    intros f b. induction b as [| e b IH]; csimpl.
    rewrite IH. csimpl.
  Qed.

  Lemma csum_rsum :
    forall (f : A -> C) b,
    Forall (compose is_real f) b ->
    csum f b = rsum (compose Re f) b.
  Proof.
    intros f b H. induction H as [| e b He H IH]; auto.
    simpl. unfold compose at 1. rewrite IH.
    rewrite RtoC_plus. f_equal.
    inversion He. reflexivity.
  Qed.

  Lemma csum_0 :
    forall (f : A -> C) b, Forall (fun e => f e = 0) b -> csum f b = 0.
  Proof.
    intros f b H. induction H; csimpl.
    rewrite H, IHForall. csimpl.
  Qed.

  Lemma csum_additive :
    forall f f' b, csum (fun e => f e + f' e) b = csum f b + csum f' b.
  Proof.
    intros f f' b. induction b as [| e b IH]; csimpl.
    repeat rewrite <- Cplus_assoc. f_equal.
    rewrite IH. repeat rewrite Cplus_assoc. f_equal. apply Cplus_comm.
  Qed.

  Lemma csum_homogeneous :
    forall z f b, csum (fun e => z * f e) b = z * csum f b.
  Proof.
    intros z f b. induction b as [| e b IH]; csimpl.
    rewrite IH. reflexivity.
  Qed.

  Lemma conj_csum :
    forall f b,
    Cconj (csum f b) = csum (compose Cconj f) b.
  Proof.
    intros f b. induction b as [| e b IH]; csimpl.
    rewrite IH. reflexivity.
  Qed.

  Lemma re_csum :
    forall f b,
    Re (csum f b) = rsum (compose Re f) b.
  Proof.
    intros f b. induction b as [| e b IH]; csimpl.
    rewrite <- IH. reflexivity.
  Qed.

  Lemma im_csum :
    forall f b,
    Im (csum f b) = rsum (compose Im f) b.
  Proof.
    intros f b. induction b as [| e b IH]; csimpl.
    rewrite <- IH. reflexivity.
  Qed.

  Lemma is_real_csum :
    forall f b,
    Forall (compose is_real f) b ->
    is_real (csum f b).
  Proof.
    intros f b H. induction H as [| e b He H IH]; csimpl.
    apply real_plus; assumption.
  Qed.

  Lemma csum_same :
    forall f f' b,
    Forall (fun e => f e = f' e) b ->
    csum f b = csum f' b.
  Proof.
    intros f f' b H. induction H as [| e b He H IH]; simpl; f_equal; assumption.
  Qed.

  (*
  Lemma nonnegative_csum :
    forall f b,
    Forall (compose nonnegative_c f) b ->
    nonnegative_c (csum f b).
  Proof.
    intros f b H. induction H as [| e b He H IH]; csimpl.
    apply nonnegative_plus; assumption.
  Qed.
   *)

End set.

Hint Rewrite Cmult_neg_1 conj_mod_squared cplus_all_real_l cplus_all_real_r
  rsum_csum :
  csimpl.
Hint Rewrite <- Cmod_correct : csimpl.
