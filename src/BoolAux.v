From Coq Require Export Bool.
From Coq Require Import Relations RelationClasses.
From VFA Require Import Perm.

(* Additional properties involving booleans *)

Lemma negb_spec :
  forall P b,
  reflect P b ->
  reflect (~ P) (negb b).
Proof.
  intros P b H. bdestruct b; constructor; auto.
Qed.

Lemma andb_spec :
  forall P P' b b',
  reflect P b ->
  reflect P' b' ->
  reflect (P /\ P') (b && b').
Proof.
  intros P P' b b' H H'.
  bdestruct b; bdestruct b'; constructor; auto; intros []; contradiction.
Qed.

Lemma orb_spec :
  forall P P' b b',
  reflect P b ->
  reflect P' b' ->
  reflect (P \/ P') (b || b').
Proof.
  intros P P' b b' H H'.
  bdestruct b; bdestruct b'; constructor; auto.
  intros []; contradiction.
Qed.

Class reflect1 {A} (P : A -> Prop) (f : A -> bool) :=
  reflect1_forall : forall e, reflect (P e) (f e).

Class reflect2 {A A'} (R : A -> A' -> Prop) (f : A -> A' -> bool) :=
  reflect2_forall : forall e e', reflect (R e e') (f e e').

Class eqb_equiv {A} (eqb : A -> A -> bool) :=
  { eqb_refl : forall e, eqb e e = true;
    eqb_sym : forall e e', eqb e e' = eqb e' e;
    eqb_trans : forall e1 e2 e, eqb e1 e2 = true -> eqb e1 e = eqb e2 e }.

Instance eqb_equiv_Equivalence :
  forall A (R : relation A) eqb,
  eqb_equiv eqb -> reflect2 R eqb -> Equivalence R.
Proof.
  intros A R eqb [Hr Hs Ht] H. split.
  - intros e. specialize (Hr e). destruct (H e e); congruence.
  - intros e e' He. specialize (Hs e e').
    destruct (H e e'), (H e' e); congruence.
  - intros e1 e2 e3 H1 H3. specialize (Ht e1 e2 e3).
    destruct (H e1 e2); destruct (H e2 e3); destruct (H e1 e3); auto;
    try discriminate Ht; auto; contradiction.
Qed.

Instance Equivalence_eqb_equiv :
  forall A (R : relation A) eqb,
  reflect2 R eqb -> Equivalence R -> eqb_equiv eqb.
Proof.
  intros A R eqb Hf [Hr Hs Ht]. split.
  - intros e. destruct (Hf e e); auto.
  - intros e e'. destruct (Hf e e'); destruct (Hf e' e); auto.
    symmetry in r. contradiction.
  - intros e1 e2 e H.
    destruct (Hf e1 e2); try discriminate.
    destruct (Hf e1 e); destruct (Hf e2 e); auto; exfalso; apply n.
    + transitivity e1; auto.
    + transitivity e2; auto.
Qed.

#[export]
Hint Resolve negb_spec andb_spec orb_spec : bdestruct.

#[export]
Hint Unfold reflect1 reflect2 : bdestruct.
