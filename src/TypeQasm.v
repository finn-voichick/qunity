From Coq Require Import List NArith Reals.
From Qunity Require Import Gates.
Import ListNotations.

(* Basic algebraic data types *)
Inductive type :=
  | void
  | unit
  | plus (T0 : type) (T : type)
  | times (T0 : type) (T : type).

Declare Scope qunity_scope.

Infix "<+>" := plus (at level 50) : qunity_scope.
Infix "<*>" := times (at level 40) : qunity_scope.

Open Scope N_scope.
Open Scope nat_scope.
Open Scope bool_scope.
Open Scope qunity_scope.

(* Are two types equal? *)
Fixpoint eqb_type T T' :=
  match T, T' with
  | void, void | unit, unit =>
      true
  | T0 <+> T1, T0' <+> T1' | T0 <*> T1, T0' <*> T1' =>
      eqb_type T0 T0' && eqb_type T1 T1'
  | _, _ => false
  end.

(* A "quantum option" with one extra flag qubit *)
Definition maybe T := unit <+> T.

(* Defining types in terms of others *)
Definition bit := maybe unit.

(* zero values *)
Fixpoint empty T :=
  match T with
  | void => true
  | unit => false
  | T0 <+> T1 => empty T0 && empty T1
  | T0 <*> T1 => empty T0 || empty T1
  end.

(* zero qubits *)
Fixpoint trivial T :=
  match T with
  | void | unit => true
  | T0 <+> T1 => false
  | T0 <*> T1 => trivial T0 && trivial T1
  end.

(* Number of qubits *)
Fixpoint size T :=
  match T with
  | void => 0
  | unit => 0
  | T0 <+> T1 => 1 + max (size T0) (size T1)
  | T0 <*> T1 => size T0 + size T1
  end.

(* TypeQASM program *)
Inductive prog :=
  | fail (T0 T1 : type) (* zero matrix *)
  | id (T : type) (* identity matrix, maybe redundant with gphase *)
  | gphase (gamma : R) (T : type) (* global phase times identity *)
  | app (g : gate) (* apply single quantum gate *)
  | share (T : type) (* use CNOT to duplicate/share in the standard basis *)
  | inj (b : bool) (T0 T1 : type) (* injection, type "T0 -> T0 + T1" *)
  | flip (T0 T1 : type) (* Pauli-X but for arbitrary types *)
  | assoc_amp (T0 T1 T2 : type) (* Associator for sums, type "(T0 + T1) + T2 -> T0 + (T1 + T2)" *)
  | get (b : bool) (T0 T1 : type) (* Discard part of a product *)
  | swap (T0 T1 : type) (* SWAP gates for reordering *)
  | assoc_loc (T0 T1 T2 : type) (* Associator for products, type "(T0 * T1) * T2 -> T0 * (T1 * T2)" *)
  | dist (b : bool) (T T0 T1 : type) (* Distributor, type "T * (T0 + T1) -> T * T0 + T * T1" *)
  | adj (p : prog) (* Adjoint, conjugate transpose *)
  | ctrl (p0 p1 : prog) (* Direct sum of operators *)
  | case (p0 p1 : prog) (* Measure and perform either p0 or p1 *)
  | tensor (p0 p1 : prog) (* Tensor product of operators *)
  | reflect (p : prog) (* Reflect about a projector *)
  | reversible (p : prog) (* Turn non-reversible into reversible "oracle" *)
  | compose (p1 p0 : prog). (* apply p0, then apply p1 *)

(* Does it have a "pure semantics" described by a single operator? *)
Fixpoint pure p :=
  match p with
  | case _ _  =>
      false (* could potentially allow purity if input types are empty, but makes compilation more complicated *)
  | fail _ _
  | id _
  | gphase _ _
  | app _
  | share _
  | inj _ _ _
  | flip _ _
  | assoc_amp _ _ _
  | swap _ _
  | assoc_loc _ _ _
  | dist _ _ _ _
  | adj _
  | ctrl _ _
  | reflect _
  | reversible _ =>
      true
  | get false _ T | get true T _ =>
      trivial T (* allows for purely appending/discarding unit *)
  | tensor p0 p1 | compose p1 p0 =>
      pure p1 && pure p0 
  end.

(* A well-typed program has an input type and output type *)
Fixpoint type_check p :=
  match p with
  | fail T0 T1 =>
      Some (T0, T1)
  | id T | gphase _ T =>
      Some (T, T)
  | app _ =>
      Some (bit, bit)
  | share T =>
      Some (T, T <*> T)
  | inj false T0 T1 =>
      Some (T0, T0 <+> T1)
  | inj true T0 T1 =>
      Some (T1, T0 <+> T1)
  | flip T0 T1 =>
      Some (T0 <+> T1, T1 <+> T0)
  | assoc_amp T0 T1 T2 =>
      Some (T0 <+> T1 <+> T2, T0 <+> (T1 <+> T2))
  | get false T0 T1 =>
      Some (T0 <*> T1, T0)
  | get true T0 T1 =>
      Some (T0 <*> T1, T1)
  | swap T0 T1 =>
      Some (T0 <*> T1, T1 <*> T0)
  | assoc_loc T0 T1 T2 =>
      Some (T0 <*> T1 <*> T2, T0 <*> (T1 <*> T2))
  | dist false T T0 T1 =>
      Some (T <*> (T0 <+> T1), T <*> T0 <+> T <*> T1)
  | dist true T T0 T1 =>
      Some ((T0 <+> T1) <*> T, T0 <*> T <+> T1 <*> T)
  | adj p' =>
      match pure p', type_check p' with
      | true, Some (T, T') => Some (T', T)
      | _, _ => None
      end
  | ctrl p0 p1 =>
      match type_check p0, type_check p1 with
      | Some (T0, T0'), Some (T1, T1') => Some (T0 <+> T1, T0' <+> T1')
      | _, _ => None
      end
  | case p0 p1 =>
      match type_check p0, type_check p1 with
      | Some (T0, T0'), Some (T1, T1') =>
          if eqb_type T0' T1' then Some (T0 <+> T1, T0') else None
      | _, _ => None
      end
  | tensor p0 p1 =>
      match type_check p0, type_check p1 with
      | Some (T0, T0'), Some (T1, T1') => Some (T0 <*> T1, T0' <*> T1')
      | _, _ => None
      end
  | reflect p' =>
      match pure p', type_check p' with
      | true, Some (_, T) => Some (T, T)
      | _, _ => None
      end
  | reversible p' =>
      match type_check p' with
      | Some (T, unit <+> unit) => Some (T <*> bit, T <*> bit)
      | _ => None
      end
  | compose p1 p0 =>
      match type_check p0, type_check p1 with
      | Some (T0, T0'), Some (T1, T1') =>
          if eqb_type T0' T1 then Some (T0, T1') else None
      | _, _ =>
          None
      end
  end.

(* number of extra qubits to prepare in the zero state *)
Fixpoint prep_count p :=
  match p with
  | fail T0 T1 =>
      size T1 - size T0 + 1
  | id _
  | gphase _ _
  | app _
  | flip _ _
  | get _ _ _
  | swap _ _
  | assoc_loc _ _ _
  | dist _ _ _ _ =>
      0
  | share T =>
      size T
  | inj _ _ _ =>
      1
  | adj p' =>
      assertion_count p'
  | assoc_amp T0 T1 T2 =>
      max (size T0) (1 + max (size T1) (size T2)) -
      max (1 + max (size T0) (size T1)) (size T2)
  | reflect p' =>
      prep_count p'
  | ctrl p0 p1
  | case p0 p1 =>
      max (prep_count p0) (prep_count p1)
  | tensor p0 p1
  | compose p1 p0 =>
      prep_count p0 + prep_count p1
  | reversible p' =>
      prep_count p' + assertion_count p'
  end
(* number of qubits asserted to be in the zero state *)
with assertion_count p := 
  match p with
  | fail _ _ =>
      1
  | id _
  | gphase _ _
  | app _
  | share _
  | inj _ _ _
  | flip _ _
  | assoc_amp _ _ _
  | get _ _ _
  | swap _ _
  | assoc_loc _ _ _
  | dist _ _ _ _ =>
      0
  | adj p'
  | reflect p' =>
      prep_count p'
  | ctrl p0 p1
  | case p0 p1 =>
      max (assertion_count p0) (assertion_count p1)
  | tensor p0 p1
  | compose p1 p0 =>
      assertion_count p0 + assertion_count p1
  | reversible p' =>
      prep_count p' + assertion_count p'
  end.

(* number of qubits discarded, zero if pure *)
Fixpoint discard_count p :=
  match p with
  | fail _ _
  | id _
  | gphase _ _
  | app _
  | share _
  | inj _ _ _
  | flip _ _
  | assoc_amp _ _ _
  | swap _ _
  | assoc_loc _ _ _
  | dist _ _ _ _
  | adj _
  | ctrl _ _
  | reflect _
  | reversible _ =>
      0
  | get false _ T | get true T _ =>
      size T
  | case p0 p1 =>
      1 + max (discard_count p0) (discard_count p1)
  | tensor p0 p1
  | compose p1 p0 =>
      discard_count p0 + discard_count p1
  end.

(* should be equal to output size + discard_count + assertion_count *)
Definition wire_count p :=
  match type_check p with
  | None => 0
  | Some (T, _) => size T + prep_count p
  end.

(* Compile a program to a low-level circuit with wire_count wires *)
Fixpoint compile p :=
  match p with
  | fail T T' =>
      (* force failure with a Pauli-X on the assertion wire *)
      pgate [max (size T) (size T')] pauli_x
  | id _ =>
      skip
  | gphase gamma _ =>
      phase gamma
  | app g =>
      pgate [0] g
  | share T =>
      let n := size T in
      Gates.share n 0 n
  | inj false T0 T1 =>
      (* prepend a |0> qubit *)
      rotr 0 (size T0)
  | inj true T0 T1 =>
      (* prepend a |1> qubit *)
      rotr 0 (size T1); pgate [0] pauli_x
  | flip _ _ =>
      (* flip the tag bit *)
      pgate [0] pauli_x
  | assoc_amp T0 T1 T2 =>
      (* move all sub-data to wire 2 *)
      pctrl [] [0] (rotr 1 (size T2); pgate [1] pauli_x);
      (* swap the two tag bits *)
      pswap 0 1;
      (* move some sub-data back to wire 1 *)
      pctrl [0] [] (rotl 1 (size T0))
  | get false _ _ =>
      (* no need to do anything because content is already in right place *)
      skip
  | get true T0 T1 =>
      move_left 0 (size T0) (size T1)
  | swap T0 T1 =>
      (* swapping is just a permutation of wires *)
      permute
        0
        (size T0 + size T1)
        (List.seq (size T0) (size T1) ++ List.seq 0 (size T0))
  | assoc_loc _ _ _ =>
      (* regrouping, but already in correct order *)
      skip
  | dist false T _ _ =>
      (* move tag bit to front *)
      rotr 0 (size T)
  | dist true T T0 T1 =>
      let n0 := size T0 in
      let n1 := size T1 in
      (* TODO double-check *)
      let n := size T in
      (* shift T0 if T0 < T1 *)
      pctrl [0] [] (move_left (S n0) (n1 - n0) n);
      (* shift T1 if T0 > T1 *)
      pctrl [] [0] (move_left (S n1) (n0 - n1) n)
  | adj p' =>
      (* prep and assertion wires are switched *)
      padj (compile p')
  | ctrl p0 p1 =>
      match type_check p0, type_check p1 with
      | Some (_, T0), Some (_, T1) =>
          let n0 := size T0 in
          let n1 := size T1 in
          let a := max (assertion_count p0) (assertion_count p1) in
          (* TODO double-check this *)
          (* ensure assertions end up after data *)
          pctrl [0] [] (reindex S (compile p0); move_left (S n0) a (n1 - n0));
          pctrl [] [0] (reindex S (compile p1); move_left (S n1) a (n0 - n1))
      | _, _ => skip
      end
  | case p0 p1 =>
      match type_check p0 with
      | None => skip
      | Some (_, T) =>
          pctrl [0] [] (reindex S (compile p0));
          pctrl [] [0] (reindex S (compile p1));
          (* assumption: discards before assertions *)
          move_left 0 1 (size T)
      end
  | tensor p0 p1 =>
      match type_check p0, type_check p1 with
      | Some (T0, T0'), Some (T1, T1') =>
          let n0 := size T0 in
          let n0' := size T0' in
          let r0 := prep_count p0 in
          let d0 := discard_count p0 in
          let a0 := assertion_count p0 in
          let n1' := size T1' in
          let d1 := discard_count p1 in
          (* get T1 out of the way of p0's prep *)
          move_right n0 (size T1) r0;
          compile p0;
          (* apply p1 after p0's assertion wires *)
          reindex (Nat.add (n0 + r0)) (compile p1);
          (* group data, assertions, and discards together *)
          permute
            n0'
            (d0 + a0 + n1' + d1)
            (List.seq (n0' + d0 + a0) n1' ++ List.seq n0' d0 ++ List.seq (n0' + d0 + a0 + n1') d1 ++ List.seq (n0' + d0) a0)
      | _, _ => skip
      end
  | reflect p' =>
      match type_check p' with
      | None => skip
      | Some (_, T) =>
          let compiled := compile p' in
          compiled;
          (* multiply by -1 if not all zero assertions *)
          pctrl (List.seq (size T) (assertion_count p')) [] (phase PI);
          phase PI;
          padj compiled
      end
  | reversible p' =>
      match type_check p' with
      | None => skip
      | Some (T, _) =>
          let n := size T in
          (* reindex with hole for extra output *)
          let compiled := reindex (fun i => if i <? n then i else S i) (compile p') in
          compiled;
          (* share output *)
          cnot 0 n;
          (* copy assertions to output assertions *)
          Gates.share (assertion_count p') (S (S (discard_count p'))) (S (wire_count p'));
          (* undo discards *)
          padj compiled
      end
  | compose p1 p0 =>
      match type_check p1 with
      | None => skip
      | Some (T, T') =>
          compile p0;
          (* get p0 discards/assertions out of the way *)
          move_right (size T) (discard_count p0 + assertion_count p0) (prep_count p1);
          compile p1;
          (* swap discards and assertions (order doesn't matter) *)
          move_left (size T' + discard_count p1) (assertion_count p1) (discard_count p0)
      end
  end.

