From Coq Require Import String.
From VFA Require Import Perm.
From Qunity Require Import RealAux MapsAux Syntax Types Tuples Lib.

(* A few example programs (work in progress) *)

(* Deutsch's algorithm *)
Definition deutsch (f : expr -> expr) :=
  hadamard (first (qlet x := #|+> in oracle (x, #|-> (+) f x))).

(* An "R_k" gate used in the QFT *)
Definition rk k :=
  app_gate (gate_u 0 0 (2 * PI / 2 ^ k)).

Module qft.

  Definition l := "l".
  Definition l' := "l'".
  Definition q := "q".
  Definition qs := "qs".
  Definition t := "t".
  Definition x := "x".
  Definition x' := "x'".
  Definition xs := "xs".
  Definition xs' := "xs'".
  Definition xs1 := "xs1".
  Definition xs2 := "xs2".
  Definition xs3 := "xs3".

  Definition qcons := expr_pair.

  Infix "::" := qcons.

  (* n is the size of the first list *)
  Fixpoint qapp n :=
    match n with
    | 0 =>
        \ (| (), t |) ->> t
    | S n' =>
        \ (| x :: xs, t |) ->> x :: qapp n' (| xs, t |)
    end.

  Lemma has_type_qapp :
    forall T n n' a,
    |-- qapp n : tuple T n <*> tuple T n' ->> (a) (tuple T (n + n')).
  Proof.
    intros T n n' a.
    induction n as [| n IH]; simpl; apply has_fun_type_abs; simpl.
    - repeat constructor. intros x' H. inversion H.
    - apply has_type_var.
    - eapply reflect_iff; try apply pat_bool_spec. reflexivity.
    - rewrite <- map_union_update_2; auto using update_neq.
      apply has_type_pair; auto using has_type_var.
      + intros x' T1 T2 H1 H2. apply update_Some_eq in H1 as [H1 _]. subst.
        do 2 rewrite update_neq in H2; discriminate.
      + apply IH. rewrite <- map_union_update_2; auto.
        apply has_type_pair; auto using has_type_var.
        intros x' T1 T2 H1 H2.
        apply update_Some_eq in H1 as [H1 _], H2 as [H2 _]. subst. discriminate.
  Qed.

  (* n is the new size of the reduced-size list *)
  Fixpoint split_last n :=
    match n with
    | 0 =>
        \ x :: () ->> (| (), x |)
    | S n' =>
        \ x :: xs ->>
        qlet (| xs', x' |) := split_last n' xs in (| x :: xs', x' |)
    end.

  Lemma has_type_split_last :
    forall T n a,
    |-- split_last n : tuple T (S n) ->> (a) (tuple T n <*> T).
  Proof.
    intros T n a. induction n as [| n IH]; simpl; apply has_fun_type_abs; simpl.
    - repeat constructor. intros x' _ Hu. inversion Hu.
    - replace (x |-> T) with (map_union empty (x |-> T))
                        by apply map_union_empty_1.
      apply has_type_pair;
      eauto using no_conflict_empty_1, has_type_unit, has_type_var.
    - repeat constructor. intros x' Hx Hs. inversion Hx. inversion Hs.
      subst. discriminate.
    - rewrite <- map_union_update.
      apply has_type_let' with (T := tuple T n <*> T); auto using has_type_var.
      + intros x' T1 T2 H1 H2.
        apply update_Some_eq in H1 as [H1 _], H2 as [H2 _].
        subst. discriminate.
      + simpl. repeat constructor.
        intros x0 H H'. inversion H. inversion H'. subst. discriminate.
      + intros x0 H.
        inversion H; inversion H1; subst; apply update_neq; discriminate.
      + simpl.
        rewrite update_permute, <- map_union_update, <- map_union_update;
        try discriminate.
        apply has_type_pair; auto using has_type_var.
        * apply no_conflict_union_1; intros x0 T1 T2 H1 H2;
          apply update_Some_eq in H1 as [H1 _], H2 as [H2 _];
          subst; discriminate.
        * apply has_type_pair; auto using has_type_var.
          intros x0 T1 T2 H1 H2.
          apply update_Some_eq in H1 as [H1 _], H2 as [H2 _].
          subst. discriminate.
  Qed.

  Fixpoint ctrl_rotations n :=
    match n with
    | 0 =>
        \ () ->> ()
    | 1 =>
        \ x :: () ->> hadamard x :: ()
    | S (S n0 as n1) =>
        \ xs1 ->>
        qlet (| xs2, l |) := split_last n1 xs1 in
        qlet x :: xs3 := ctrl_rotations n1 xs2 in
        qlet (| l', x' |) := ctrl (rk n) (| l, x |) in
        x' :: qapp n0 (| xs3,  (x' :: ()) |)
    end.

  Lemma has_type_ctrl_rotations :
    forall n a,
    |-- ctrl_rotations n : tuple Bit n ->> (a) tuple Bit n.
  Proof.
    intros [| n] a.
    - apply has_fun_type_abs; eauto.
    destruct n as [| n].
    intros n a. induction n as [| n IH]; simpl; apply has_fun_ty


        \ q :: qs ->> ctrl_rotations n' qs
        ctrl (rk n) (| q, ctrl_rotations n' qs |)
    end.

  Fixpoint rotations l n :=
    match l with
    | 0 =>
        \ () ->> ()
    | S l' =>
        \ q :: qs ->> rk (n + 1 - l) q :: rotations l' n qs
    end.

  Fixpoint qft n :=
    match n with
    | 0 =>
        \ () ->> ()
    | 1 =>
        \ x :: () ->> hadamard x :: ()
    | S n' =>
        \ x :: xs ->>
        qlet (| x', xs' |) := ctrl (rotations n' n') (| x, qft n' xs |) in
        (| hadamard x, xs |)
    end.

  Lemma has_type_qft


