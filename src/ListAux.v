From Coq Require Export List ListSet SetoidList.
From Coq Require Import Basics Relations RelationClasses.
From VFA Require Import Perm.
From Qunity Require Import BoolAux.
Import ListNotations.

Set Implicit Arguments.

(* This file contains added definitions based on Coq's ListSet *)

Section definitions.

  Variable A : Type.

  Variable R : relation A.

  (* Are two sets disjoint? *)
  Inductive disjoint : set A -> set A -> Prop :=
    | disjoint_empty s :
        disjoint [] s
    | disjoint_cons e s s' :
        ~ InA R e s' ->
        disjoint s s' ->
        disjoint (e :: s) s'.

  Section quantifiers.

    (* Separating this out makes it easier to use in some places *)
    Variable P : A -> Prop.

    (* Exists, but as a Fixpoint *)
    Fixpoint set_Exists (s : set A) :=
      match s with
      | [] => False
      | e :: s' => P e \/ set_Exists s'
      end.

    (* Forall, but as a Fixpoint *)
    Fixpoint set_Forall (s : set A) :=
      match s with
      | [] => True
      | e :: s' => P e /\ set_Forall s'
      end.

    (* If P is True for one element, it's true for all of them *)
    Definition IffAll s := Exists P s -> Forall P s.

  End quantifiers.

  Fixpoint map_option {A'} (f : A -> option A') (l : list A) :=
    match l with
    | [] => []
    | e :: l' => match f e with
                 | None => map_option f l'
                 | Some e' => e' :: map_option f l'
                 end
    end.

  Section eqb.

    Variable eqb : A -> A -> bool.

    (* Are two lists equivalent using eqb? *)
    Fixpoint eqb_list (l l' : list A) :=
      match l, l' with
      | [], [] => true
      | e :: l, e' :: l' => eqb e e' && eqb_list l l'
      | _, _ => false
      end.

    (* Like set_add but using eqb *)
    Fixpoint set_add_bool e (s : set A) : set A :=
      match s with
      | [] => [e]
      | e' :: s' => if eqb e e' then s else e' :: set_add_bool e s'
      end.

    (* Like set_mem but using eqb *)
    Fixpoint set_mem_bool (e : A) (s : set A) :=
      match s with
      | [] => false
      | e' :: s' => eqb e e' || set_mem_bool e s'
      end.

    (* Like set_union but using eqb *)
    Fixpoint set_union_bool (s s' : set A) :=
      match s' with
      | [] => s
      | e' :: t' => set_add_bool e' (set_union_bool s t')
      end.

    (* Like set_diff but using eqb *)
    Fixpoint set_diff_bool (s s' : set A) : set A :=
      match s with
      | [] => []
      | e :: t =>
          if set_mem_bool e s'
          then set_diff_bool t s'
          else set_add_bool e (set_diff_bool t s')
      end.

    (* Is everything in s also in s'? *)
    Fixpoint subset_bool (s s' : set A) :=
      match s with
      | [] => true
      | e :: t => set_mem_bool e s' && subset_bool t s'
      end.

    (* Do two sets contain the same elements? *)
    Definition eqb_set (s s' : set A) :=
      subset_bool s s' && subset_bool s' s.

    (* Are two sets disjoint? *)
    Fixpoint disjoint_bool (s s' : set A) :=
      match s with
      | [] => true
      | e :: t => negb (set_mem_bool e s') && disjoint_bool t s'
      end.

  End eqb.

  (* A computable boolean for ForallOrdPairs *)
  Fixpoint forall_ord_pairs f (s : set A) :=
    match s with
    | [] => true
    | e :: s' => forallb (f e) s' && forall_ord_pairs f s'
    end.

  (* Is the truth of f the same for all elements in the set? *)
  Definition iff_all (f : A -> bool) (s : set A) :=
    match s with
    | [] => true
    | e :: s' => if f e then forallb f s' else negb (existsb f s')
    end.

End definitions.

Section big_union.

  Variable A A' : Type.

  Variable eqb' : A' -> A' -> bool.

  Variable f : A -> set A'.

  (* Kind of like flat_map, but taking a big union instead of concatenating *)
  Fixpoint big_union (s : set A) : set A' :=
    match s with
    | [] => []
    | e :: s' => set_union_bool eqb' (f e) (big_union s')
    end.

End big_union.

#[export]
Hint Unfold inclA equivlistA IffAll : core.

Lemma InA_In :
  forall A (e : A) s, InA eq e s <-> In e s.
Proof.
  intros A e s. split.
  - intros H. induction H as [e' s H | e' s H IH]; subst; simpl; auto.
  - intros H. induction s as [| e' s IH]; destruct H; subst; auto.
Qed.

Lemma inclA_incl :
  forall A (s s' : set A), inclA eq s s' <-> incl s s'.
Proof.
  intros A s s'. split; intros H e Hi; apply InA_In, H, InA_In, Hi.
Qed.

Lemma incl_nil_2 :
  forall A R (s : set A), Equivalence R -> inclA R s [] -> s = [].
Proof.
  intros A R s He H. destruct s as [| e s]; auto.
  specialize (H e). rewrite InA_nil in H. destruct H. left. reflexivity.
Qed.

Lemma incl_cons_1 :
  forall A R e (s s' : set A),
  Equivalence R -> InA R e s' -> inclA R s s' -> inclA R (e :: s) s'.
Proof.
  intros A R e s s' Hq He Hs e' H'. inversion_clear H'; auto.
  eapply InA_eqA; eauto.
  symmetry. assumption.
Qed.

Lemma incl_cons_1_iff :
  forall A R e (s s' : set A),
  Equivalence R ->
  inclA R (e :: s) s' <-> InA R e s' /\ inclA R s s'.
Proof.
  intros A R e s s' Hq. split.
  - intros H. split; auto.
    apply H. left. reflexivity.
  - intros [He Hs]; auto using incl_cons_1.
Qed.

Lemma incl_cons_2 :
  forall A (R : relation A) (s s' : set A) e,
  inclA R s s' -> inclA R s (e :: s').
Proof. auto. Qed.

Instance incl_cons_3 :
  forall A (R : relation A) e,
  Proper (inclA R ==> inclA R) (cons e).
Proof.
  intros A R e s s' H e' H'. inversion_clear H'; auto.
Qed.

Instance incl_refl :
  forall A (R : relation A), Reflexive (inclA R).
Proof. auto. Qed.

Instance incl_trans :
  forall A (R : relation A), Transitive (inclA R).
Proof. auto. Qed.

#[export]
Hint Unfold const : core.

Instance Exists_incl :
  forall A (R : relation A) P,
  Equivalence R ->
  Proper (R ==> iff) P ->
  Proper (inclA R ==> impl) (Exists P).
Proof.
  intros A R P Hq Hp s s' H He.
  apply Exists_exists in He as [e [Hi He]]. eapply In_InA in Hi; eauto.
  apply H, InA_alt in Hi as [e' [H' Hi]]. apply Exists_exists.
  exists e'. split; auto.
  rewrite <- H'. assumption.
Qed.

Instance Forall_incl :
  forall A (R : relation A) P,
  Equivalence R ->
  Proper (R ==> iff) P ->
  Proper (inclA R ==> flip impl) (Forall P).
Proof.
  intros A R P Hq Hp s s' H Hf. rewrite Forall_forall in *. intros e Hi.
  eapply In_InA in Hi; eauto.
  apply H, InA_alt in Hi as [e' [H' Hi]]. rewrite H'. auto.
Qed.

Lemma Forall_InA :
  forall A R P (s : set A),
  Equivalence R ->
  Proper (R ==> iff) P ->
  Forall P s <-> forall e, InA R e s -> P e.
Proof.
  intros A R P s Hq Hp. split.
  - intros H. induction H as [| e s He H IH].
    + intros e Hi. inversion Hi.
    + intros e' Hi. inversion_clear Hi; auto.
      rewrite H0. assumption.
  - intros H. induction s as [| e s IH]; constructor; auto.
    apply H. left. reflexivity.
Qed.

Lemma add_incl_1 :
  forall A R eqb s (e : A), inclA R s (set_add_bool eqb e s).
Proof.
  intros A R eqb s e e' Hi.
  induction Hi as [h s He | h s Hi IH]; simpl; destruct (eqb e h); auto.
Qed.

Lemma add_incl_cons :
  forall A (R : relation A) eqb s s' (e : A),
  inclA R s s' -> inclA R (set_add_bool eqb e s) (e :: s).
Proof.
  intros A R eqb s s' e H e' H'. induction s as [| h s IH]; auto.
  simpl in H'. destruct (eqb e h); auto.
  inversion_clear H'; auto.
  apply IH in H0; auto.
  inversion_clear H0; auto.
Qed.

Lemma diff_incl :
  forall A (R : relation A) eqb s s',
  inclA R (set_diff_bool eqb s s') s.
Proof.
  intros A R eqb s s'. induction s as [| e s IH]; auto.
  simpl. destruct (set_mem_bool eqb e s'); auto.
  transitivity (e :: set_diff_bool eqb s s');
  eauto using add_incl_cons.
  rewrite IH. reflexivity.
Qed.

Lemma forallb_spec :
  forall A (P : A -> Prop) f,
  reflect1 P f ->
  reflect1 (Forall P) (forallb f).
Proof.
  intros A P f H s. induction s as [| e s IH]; repeat constructor.
  simpl. destruct (H e); destruct IH; constructor; auto;
  intros Hc; inversion Hc; contradiction.
Qed.

#[export]
Hint Resolve forallb_spec : bdestruct.

Lemma forallb_andb :
  forall A f f' (s : set A),
  forallb (fun e => f e && f' e) s = forallb f s && forallb f' s.
Proof.
  intros A f f' s. induction s as [| e s IH]; auto.
  simpl. rewrite IH.
  repeat rewrite andb_assoc. f_equal.
  repeat rewrite <- andb_assoc. f_equal.
  apply andb_comm.
Qed.

Lemma existsb_spec :
  forall A (P : A -> Prop) f,
  reflect1 P f ->
  reflect1 (Exists P) (existsb f).
Proof.
  intros A P f H s. apply iff_reflect. rewrite Exists_exists, existsb_exists.
  split; intros [e [Hi He]]; exists e; split; auto; destruct (H e); auto.
  discriminate.
Qed.

#[export]
Hint Resolve existsb_spec : bdestruct.

Lemma eqb_list_refl :
  forall A eqb (l : list A),
  eqb_equiv eqb ->
  eqb_list eqb l l = true.
Proof.
  intros A eqb l [H _ _]. induction l as [| e l IH]; auto.
  simpl. rewrite H. assumption.
Qed.

Lemma eqb_list_spec :
  forall A (R : relation A) eqb,
  reflect2 R eqb ->
  reflect2 (eqlistA R) (eqb_list eqb).
Proof.
  intros A R eqb Hr l l'. apply iff_reflect. split.
  - intros H. induction H as [| e e' l l' He H IH]; auto.
    simpl. destruct (Hr e e').
    + assumption.
    + contradiction.
  - generalize dependent l'.
    induction l as [| e l IH]; intros l' H; destruct l' as [| e' l'];
    try discriminate.
    + constructor.
    + simpl in H. apply andb_prop in H as [He H].
      constructor; auto.
      destruct (Hr e e'); congruence.
Qed.

Lemma Forall_add :
  forall A P eqb (s : set A) e,
  Forall P s ->
  P e ->
  Forall P (set_add_bool eqb e s).
Proof.
  intros A P eqb s e Hs He. induction Hs as [| e' s H' Hs IH]; simpl; auto.
  destruct (eqb e e'); auto.
Qed.

(*
Lemma In_set_add_bool :
  forall A (R : relation A) eqb s e e',
  (forall e e', reflect (R e e') (eqb e e')) ->
  InA R e (set_add_bool eqb e' s) ->
  R e e' \/ InA R e s.
Proof.
  intros A R eqb s e e' Hr Hi. induction s as [| h s IH].
  - inversion Hi; auto.
  - simpl in Hi. bdestruct (eqb e' h); auto.
    inversion_clear Hi; auto.
    apply IH in H0 as []; auto.
Qed.
 *)

Lemma set_add_bool_spec :
  forall A R eqb s (e e' : A), 
  Equivalence R ->
  reflect2 R eqb ->
  InA R e (set_add_bool eqb e' s) <-> R e e' \/ InA R e s.
Proof.
  intros A R eqb s e e' Hq Hr. split.
  - intros H. induction s as [| h s IH].
    + apply InA_singleton in H. auto.
    + simpl in H. destruct (Hr e' h); auto.
      inversion_clear H; auto.
      apply IH in H0 as []; auto.
  - intros [H | H].
    + induction s as [| h s IH]; simpl; auto.
      destruct (Hr e' h); auto.
      left. transitivity e'; assumption.
    + induction H as [h s Hh | h s H IH]; simpl; destruct (Hr e' h); auto.
Qed.

Lemma set_add_alt :
  forall A eqb s (e : A) H, 
  reflect2 eq eqb ->
  set_add_bool eqb e s = set_add H e s.
Proof.
  intros A eqb s e H Hr. induction s as [| e' s IH]; auto.
  simpl. destruct (Hr e e'); destruct (H e e'); try contradiction.
  - reflexivity.
  - f_equal. assumption.
Qed.

Lemma set_mem_bool_spec :
  forall A (R : relation A) eqb, 
  reflect2 R eqb ->
  reflect2 (InA R) (set_mem_bool eqb).
Proof.
  intros A P eqb Hr e s. apply iff_reflect. split.
  - intros H. induction H as [e' s H | e' s H IH]; simpl.
    + destruct (Hr e e'); auto.
    + rewrite IH. auto with bool.
  - intros H. induction s as [| e' s IH]; try discriminate.
    simpl in H. apply orb_prop in H as [H | H]; auto.
    destruct (Hr e e'); auto.
    discriminate.
Qed.

Lemma set_mem_bool_existsb :
  forall A eqb s (e : A),
  set_mem_bool eqb e s = existsb (eqb e) s.
Proof.
  intros A eqb s e. induction s as [| e' s IH]; auto.
  simpl. rewrite IH. reflexivity.
Qed.

#[export]
Hint Resolve set_mem_bool_spec : bdestruct.

Lemma set_mem_alt :
  forall A eqb s (e : A) H,
  reflect2 eq eqb ->
  set_mem_bool eqb e s = set_mem H e s.
Proof.
  intros A eqb s e H Hq. induction s as [| e' s IH]; auto.
  simpl. destruct (Hq e e'); destruct (H e e'); auto.
  contradiction.
Qed.

Lemma set_mem_eqb :
  forall A eqb s (e e' : A),
  eqb_equiv eqb ->
  eqb e e' = true ->
  set_mem_bool eqb e s = set_mem_bool eqb e' s.
Proof.
  intros A eqb s e e' Hq H. induction s as [| h s IH]; auto.
  simpl. f_equal; auto using eqb_trans.
Qed.

Lemma set_mem_set_add :
  forall A eqb s (e e' : A),
  eqb_equiv eqb ->
  set_mem_bool eqb e (set_add_bool eqb e' s) = eqb e e' || set_mem_bool eqb e s.
Proof.
  intros A eqb s e e' H. induction s as [| h s IH]; auto.
  simpl. destruct (eqb e' h) eqn:E; simpl.
  - apply eqb_trans with (e0 := e) in E; auto.
    rewrite eqb_sym in E; auto.
    destruct (eqb e e'); auto.
    simpl. rewrite eqb_sym in E; auto. rewrite <- E. reflexivity.
  - rewrite IH. repeat rewrite orb_assoc. f_equal. apply orb_comm.
Qed.

Lemma Forall_union :
  forall A P eqb (s s' : set A),
  Forall P s -> Forall P s' -> Forall P (set_union_bool eqb s s').
Proof.
  intros A P eqb s s' H H'.
  induction H' as [| e' s' He H' IH]; simpl; auto using Forall_add.
Qed.

Lemma Forall_big_union :
  forall A A' P eqb' (f : A -> set A') s,
  Forall (fun e => Forall P (f e)) s ->
  Forall P (big_union eqb' f s).
Proof.
  intros A A' P eqb' f s H. induction H as [| e s He H IH]; simpl; auto.
  apply Forall_union; assumption.
Qed.

Lemma set_union_bool_spec :
  forall A R eqb s s' (e : A),
  Equivalence R ->
  reflect2 R eqb ->
  InA R e (set_union_bool eqb s s') <-> InA R e s \/ InA R e s'.
Proof.
  intros A R eqb s s' e Hr. split.
  - intros H. induction s' as [| e' s' IH]; auto.
    simpl in H. apply set_add_bool_spec in H as [H | H]; auto.
    apply IH in H as [H | H]; auto.
  - intros [H | H].
    + induction s' as [| e' s' IH]; auto.
      simpl. apply add_incl_1, IH.
    + induction H as [e' s' H | e' s' H IH]; simpl;
      apply set_add_bool_spec; auto.
Qed.

Lemma set_union_alt :
  forall A eqb (s s' : set A) H,
  reflect2 eq eqb ->
  set_union_bool eqb s s' = set_union H s s'.
Proof.
  intros A eqb s s' H Hr. induction s' as [| e' s' IH]; auto.
  simpl. erewrite <- set_add_alt by eauto. f_equal. assumption.
Qed.

Lemma set_mem_set_union :
  forall A eqb s s' (e : A),
  eqb_equiv eqb ->
  set_mem_bool eqb e (set_union_bool eqb s s') =
  set_mem_bool eqb e s || set_mem_bool eqb e s'.
Proof.
  intros A eqb s s' e H. induction s' as [| e' s' IH]; auto with bool.
  simpl. rewrite set_mem_set_add, IH by assumption.
  repeat rewrite orb_assoc. f_equal. apply orb_comm.
Qed.

Lemma set_diff_bool_spec :
  forall A R eqb s s' (e : A),
  Equivalence R ->
  reflect2 R eqb ->
  InA R e (set_diff_bool eqb s s') <-> InA R e s /\ ~ InA R e s'.
Proof.
  intros A R eqb s s' e Hq Hr. apply set_mem_bool_spec in Hr as H2. split.
  - intros H.
    split; induction s as [| e' s IH]; try (now inversion H);
    simpl in H; destruct (H2 e' s'); auto;
    apply set_add_bool_spec in H as [H | H]; auto.
    intros Hc. apply n. eapply InA_eqA; eauto.
  - intros [Hi Hn].
    induction Hi as [e' s Hi | e' s Hi IH]; simpl;
    destruct (H2 e' s').
    + exfalso. symmetry in Hi. eapply Hn, InA_eqA; eauto.
    + apply set_add_bool_spec; auto.
    + assumption.
    + apply set_add_bool_spec; auto.
Qed.

Lemma set_diff_alt :
  forall A eqb (s s' : set A) H,
  reflect2 eq eqb ->
  set_diff_bool eqb s s' = set_diff H s s'.
Proof.
  intros A eqb s s' H Hr. induction s as [| e s IH]; auto.
  simpl. erewrite <- set_mem_alt, <- set_add_alt, IH; eauto.
Qed.

Lemma set_mem_set_diff :
  forall A eqb s s' (e : A),
  eqb_equiv eqb ->
  set_mem_bool eqb e (set_diff_bool eqb s s') =
  set_mem_bool eqb e s && negb (set_mem_bool eqb e s').
Proof.
  intros A eqb s s' e Hq.
  induction s as [| e' s IH]; auto.
  simpl. destruct (eqb e e') eqn:E1.
  - simpl. rewrite eqb_sym in E1; auto.
    rewrite set_mem_eqb with (e := e') (e' := e); auto.
    destruct (set_mem_bool eqb e s') eqn:E2.
    + simpl in *. rewrite andb_false_r in IH. assumption.
    + rewrite set_mem_set_add by assumption.
      rewrite eqb_sym in E1; auto.
      rewrite E1. reflexivity.
  - simpl. destruct (set_mem_bool eqb e' s') eqn:E2; auto.
    rewrite set_mem_set_add by assumption. rewrite E1. assumption.
Qed.

Lemma subset_bool_spec :
  forall A (R : relation A) eqb,
  Equivalence R ->
  reflect2 R eqb ->
  reflect2 (inclA R) (subset_bool eqb).
Proof.
  intros A R eqb Hq Hr s s'. induction s as [| e s IH]; simpl.
  - constructor. apply incl_nil.
  - apply set_mem_bool_spec in Hr as H2. destruct (H2 e s').
    + destruct IH; constructor.
      * apply incl_cons_1; auto.
      * intros Hc. apply incl_cons_1_iff in Hc as [_ Hc]; auto.
    + constructor. intros Hc. apply incl_cons_1_iff in Hc as [Hc _]; auto.
Qed.

#[export]
Hint Resolve subset_bool_spec : bdestruct.

Lemma eqb_set_spec :
  forall A (R : relation A) eqb,
  Equivalence R ->
  reflect2 R eqb ->
  reflect2 (equivlistA R) (eqb_set eqb).
Proof.
  intros A R eqb Hq Hr s s'. unfold eqb_set.
  apply subset_bool_spec in Hr as H2; auto.
  destruct (H2 s s').
  - destruct (H2 s' s); constructor.
    + split; auto.
    + intros Hc. apply n. intros e. apply Hc.
  - constructor. intros Hc. apply n. intros e. apply Hc.
Qed.

Lemma disjoint_bool_spec :
  forall A (R : relation A) eqb,
  Equivalence R ->
  reflect2 R eqb ->
  reflect2 (disjoint R) (disjoint_bool eqb).
Proof.
  intros A R eqb Hq Hr s s'. apply set_mem_bool_spec in Hr as H2.
  apply iff_reflect. split.
  - intros H. induction H as [s' | e s s' He H IH]; auto.
    simpl. destruct (H2 e s'); auto.
  - intros H. induction s as [| e s IH]; constructor; simpl in H.
    + destruct (H2 e s'); auto.
      discriminate.
    + apply andb_prop in H as [_ H]. auto.
Qed.

Lemma forall_ord_pairs_spec :
  forall A (R : relation A) f,
  reflect2 R f ->
  reflect1 (ForallOrdPairs R) (forall_ord_pairs f).
Proof.
  intros A R f H s. induction s as [| e s IH]; repeat constructor.
  simpl. specialize (H e). apply forallb_spec in H as H1. destruct (H1 s).
  - destruct IH; auto using reflect, ForallOrdPairs.
    constructor. intros Hc. inversion Hc. contradiction.
  - constructor. intros Hc. inversion Hc. contradiction.
Qed.

Lemma forall_ord_pairs_andb :
  forall A f f' (s : set A),
  forall_ord_pairs (fun e e' => f e e' && f' e e') s =
  forall_ord_pairs f s && forall_ord_pairs f' s.
Proof.
  intros A f f' s. induction s as [| e s IH]; auto.
  simpl. rewrite IH, forallb_andb.
  repeat rewrite andb_assoc. f_equal.
  repeat rewrite <- andb_assoc. f_equal.
  apply andb_comm.
Qed.

Lemma set_Exists_spec :
  forall A P (s : set A), set_Exists P s <-> Exists P s.
Proof.
  intros A P s. split.
  - induction s as [| e s IH].
    + contradiction.
    + intros [H | H]; auto.
  - intros H. induction H as [e s H | e s IH]; simpl; auto.
Qed.

Lemma set_Forall_spec :
  forall A P (s : set A), set_Forall P s <-> Forall P s.
Proof.
  intros A P s. split.
  - induction s as [| e s IH].
    + auto.
    + intros [He H]. auto.
  - intros H. induction H as [| e s He H IH]; constructor; assumption.
Qed.

Lemma Forall_impl_2 :
  forall A (P P' : A -> Prop) (s : set A),
  Forall (fun e => P e -> P' e) s ->
  Forall P s ->
  Forall P' s.
Proof.
  intros A P P' s Hs H.
  induction H as [| e s He H IH]; constructor; inversion Hs; auto.
Qed.

Lemma ForallOrdPairs_impl :
  forall A (R R' : relation A) (s : set A),
  ForallOrdPairs (fun e e' => R e e' -> R' e e') s ->
  ForallOrdPairs R s ->
  ForallOrdPairs R' s.
Proof.
  intros A R R' s Hs H.
  induction H as [| e s Hf H IH]; constructor; inversion_clear Hs;
  eauto using Forall_impl_2.
Qed.

Lemma IffAll_nil :
  forall A (P : A -> Prop), IffAll P [].
Proof.
  intros A P H. inversion H.
Qed.

Lemma IffAll_cons :
  forall A P s (e : A),
  IffAll P (e :: s) <-> (P e -> Forall P s) /\ (Exists P s -> P e).
Proof.
  intros A P s e. split.
  - intros H.
    split; intros He; assert (Exists P (e :: s)) as Hx by auto;
    apply H in Hx; inversion Hx; assumption.
  - intros [Hf He] H. inversion H; subst; auto.
Qed.

Lemma IffAll_cons_forall :
  forall A P s (e : A),
  IffAll P (e :: s) <-> Forall (fun e' => P e <-> P e') s.
Proof.
  intros A P s e. rewrite IffAll_cons. split.
  - intros [Hf He]. apply Forall_forall. intros e' H'.
    split; intros Hp.
    + apply Hf in Hp. rewrite Forall_forall in Hp. auto.
    + apply He, Exists_exists. exists e'. auto.
  - intros H. split; intros He; rewrite Forall_forall in *.
    + intros e' H'. apply H; assumption.
    + apply Exists_exists in He as [e' [Hi Hp]]. apply H in Hp; assumption.
Qed.

Lemma IffAll_cons_IffAll :
  forall A P s (e : A),
  IffAll P (e :: s) -> IffAll P s.
Proof.
  intros A P s e. rewrite IffAll_cons. intros [Hf He] H. auto.
Qed.

Lemma IffAll_ord :
  forall A P (s : set A),
  IffAll P s <-> ForallOrdPairs (fun e e' => P e <-> P e') s.
Proof.
  intros A P s. split.
  - intros H.
    induction s as [| e0 s IH]; constructor; eauto using IffAll_cons_IffAll.
    apply IffAll_cons in H as [Hf He].
    apply Forall_forall. intros e Hi. split.
    + intros H0. apply Forall_forall with (l := s); auto.
    + intros H. apply He, Exists_exists. exists e. auto.
  - intros H. induction H as [| e s Hf H IH].
    + apply IffAll_nil.
    + apply IffAll_cons_forall, Hf.
Qed.

Lemma IffAll_iff :
  forall A P (s : set A),
  IffAll P s <-> ForallPairs (fun e e' => P e <-> P e') s.
Proof.
  intros A P s. split.
  - intros H. apply ForallOrdPairs_ForallPairs.
    + intros e. reflexivity.
    + intros e e' He. split; apply He.
    + apply IffAll_ord, H.
  - intros H. apply IffAll_ord, ForallPairs_ForallOrdPairs, H.
Qed.

Lemma iff_all_spec :
  forall A (P : A -> Prop) f,
  reflect1 P f ->
  reflect1 (IffAll P) (iff_all f).
Proof.
  intros A P f Hr [| e s]; simpl; auto using reflect, IffAll_nil.
  destruct (Hr e).
  - apply forallb_spec in Hr as []; constructor.
    + constructor; auto.
    + intros Hc. assert (Exists P (e :: s)) as He by auto. apply Hc in He.
      inversion He. contradiction.
  - apply existsb_spec in Hr as []; constructor.
    + intros Hc. assert (Exists P (e :: s)) as He by auto. apply Hc in He.
      inversion He. contradiction.
    + intros Hc. inversion Hc; subst; contradiction.
Qed.

Lemma disjoint_spec :
  forall A R s s',
  Equivalence R ->
  disjoint R s s' <-> forall e : A, InA R e s -> InA R e s' -> False.
Proof.
  intros A R s s' Hr. split; intros H.
  - induction H; intros e' Hi H'; inversion_clear Hi; eauto using InA_eqA.
  - induction s as [| e s IH]; constructor.
    + intros Hc. apply H in Hc; auto.
      left. reflexivity.
    + apply IH. intros e' Hi. apply H. simpl. auto.
Qed.

Lemma big_union_spec :
  forall A A' R' eqb' (f : A -> set A') s e',
  Equivalence R' ->
  (forall e e', reflect (R' e e') (eqb' e e')) ->
  InA R' e' (big_union eqb' f s) <-> Exists (fun e => InA R' e' (f e)) s.
Proof.
  intros A A' R' eqb' f s e' Hq Hr. split.
  - intros H. induction s as [| e s IH].
    + inversion H.
    + simpl in H. apply set_union_bool_spec in H as [H | H]; auto.
  - intros H.
    induction H as [e s H | e s H IH]; simpl; apply set_union_bool_spec; auto.
Qed.

#[export]
Hint Resolve
  eqb_list_spec set_mem_bool_spec forall_ord_pairs_spec iff_all_spec :
  bdestruct.
