From PLF Require Export Maps.
From Coq Require Import RelationClasses FunctionalExtensionality.
From VFA Require Import Perm.
From Qunity Require Import BoolAux ListAux.
Import Maps (inclusion).

(* Additional properties of partial maps *)

Lemma eqb_string_spec :
  reflect2 eq eqb_string.
Proof.
  intros x x'. unfold eqb_string.
  destruct (string_dec x x'); constructor; assumption.
Qed.

#[export]
Hint Resolve eqb_string_spec : bdestruct.

Definition var_set := set string.

Definition eq_map {A} (s : var_set) (m m' : partial_map A) :=
  Forall (fun x => m x = m' x) s.

Instance eq_map_equivalence :
  forall A s, Equivalence (eq_map (A:=A) s).
Proof.
  intros A s. split.
  - intros m. apply Forall_forall. reflexivity.
  - intros m m' H. eapply Forall_impl; eauto.
    intros x H'. symmetry. assumption.
  - intros m1 m2 m3 H1 H3. unfold eq_map in *. rewrite Forall_forall in *.
    intros x H. transitivity (m2 x); auto.
Qed.

Lemma update_neq_empty :
  forall A m x (v : A), (x |-> v; m) <> empty.
Proof.
  intros A m x v H. apply equal_f with (x0 := x) in H. rewrite update_eq in H.
  discriminate.
Qed.

Lemma update_neq_empty_2 :
  forall A (s : var_set) m (v : A),
  ~ Exists (fun x => eq_map s (x |-> v; m) empty) s.
Proof.
  intros A s m v H. induction s as [| e s IH]; inversion_clear H.
  - inversion_clear H0. rewrite update_eq in H. discriminate.
  - apply IH, Exists_exists. apply Exists_exists in H0 as [x [Hi H]].
    exists x. inversion H. auto.
Qed.

Lemma update_Some_eq :
  forall A x x' (v v' : A), (x |-> v) x' = Some v' <-> x = x' /\ v = v'.
Proof.
  intros A x x' v v'. split.
  - intros H. unfold update, t_update in H. bdestruct (eqb_string x x').
    + injection H as. auto.
    + discriminate.
  - intros [Hx Hv]. subst. apply update_eq.
Qed.

Lemma inclusion_empty :
  forall A (m : partial_map A), inclusion empty m.
Proof.
  intros A m x v H. discriminate.
Qed.

Instance inclusion_refl :
  forall A, Reflexive (inclusion (A:=A)).
Proof.
  intros A m x. auto.
Qed.

Instance inclusion_trans :
  forall A, Transitive (inclusion (A:=A)).
Proof.
  intros A m1 m2 m3 H1 H3 x v Hv. apply H3, H1, Hv.
Qed.

Lemma inclusion_eq :
  forall A (m m' : partial_map A),
  inclusion m m' ->
  inclusion m' m ->
  m = m'.
Proof.
  intros A m m' H H'. apply functional_extensionality. intros x.
  specialize (H x). specialize (H' x).
  destruct (m x).
  - symmetry. auto.
  - destruct (m' x); auto.
Qed.

(* Is the map defined (Some) at the given key? *)
Definition defined_on {A} x (m : partial_map A) :=
  exists v, m x = Some v.

(* defined_on as a computable boolean *)
Definition defined_on_bool {A} x (m : partial_map A) :=
  match m x with
  | None => false
  | Some _ => true
  end.

Lemma defined_on_bool_spec :
  forall A (m : partial_map A) x,
  reflect (defined_on x m) (defined_on_bool x m).
Proof.
  intros A m x. unfold defined_on, defined_on_bool.
  destruct (m x) as [v |]; constructor.
  - exists v. reflexivity.
  - intros [v H]. discriminate.
Qed.

#[export]
Hint Resolve defined_on_bool_spec : bdestruct.

Lemma defined_not_None :
  forall A x (m : partial_map A), defined_on x m <-> m x <> None.
Proof.
  intros A x m. unfold defined_on.
  destruct (m x) as [v |] eqn:E; split; try (intros []; congruence).
  exists v. reflexivity.
Qed.

Lemma undefined_None :
  forall A x (m : partial_map A), m x = None <-> ~ defined_on x m.
Proof.
  intros A x m. unfold defined_on.
  destruct (m x) as [v |] eqn:E; split; try congruence.
  - intros Hc. exfalso. apply Hc. exists v. reflexivity.
  - intros _ [v H]. discriminate.
Qed.

Lemma undefined_empty :
  forall A x, ~ defined_on x (empty (A:=A)).
Proof.
  intros A x [v H]. discriminate.
Qed.

Lemma defined_update_eq :
  forall A m x (v : A), defined_on x (x |-> v; m).
Proof.
  intros A m x v. exists v. apply update_eq.
Qed.

Lemma defined_update_neq :
  forall A m x x' (v' : A), defined_on x m -> defined_on x (x' |-> v'; m).
Proof.
  intros a m x x' v [v' H']. unfold defined_on, update, t_update.
  bdestruct (eqb_string x' x).
  - exists v. reflexivity.
  - exists v'. assumption.
Qed.

Lemma defined_update_iff :
  forall A m x x' (v : A),
  defined_on x' (x |-> v; m) -> x = x' \/ defined_on x' m.
Proof.
  intros A m x x' v [v' H]. unfold update, t_update in H.
  bdestruct (eqb_string x x'); auto.
  right. exists v'. assumption.
Qed.

Lemma defined_update_empty :
  forall A x x' (v : A), defined_on x' (x |-> v) -> x = x'.
Proof.
  intros A x x' v H. apply defined_update_iff in H as [H | H]; auto.
  apply undefined_empty in H as [].
Qed.

Lemma inclusion_defined :
  forall A (m m' : partial_map A) x,
  inclusion m m' -> defined_on x m -> defined_on x m'.
Proof.
  intros A m m' x Hi [v H]. exists v. apply Hi, H.
Qed.

(* Do the two maps agree on every key that they share? *)
Definition no_conflict {A} (m m' : partial_map A) :=
  forall x v v', m x = Some v -> m' x = Some v' -> v = v'.

Instance no_conflict_refl :
  forall A, Reflexive (no_conflict (A := A)).
Proof.
  intros A m x v v' H H'. rewrite H in H'. congruence.
Qed.

Instance no_conflict_sym :
  forall A, Symmetric (no_conflict (A := A)).
Proof.
  intros A m m' Hn x v' v H' H. symmetry. eauto.
Qed.

Lemma no_conflict_empty_1 :
  forall A (m : partial_map A), no_conflict empty m.
Proof.
  unfold empty. intros A m x v v' Hv. discriminate.
Qed.

Lemma no_conflict_empty_2 :
  forall A (m : partial_map A), no_conflict m empty.
Proof.
  unfold empty. intros A m x v v' Hv Hv'. discriminate.
Qed.

Lemma no_conflict_inclusion :
  forall A (m m' : partial_map A), inclusion m m' -> no_conflict m m'.
Proof.
  intros A m m' Hi x v v' H H'. apply Hi in H. rewrite H in H'. congruence.
Qed.

Lemma no_conflict_inclusion_1 :
  forall A (m1 m2 m : partial_map A),
  inclusion m1 m2 -> no_conflict m2 m -> no_conflict m1 m.
Proof.
  intros A m1 m2 m Hi H x v1 v H1 Hv. eapply H.
  - apply Hi, H1.
  - assumption.
Qed.

Lemma no_conflict_inclusion_2 :
  forall A (m1 m2 m : partial_map A),
  inclusion m1 m2 -> no_conflict m m2 -> no_conflict m m1.
Proof.
  intros A m1 m2 m Hi H. symmetry. eapply no_conflict_inclusion_1; eauto.
  symmetry. assumption.
Qed.

Definition no_conflict_bool {A} eqb (b : set string) (m m' : partial_map A) :=
  forallb (fun x => match m x, m' x with
                    | Some v, Some v' => eqb v v'
                    | _, _ => true
                    end) b.

(* Union of partial maps, defaulting to the second one *)
Definition map_union {A} (m m' : partial_map A) : partial_map A :=
  fun x => match m x, m' x with
           | _, Some v => Some v
           | Some v, None => Some v
           | None, None => None
           end.

Lemma map_union_empty_1 :
  forall A (m : partial_map A), map_union empty m = m.
Proof.
  intros A m. apply functional_extensionality. intros x. unfold map_union.
  rewrite apply_empty. destruct (m x); reflexivity.
Qed.

Lemma map_union_empty_2 :
  forall A (m : partial_map A), map_union m empty = m.
Proof.
  intros A m. apply functional_extensionality. intros x. unfold map_union.
  rewrite apply_empty. destruct (m x); reflexivity.
Qed.

Lemma map_union_empty_iff :
  forall A (m m' : partial_map A),
  map_union m m' = empty <-> m = empty /\ m' = empty.
Proof.
  intros A m m'. split.
  - intros H.
    split; apply functional_extensionality; intros x;
    apply equal_f with (x0 := x) in H; unfold map_union in H;
    destruct (m x), (m' x); auto.
    discriminate.
  - intros [H H']. subst. apply map_union_empty_1.
Qed.

Lemma map_union_assoc :
  forall A (m1 m2 m3 : partial_map A),
  map_union m1 (map_union m2 m3) = map_union (map_union m1 m2) m3.
Proof.
  intros A m1 m2 m3. apply functional_extensionality. intros x.
  unfold map_union. destruct (m1 x), (m2 x), (m3 x); reflexivity.
Qed.

Lemma map_union_spec :
  forall A (m m' : partial_map A) x v,
  no_conflict m m' ->
  map_union m m' x = Some v <-> m x = Some v \/ m' x = Some v.
Proof.
  intros A m m' x v H. unfold map_union.
  split; destruct (m x) as [v0 |] eqn:E, (m' x) as [v1 |] eqn:E'; auto;
  intros [H0 | H1]; try congruence.
  eapply H in E. apply E in E'. congruence.
Qed.

Lemma no_conflict_union :
  forall A (m m' : partial_map A),
  no_conflict m m' <-> map_union m m' = map_union m' m.
Proof.
  intros A m m'. split.
  - intros H. apply functional_extensionality. intros x. unfold map_union.
    destruct (m x) eqn:E, (m' x) eqn:E'; auto.
    f_equal. symmetry. apply (H x); assumption.
  - intros Hu x v v' H H'.
    unfold map_union in Hu. apply equal_f with (x0 := x) in Hu.
    rewrite H, H' in Hu. congruence.
Qed.

Lemma no_conflict_union_1 :
  forall A (m1 m2 m : partial_map A),
  no_conflict m1 m -> no_conflict m2 m -> no_conflict (map_union m1 m2) m.
Proof.
  unfold map_union. intros A m1 m2 m H1 H2 x v v' H H'.
  destruct (m1 x) as [v1 |] eqn:E1, (m2 x) as [v2 |] eqn:E2; try discriminate;
  injection H as; subst; eauto.
Qed.

Lemma no_conflict_union_2 :
  forall A (m m1 m2 : partial_map A),
  no_conflict m m1 -> no_conflict m m2 -> no_conflict m (map_union m1 m2).
Proof.
  intros A m m1 m2 H1 H2. symmetry.
  apply no_conflict_union_1; symmetry; assumption.
Qed.

Lemma map_union_defined_1 :
  forall A (m m' : partial_map A) x,
  defined_on x m -> defined_on x (map_union m m').
Proof.
  intros A m m' x [v H]. unfold defined_on, map_union. rewrite H.
  destruct (m' x) as [v' |].
  - exists v'. reflexivity.
  - exists v. reflexivity.
Qed.

Lemma map_union_defined_2 :
  forall A (m m' : partial_map A) x,
  defined_on x m' ->
  defined_on x (map_union m m').
Proof.
  intros A m m' x [v H]. exists v. unfold map_union. rewrite H.
  destruct (m x); reflexivity.
Qed.

Lemma map_union_defined_iff :
  forall A (m m' : partial_map A) x,
  defined_on x (map_union m m') <-> defined_on x m \/ defined_on x m'.
Proof.
  intros A m m' x. split.
  - unfold map_union. intros [v H]. unfold defined_on.
    destruct (m x) as [v1 |].
    + left. exists v1. reflexivity.
    + right. destruct (m' x) as [v2 |].
      * exists v2. reflexivity.
      * discriminate.
  - intros [H | H]; auto using map_union_defined_1, map_union_defined_2.
Qed.

Lemma map_union_undefined :
  forall A (m m' : partial_map A) x,
  m x = None -> m' x = None -> map_union m m' x = None.
Proof.
  intros A m m' x H H'. unfold map_union. rewrite H, H'. reflexivity.
Qed.

Lemma map_union_undefined_iff :
  forall A (m m' : partial_map A) x,
  map_union m m' x = None <-> m x = None /\ m' x = None.
Proof.
  intros A m m' x. split.
  - unfold map_union. intros H. destruct (m x), (m' x); try discriminate. auto.
  - intros [H H']. auto using map_union_undefined.
Qed.

Lemma map_union_idempotent :
  forall A (m : partial_map A), map_union m m = m.
Proof.
  intros A m. apply functional_extensionality. intros x.
  unfold map_union. destruct (m x); reflexivity.
Qed.

Lemma map_union_update :
  forall A (m : partial_map A) x v,
  map_union m (x |-> v) = (x |-> v; m).
Proof.
  intros A m x v. apply functional_extensionality. intros x'.
  unfold map_union, update, t_update. bdestruct (eqb_string x x').
  - destruct (m x'); reflexivity.
  - rewrite apply_empty. destruct (m x'); reflexivity.
Qed.

Lemma map_union_update_2 :
  forall A (m : partial_map A) x v,
  m x = None -> map_union (x |-> v) m = (x |-> v; m).
Proof.
  intros A m x v H. apply functional_extensionality. intros x'.
  unfold map_union, update, t_update. bdestruct (eqb_string x x').
  - subst. rewrite H. reflexivity.
  - rewrite apply_empty. destruct (m x'); auto.
Qed.

Lemma map_union_eq_1 :
  forall A (m m' : partial_map A) x v,
  no_conflict m m' -> m x = Some v -> map_union m m' x = Some v.
Proof.
  intros A m m' x v H Hx. unfold map_union. rewrite Hx.
  destruct (m' x) as [v' |] eqn:E; auto.
  f_equal. symmetry. eapply H.
  - apply Hx.
  - apply E.
Qed.

Lemma map_union_eq_2 :
  forall A (m m' : partial_map A) x v,
  m' x = Some v -> map_union m m' x = Some v.
Proof.
  intros A m m' x v H. unfold map_union. rewrite H.
  destruct (m x); reflexivity.
Qed.

Lemma union_inclusion_1 :
  forall A (m m' : partial_map A), inclusion m m' <-> map_union m' m = m'.
Proof.
  intros A m m'. split.
  - intros H. apply functional_extensionality. intros x. unfold map_union.
    destruct (m x) eqn:E.
    + apply H in E. rewrite E. reflexivity.
    + destruct (m' x); reflexivity.
  - intros H x v Hv. rewrite <- H. apply map_union_eq_2, Hv.
Qed.

Lemma union_inclusion_2 :
  forall A (m m' : partial_map A), inclusion m m' -> map_union m m' = m'.
Proof.
  intros A m m' H. apply functional_extensionality. intros x.
  unfold map_union. destruct (m' x) as [v |] eqn:E'.
  - destruct (m x); reflexivity.
  - destruct (m x) eqn:E; auto.
    apply H in E. congruence.
Qed.

Lemma union_inclusion_2_iff :
  forall A (m m' : partial_map A),
  no_conflict m m' /\ map_union m m' = m' <-> inclusion m m'.
Proof.
  intros A m m'. split.
  - intros [Hn H] x v Hv. destruct (m' x) eqn:E.
    + f_equal. symmetry. eauto.
    + rewrite <- H in E. unfold map_union in E. rewrite Hv in E.
      destruct (m' x); discriminate.
  - intros H. split; auto using union_inclusion_2.
    intros x v1 v2 H1 H2. apply H in H1. rewrite H1 in H2. injection H2. auto.
Qed.

Lemma inclusion_union_l1 :
  forall A (m1 m2 m : partial_map A),
  no_conflict m1 m2 ->
  inclusion (map_union m1 m2) m ->
  inclusion m1 m.
Proof.
  intros A m1 m2 m Hn H x v Hx. auto using map_union_eq_1.
Qed.

Lemma inclusion_union_l2 :
  forall A (m1 m2 m : partial_map A),
  inclusion (map_union m1 m2) m ->
  inclusion m2 m.
Proof.
  intros A m1 m2 m H x v Hx. auto using map_union_eq_2.
Qed.

Lemma inclusion_union_l_iff :
  forall A (m1 m2 m : partial_map A),
  no_conflict m1 m2 /\ inclusion (map_union m1 m2) m <->
  inclusion m1 m /\ inclusion m2 m.
Proof.
  intros A m1 m2 m. split.
  - intros [Hn H]. eauto using inclusion_union_l1, inclusion_union_l2.
  - intros [H1 H2]. split.
    + intros x v1 v2 H H'.
      apply H1 in H. apply H2 in H'. rewrite H in H'. congruence.
    + intros x v H. unfold map_union in H.
      destruct (m1 x) as [v1 |] eqn:E1, (m2 x) as [v2 |] eqn:E2;
      try discriminate; injection H as H; subst; auto.
Qed.

Lemma inclusion_union_r1 :
  forall A (m m1 m2 : partial_map A),
  no_conflict m1 m2 ->
  inclusion m m1 ->
  inclusion m (map_union m1 m2).
Proof.
  intros A m m1 m2 Hn H x v Hv.
  unfold map_union. apply H in Hv. rewrite Hv.
  destruct (m2 x) as [v' |] eqn:E; auto.
  f_equal. symmetry. eauto.
Qed.

Lemma inclusion_union_r2 :
  forall A (m m1 m2 : partial_map A),
  inclusion m m2 -> inclusion m (map_union m1 m2).
Proof.
  intros A m m1 m2 H x v Hv.
  unfold map_union. apply H in Hv. rewrite Hv.
  destruct (m1 x); reflexivity.
Qed.

(* Filter by the keys of a partial map *)
Definition filter_map {A} (f : string -> bool) (m : partial_map A) :
  partial_map A :=
  fun x => if f x then m x else None.

Lemma filter_empty :
  forall A f, filter_map (A := A) f empty = empty.
Proof.
  intros A f. apply functional_extensionality. intros x.
  unfold filter_map. destruct (f x); reflexivity.
Qed.

Lemma defined_on_filter :
  forall A (m : partial_map A) f x,
  defined_on x (filter_map f m) <-> f x = true /\ defined_on x m.
Proof.
  unfold defined_on, filter_map. intros A m f x. split.
  - intros [v H].
    destruct (f x); try discriminate.
    split; auto.
    exists v. assumption.
  - intros [He [v H]]. exists v. rewrite He. assumption.
Qed.

Lemma inclusion_filter_1 :
  forall A (m m' : partial_map A) f,
  inclusion m m' -> inclusion (filter_map f m) m'.
Proof.
  unfold filter_map. intros A m m' f H x v H'. destruct (f x); auto.
  discriminate.
Qed.

Lemma inclusion_filter_2 :
  forall A (m m' : partial_map A) f f',
  (forall x, f x = true -> f' x = true) ->
  inclusion m m' ->
  inclusion (filter_map f m) (filter_map f' m').
Proof.
  unfold filter_map. intros A m m' f f' H Hi x v Hf.
  destruct (f x) eqn:E; try discriminate.
  apply H in E. rewrite E. auto.
Qed.

Lemma filter_update :
  forall A m x (v : A) f,
  filter_map f (x |-> v; m) =
  if f x then (x |-> v; filter_map f m) else filter_map f m.
Proof.
  intros A m x v f. apply functional_extensionality. intros x'.
  unfold filter_map, update, t_update.
  destruct (f x) eqn:E; bdestruct (eqb_string x x'); subst;
  try rewrite E; reflexivity.
Qed.

Lemma filter_permute :
  forall A (m : partial_map A) f f',
  filter_map f (filter_map f' m) = filter_map f' (filter_map f m).
Proof.
  intros A m f f'. apply functional_extensionality. intros x.
  unfold filter_map. destruct (f x), (f' x); reflexivity.
Qed.

Lemma filter_andb :
  forall A (m : partial_map A) f f',
  filter_map f (filter_map f' m) = filter_map (fun x => f x && f' x) m.
Proof.
  intros A m f f'. apply functional_extensionality. intros x.
  unfold filter_map. destruct (f x), (f' x); reflexivity.
Qed.

Lemma filter_union :
  forall A f (m m' : partial_map A),
  filter_map f (map_union m m') = map_union (filter_map f m) (filter_map f m').
Proof.
  intros A f m m'. apply functional_extensionality. intros x.
  unfold filter_map, map_union. destruct (f x), (m x), (m' x); reflexivity.
Qed.

Lemma filter_union_orb :
  forall A f f' (m : partial_map A),
  map_union (filter_map f m) (filter_map f' m) =
  filter_map (fun x => f x || f' x) m.
Proof.
  intros A f f' m. apply functional_extensionality. intros x.
  unfold map_union, filter_map. destruct (f x), (f' x), (m x); reflexivity.
Qed.

Lemma no_conflict_filter_l :
  forall A (m m' : partial_map A) f,
  no_conflict m m' -> no_conflict (filter_map f m) m'.
Proof.
  intros A m m' f Hc x v v' H H'. eapply inclusion_filter_1 in H; eauto.
  reflexivity.
Qed.

Lemma no_conflict_filter_r :
  forall A (m m' : partial_map A) f,
  no_conflict m m' -> no_conflict m (filter_map f m').
Proof.
  intros A m m' f Hc x v v' H H'. eapply inclusion_filter_1 in H'; eauto.
  reflexivity.
Qed.

Lemma filter_unchanged :
  forall A (m : partial_map A) f,
  (forall x, defined_on x m -> f x = true) -> filter_map f m = m.
Proof.
  intros A m f H. apply functional_extensionality. intros x. unfold filter_map.
  destruct (f x) eqn:E; auto.
  symmetry. apply undefined_None. intros Hc. apply H in Hc. congruence.
Qed.

Lemma inclusion_filter_update :
  forall A (m m' : partial_map A) x v f f',
  inclusion m m' ->
  (forall x', x' <> x -> f x' = true -> f' x' = true) ->
  inclusion (filter_map f (x |-> v; m)) (x |-> v; filter_map f' m').
Proof.
  intros A m m' x v f f' Hi Hf x' v' H.
  unfold update, t_update, filter_map in *.
  bdestruct (eqb_string x x'); destruct (f x') eqn:E, (f' x') eqn:E';
  auto; try discriminate.
  rewrite Hf in E'; congruence.
Qed.
