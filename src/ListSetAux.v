From Coq Require Export ListSet.
From Coq Require Import List Basics Relations RelationClasses.
From VFA Require Import Perm.
From Qunity Require Import BoolAux.
Import ListNotations.

Set Implicit Arguments.

(* This file contains added definitions based on Coq's ListSet *)

Section definitions.

  Variable A : Type.

  (* Is everything in the first set also in the second? *)
  Inductive subset : set A -> set A -> Prop :=
    | subset_empty s :
        subset [] s
    | subset_cons e s s' :
        set_In e s' ->
        subset s s' ->
        subset (e :: s) s'.

  (* Do two sets contain the same elements? *)
  Definition eq_set (s s' : set A) := subset s s' /\ subset s' s.

  (* Are two sets disjoint? *)
  Inductive disjoint : set A -> set A -> Prop :=
    | disjoint_empty s :
        disjoint [] s
    | disjoint_cons e s s' :
        ~ set_In e s' ->
        disjoint s s' ->
        disjoint (e :: s) s'.

  Section quantifiers.

    (* Separating this out makes it easier to use in some places *)
    Variable P : A -> Prop.

    (* Exists, but as a Fixpoint *)
    Fixpoint set_Exists (s : set A) :=
      match s with
      | [] => False
      | e :: s' => P e \/ set_Exists s'
      end.

    (* Forall, but as a Fixpoint *)
    Fixpoint set_Forall (s : set A) :=
      match s with
      | [] => True
      | e :: s' => P e /\ set_Forall s'
      end.

    (* If P is True for one element, it's true for all of them *)
    Definition IffAll s := Exists P s -> Forall P s.

  End quantifiers.

  Section eqb.

    Variable eqb : A -> A -> bool.

    (* Are two lists equivalent using eqb? *)
    Fixpoint eqb_list (l l' : list A) :=
      match l, l' with
      | [], [] => true
      | e :: l, e' :: l' => eqb e e' && eqb_list l l'
      | _, _ => false
      end.

    (* Like set_add but using eqb *)
    Fixpoint set_add_bool e (s : set A) : set A :=
      match s with
      | [] => [e]
      | e' :: s' => if eqb e e' then s else e' :: set_add_bool e s'
      end.

    (* Like set_mem but using eqb *)
    Fixpoint set_mem_bool (e : A) (s : set A) :=
      match s with
      | [] => false
      | e' :: s' => eqb e e' || set_mem_bool e s'
      end.

    (* Like set_union but using eqb *)
    Fixpoint set_union_bool (s s' : set A) :=
      match s' with
      | [] => s
      | e' :: t' => set_add_bool e' (set_union_bool s t')
      end.

    (* Like set_diff but using eqb *)
    Fixpoint set_diff_bool (s s' : set A) : set A :=
      match s with
      | [] => []
      | e :: t =>
          if set_mem_bool e s'
          then set_diff_bool t s'
          else set_add_bool e (set_diff_bool t s')
      end.

    (* Is everything in s also in s'? *)
    Fixpoint subset_bool (s s' : set A) :=
      match s with
      | [] => true
      | e :: t => set_mem_bool e s' && subset_bool t s'
      end.

    (* Do two sets contain the same elements? *)
    Definition eqb_set (s s' : set A) :=
      subset_bool s s' && subset_bool s' s.

    (* Are two sets disjoint? *)
    Fixpoint disjoint_bool (s s' : set A) :=
      match s with
      | [] => true
      | e :: t => negb (set_mem_bool e s') && disjoint_bool t s'
      end.

  End eqb.

  (* A computable boolean for ForallOrdPairs *)
  Fixpoint forall_ord_pairs f (s : set A) :=
    match s with
    | [] => true
    | e :: s' => forallb (f e) s' && forall_ord_pairs f s'
    end.

  (* Is the truth of f the same for all elements in the set? *)
  Definition iff_all (f : A -> bool) (s : set A) :=
    match s with
    | [] => true
    | e :: s' => if f e then forallb f s' else negb (existsb f s')
    end.

End definitions.

Section big_union.

  Variable A A' : Type.

  Variable eqb' : A' -> A' -> bool.

  Variable f : A -> set A'.

  (* Kind of like flat_map, but taking a big union instead of concatenating *)
  Fixpoint big_union (s : set A) : set A' :=
    match s with
    | [] => []
    | e :: s' => set_union_bool eqb' (f e) (big_union s')
    end.

End big_union.

#[export]
Hint Unfold eq_set IffAll : core.

Class eqb_equiv {A} (eqb : A -> A -> bool) :=
  { eqb_refl : forall e, eqb e e = true;
    eqb_sym : forall e e', eqb e e' = eqb e' e;
    eqb_trans :
    forall e1 e2 e3, eqb e1 e2 = true -> eqb e2 e3 = true -> eqb e1 e3 = true }.

Instance eqb_equiv_Equivalence :
  forall A (R : relation A) eqb,
  (forall e e', reflect (R e e') (eqb e e')) -> eqb_equiv eqb -> Equivalence R.
Proof.
  intros A R eqb Hf [Hr Hs Ht]. split.
  - intros e. specialize (Hr e). bdestruct (eqb e e); congruence.
  - intros e e' H. specialize (Hs e e').
    bdestruct (eqb e e'); bdestruct (eqb e' e); congruence.
  - intros e1 e2 e3 H1 H3. specialize (Ht e1 e2 e3).
    bdestruct (eqb e1 e2); bdestruct (eqb e2 e3); bdestruct (eqb e1 e3); auto;
    try discriminate Ht; auto.
Qed.

Instance Equivalence_eqb_equiv :
  forall A (R : relation A) eqb,
  (forall e e', reflect (R e e') (eqb e e')) -> Equivalence R -> eqb_equiv eqb.
Proof.
  intros A R eqb Hf [Hr Hs Ht]. split.
  - intros e. bdestruct (eqb e e); auto.
  - intros e e'. bdestruct (eqb e e'); bdestruct (eqb e' e); auto.
    symmetry in H. contradiction.
  - intros e1 e2 e3 H1 H3.
    bdestruct (eqb e1 e2); bdestruct (eqb e2 e3); try discriminate.
    bdestruct (eqb e1 e3); auto.
    exfalso. apply H2. transitivity e2; assumption.
Qed.

Lemma eqb_equiv_Equivalence_iff :
  forall A (R : relation A) eqb,
  (forall e e', reflect (R e e') (eqb e e')) -> eqb_equiv eqb <-> Equivalence R.
Proof.
  intros A R eqb H.
  split; eauto using eqb_equiv_Equivalence, Equivalence_eqb_equiv.
Qed.

Class eqb_correct {A} (P : A -> Prop) eqb :=
  { eqb_correct_equiv : eqb_equiv eqb;
    eqb_correct_eq : forall e e', P e -> P e' -> reflect (e = e') (eqb e e') }.

Class eqb_all_correct {A} (eqb : A -> A -> bool) :=
  eqb_reflects : forall e e', reflect (e = e') (eqb e e').

#[export]
Hint Unfold const eqb_all_correct : core.

Instance eqb_all_correct_equiv :
  forall A (eqb : A -> A -> bool), eqb_all_correct eqb -> eqb_equiv eqb.
Proof.
  intros A eqb H. split.
  - intros e. bdestruct (eqb e e); congruence.
  - intros e e'. bdestruct (eqb e e'); bdestruct (eqb e' e); congruence.
  - intros e1 e2 e3 H1 H3. bdestruct (eqb e1 e2); congruence.
Qed.

Instance eqb_all_correct_1 :
  forall A (P : A -> Prop) eqb, eqb_all_correct eqb -> eqb_correct P eqb.
Proof.
  intros A P eqb H. split; auto using eqb_all_correct_equiv.
Qed.

Instance eqb_all_correct_2 :
  forall A (eqb : A -> A -> bool),
  eqb_correct (const True) eqb -> eqb_all_correct eqb.
Proof.
  intros A eqb [H Hp] e e'. auto.
Qed.

#[export]
Hint Resolve eqb_all_correct_1 eqb_all_correct_2 : bdestruct.

Lemma subset_nil_2 :
  forall A (s : set A), subset s [] -> s = [].
Proof.
  intros A s H. inversion H; auto.
  contradiction.
Qed.

Lemma subset_cons_2 :
  forall A s s' (e : A), subset s s' -> subset s (e :: s').
Proof.
  intros A s s' e' H.
  induction H as [s' | e s s' Hi H IH]; constructor; simpl; auto.
Qed.

Lemma subset_cons_3 :
  forall A s s' (e : A), subset s s' -> subset (e :: s) (e :: s').
Proof.
  intros A s s' e H. constructor; simpl; auto using subset_cons_2.
Qed.

Lemma subset_spec :
  forall A (s s' : set A), subset s s' <-> Forall (fun e => set_In e s') s.
Proof.
  intros s s'. split; intros H; induction H; auto using subset.
Qed.

Instance subset_refl :
  forall A, Reflexive (subset (A := A)).
Proof.
  intros A s.
  induction s as [| e s IH]; constructor; simpl; auto using subset_cons_2.
Qed.

Instance subset_trans :
  forall A, Transitive (subset (A := A)).
Proof.
  intros A s1 s2 s3 H1 H3. rewrite subset_spec, Forall_forall in *.
  intros e H. apply H3, H1, H.
Qed.

Lemma Forall_True :
  forall A (s : set A), Forall (const True) s.
Proof.
  intros A s. induction s as [| e s IH]; constructor; auto.
Qed.

Lemma Exists_subset :
  forall A P (s s' : set A), subset s s' -> Exists P s -> Exists P s'.
Proof.
  intros A P s s' Hs He. apply Exists_exists in He as [e [Hi He]].
  apply Exists_exists. exists e. split; auto.
  apply subset_spec in Hs. rewrite Forall_forall in Hs. apply Hs, Hi.
Qed.

Lemma Forall_subset :
  forall A P (s s' : set A), subset s s' -> Forall P s' -> Forall P s.
Proof.
  intros A P s s' H H'. induction H as [s' | e s s' Hi H IH]; constructor; auto.
  rewrite Forall_forall in H'. auto.
Qed.

Lemma add_subset_1 :
  forall A eqb s (e : A), subset s (set_add_bool eqb e s).
Proof.
  intros A eqb s e.
  induction s as [| e' s IH]; constructor;
  simpl; destruct (eqb e e'); simpl; auto;
  apply subset_cons_2; auto.
  reflexivity.
Qed.

Lemma add_subset_cons :
  forall A eqb s s' (e : A),
  subset s s' -> subset (set_add_bool eqb e s) (e :: s).
Proof.
  intros A eqb s s' e H. induction H as [s' | e' s s' He H IH].
  - reflexivity.
  - simpl. destruct (eqb e e').
    + apply subset_cons_2. reflexivity.
    + constructor; simpl; auto.
      transitivity (e :: s); auto.
      constructor; simpl; auto.
      repeat apply subset_cons_2. reflexivity.
Qed.

Lemma diff_subset :
  forall A eqb (s s' : set A), subset (set_diff_bool eqb s s') s.
Proof.
  intros A eqb s s'. induction s as [| e s IH]; simpl; auto using subset.
  destruct (set_mem_bool eqb e s'); auto using subset_cons_2.
  transitivity (e :: set_diff_bool eqb s s');
  eauto using add_subset_cons, subset_cons_3.
Qed.

Lemma forallb_spec :
  forall A P f (s : set A),
  (forall e, reflect (P e) (f e)) ->
  reflect (Forall P s) (forallb f s).
Proof.
  intros A P f s H. induction s as [| e s IH]; repeat constructor.
  simpl. bdestruct (f e); bdestruct (forallb f s); constructor; auto;
  intros Hc; inversion Hc; contradiction.
Qed.

#[export]
Hint Resolve forallb_spec : bdestruct.

Lemma forallb_andb :
  forall A f f' (s : set A),
  forallb (fun e => f e && f' e) s = forallb f s && forallb f' s.
Proof.
  intros A f f' s. induction s as [| e s IH]; auto.
  simpl. rewrite IH.
  repeat rewrite andb_assoc. f_equal.
  repeat rewrite <- andb_assoc. f_equal.
  apply andb_comm.
Qed.

Lemma existsb_spec :
  forall A P f (s : set A),
  (forall e, reflect (P e) (f e)) ->
  reflect (Exists P s) (existsb f s).
Proof.
  intros A P f s H. apply iff_reflect. rewrite Exists_exists, existsb_exists.
  split; intros [e [Hi He]]; exists e; split; auto; bdestruct (f e); auto.
  discriminate.
Qed.

#[export]
Hint Resolve existsb_spec : bdestruct.

Lemma eqb_list_refl :
  forall A eqb (l : list A),
  eqb_equiv eqb ->
  eqb_list eqb l l = true.
Proof.
  intros A eqb l [H _ _]. induction l as [| e l IH]; auto.
  simpl. rewrite H. assumption.
Qed.

Lemma eqb_list_spec :
  forall A (P : A -> Prop) eqb l l',
  eqb_correct P eqb ->
  Forall P l ->
  Forall P l' ->
  reflect (l = l') (eqb_list eqb l l').
Proof.
  intros A P eqb l l' [Hq Hr] Hl H'.
  apply iff_reflect. split; intros H; subst; eauto using eqb_list_refl.
  generalize dependent l'.
  induction Hl as [| h l Hh Hl IH]; simpl; intros [| h' l'] H' H;
  try discriminate; auto.
  inversion_clear H'. bdestruct (eqb h h'); try discriminate.
  subst. f_equal. apply IH; assumption.
Qed.

Lemma Forall_add :
  forall A P eqb s (e : A),
  Forall P s ->
  P e ->
  Forall P (set_add_bool eqb e s).
Proof.
  intros A P eqb s e Hs He. induction Hs as [| e' s H' Hs IH]; simpl; auto.
  destruct (eqb e e'); auto.
Qed.

Lemma set_add_bool_spec :
  forall A P eqb s (e e' : A), 
  eqb_correct P eqb ->
  Forall P s ->
  P e ->
  P e' ->
  set_In e (set_add_bool eqb e' s) <-> e = e' \/ set_In e s.
Proof.
  intros A P eqb s e e' [Hq Hr] Hs He H'. induction Hs as [| h s Hh Hs IH]; simpl.
  - split; intros [Hs | Hf]; subst; auto.
  - bdestruct (eqb e' h).
    + subst. split; auto.
      intros [H0 | Ho]; simpl; auto.
    + simpl. rewrite IH. split; intros [Hi | [Hi | Hi]]; subst; auto.
Qed.

Corollary set_add_bool_all_spec :
  forall A eqb s (e e' : A),
  eqb_all_correct eqb ->
  set_In e (set_add_bool eqb e' s) <-> e = e' \/ set_In e s.
Proof.
  intros A eqb s e e' Hq.
  eapply set_add_bool_spec; eauto using eqb_all_correct_1, Forall_True.
Qed.

Lemma set_add_alt :
  forall A eqb s (e : A) H, 
  eqb_all_correct eqb ->
  set_add_bool eqb e s = set_add H e s.
Proof.
  intros A eqb s e H Hr. induction s as [| e' s IH]; auto.
  simpl. bdestruct (eqb e e'); destruct (H e e'); try contradiction.
  - reflexivity.
  - f_equal. assumption.
Qed.

Lemma set_mem_bool_spec :
  forall A P eqb s (e : A), 
  eqb_correct P eqb ->
  Forall P s ->
  P e ->
  reflect (set_In e s) (set_mem_bool eqb e s).
Proof.
  intros A P eqb s e [Hq Hr] Hs He. apply iff_reflect.
  induction Hs as [| e' s H' Hs [IH1 IH2]]; simpl.
  - split.
    + contradiction. 
    + discriminate.
  - rewrite orb_true_iff.
    split; intros [H | H]; subst; eauto using eqb_refl.
    left. bdestruct (eqb e e'); congruence.
Qed.

Corollary set_mem_bool_all_spec :
  forall A eqb s (e : A),
  eqb_all_correct eqb ->
  reflect (set_In e s) (set_mem_bool eqb e s).
Proof.
  intros A eqb s e H.
  eapply set_mem_bool_spec; auto using eqb_all_correct_1, Forall_True.
Qed.

Lemma set_mem_bool_existsb :
  forall A eqb s (e : A),
  set_mem_bool eqb e s = existsb (eqb e) s.
Proof.
  intros A eqb s e. induction s as [| e' s IH]; auto.
  simpl. rewrite IH. reflexivity.
Qed.

#[export]
Hint Resolve set_mem_bool_spec set_mem_bool_all_spec : bdestruct.

Lemma set_mem_alt :
  forall A eqb s (e : A) H,
  eqb_all_correct eqb ->
  set_mem_bool eqb e s = set_mem H e s.
Proof.
  intros A eqb s e H Hq. induction s as [| e' s IH]; auto.
  simpl. bdestruct (eqb e e'); destruct (H e e'); auto.
  contradiction.
Qed.

Lemma Forall_union :
  forall A P eqb (s s' : set A),
  Forall P s -> Forall P s' -> Forall P (set_union_bool eqb s s').
Proof.
  intros A P eqb s s' H H'.
  induction H' as [| e' s' He H' IH]; simpl; auto using Forall_add.
Qed.

Lemma Forall_big_union :
  forall A A' P eqb' (f : A -> set A') s,
  Forall (fun e => Forall P (f e)) s ->
  Forall P (big_union eqb' f s).
Proof.
  intros A A' P eqb' f s H. induction H as [| e s He H IH]; simpl; auto.
  apply Forall_union; assumption.
Qed.

Lemma set_union_bool_spec :
  forall A P eqb s s' (e : A),
  eqb_correct P eqb ->
  Forall P s ->
  Forall P s' ->
  P e ->
  set_In e (set_union_bool eqb s s') <-> set_In e s \/ set_In e s'.
Proof.
  intros A P eqb s s' e Hq Hs H' He. induction H' as [| e' s' Hp H' IH]; simpl.
  - tauto.
  - rewrite set_add_bool_spec; eauto.
    + rewrite IH. split; intros [H | [H | H]]; subst; auto.
    + apply Forall_union; assumption.
Qed.

Corollary set_union_bool_all_spec :
  forall A eqb s s' (e : A),
  eqb_all_correct eqb ->
  set_In e (set_union_bool eqb s s') <-> set_In e s \/ set_In e s'.
Proof.
  intros A eqb s s' e H.
  eapply set_union_bool_spec; auto using eqb_all_correct_1, Forall_True.
Qed.

Lemma set_union_alt :
  forall s s' H,
  eqb_all_correct eqb ->
  set_union_bool eqb s s' = set_union H s s'.
Proof.
  intros s s' H Hr. induction s' as [| e' s' IH]; auto.
  simpl. erewrite <- set_add_alt by eauto. f_equal. assumption.
Qed.

Lemma set_diff_bool_spec :
  forall A P eqb s s' (e : A),
  eqb_correct P eqb ->
  Forall P s ->
  Forall P s' ->
  P e ->
  set_In e (set_diff_bool eqb s s') <-> set_In e s /\ ~ set_In e s'.
Proof.
  intros A P eqb s s' e Hq Hs H' He.
  induction Hs as [| e' s Hp Hs IH].
  - tauto.
  - simpl. bdestruct (set_mem_bool eqb e' s').
    + rewrite IH. split.
      * tauto.
      * intros [[Hi | Hi] Hn]; subst; auto.
        contradiction.
    + rewrite set_add_bool_spec; eauto using Forall_subset, diff_subset.
      rewrite IH. split.
      * intros [Hi | [Hi Hn]]; subst; auto.
      * intros [[Hi | Hi] Hn]; subst; auto.
Qed.

Corollary set_diff_bool_all_spec :
  forall A eqb s s' (e : A),
  eqb_all_correct eqb ->
  set_In e (set_diff_bool eqb s s') <-> set_In e s /\ ~ set_In e s'.
Proof.
  intros A eqb s s' e H.
  eapply set_diff_bool_spec; eauto using eqb_all_correct_1, Forall_True.
Qed.

Lemma set_diff_alt :
  forall A eqb (s s' : set A) H,
  eqb_all_correct eqb ->
  set_diff_bool eqb s s' = set_diff H s s'.
Proof.
  intros A eqb s s' H Hr. induction s as [| e s IH]; auto.
  simpl. erewrite <- set_mem_alt, <- set_add_alt, IH; eauto.
Qed.

Lemma set_mem_set_diff :
  forall A P eqb s s' (e : A),
  eqb_correct P eqb ->
  Forall P s ->
  Forall P s' ->
  P e ->
  set_mem_bool eqb e (set_diff_bool eqb s s') =
  set_mem_bool eqb e s && negb (set_mem_bool eqb e s').
Proof.
  intros A P eqb s s' e Hp Hs H' He.
  set (sd := set_diff_bool eqb s s').
  assert (Forall P sd) as Hd.
  { eapply Forall_subset.
    - apply diff_subset.
    - assumption. }
  bdestruct (set_mem_bool eqb e sd);
  bdestruct (set_mem_bool eqb e s);
  bdestruct (set_mem_bool eqb e s');
  auto; exfalso; auto; subst sd; rewrite set_diff_bool_spec in H; eauto;
  destruct H; contradiction.
Qed.

Corollary set_mem_set_diff_all :
  forall A eqb s s' (e : A),
  eqb_all_correct eqb ->
  set_mem_bool eqb e (set_diff_bool eqb s s') =
  set_mem_bool eqb e s && negb (set_mem_bool eqb e s').
Proof. eauto using set_mem_set_diff, eqb_all_correct_1, Forall_True. Qed.

Lemma subset_bool_spec :
  forall A P eqb (s s' : set A),
  eqb_correct P eqb ->
  Forall P s ->
  Forall P s' ->
  reflect (subset s s') (subset_bool eqb s s').
Proof.
  intros A P eqb s s' Hp Hs H'. apply iff_reflect.
  induction Hs as [| e s He Hs IH]; simpl; split; auto using subset.
  - intros H. inversion_clear H. apply IH in H1. rewrite H1.
    bdestruct (set_mem_bool eqb e s'); auto.
  - intros H. apply andb_prop in H as [Hm Hu]. constructor.
    + bdestruct (set_mem_bool eqb e s'); congruence.
    + apply IH, Hu.
Qed.

Corollary subset_bool_all_spec :
  forall A eqb (s s' : set A),
  eqb_all_correct eqb ->
  reflect (subset s s') (subset_bool eqb s s').
Proof. eauto using subset_bool_spec, eqb_all_correct_1, Forall_True. Qed.

Lemma eqb_set_spec :
  forall A P eqb (s s' : set A),
  eqb_correct P eqb ->
  Forall P s ->
  Forall P s' ->
  reflect (eq_set s s') (eqb_set eqb s s').
Proof.
  intros A P eqb s s' H Hs H'. unfold eq_set, eqb_set.
  apply andb_spec; eauto using subset_bool_spec.
Qed.

Corollary eqb_set_all_spec :
  forall A eqb (s s' : set A),
  eqb_all_correct eqb ->
  reflect (eq_set s s') (eqb_set eqb s s').
Proof. eauto using eqb_set_spec, eqb_all_correct_1, Forall_True. Qed.

Lemma disjoint_bool_spec :
  forall A P eqb (s s' : set A),
  eqb_correct P eqb ->
  Forall P s ->
  Forall P s' ->
  reflect (disjoint s s') (disjoint_bool eqb s s').
Proof.
  intros A P eqb s s' Hq Hs H'. apply iff_reflect. split.
  - intros H. induction H as [s' | e s s' He H IH]; auto.
    simpl. inversion_clear Hs.
    bdestruct (set_mem_bool eqb e s').
    + contradiction.
    + apply IH; assumption.
  - intros H. induction Hs as [| e s He Hs IH]; auto using disjoint.
    simpl in H. apply andb_prop in H as [Hm H]. constructor; auto.
    bdestruct (set_mem_bool eqb e s'); auto.
    discriminate.
Qed.

Corollary disjoint_bool_all_spec :
  forall A eqb (s s' : set A),
  eqb_all_correct eqb ->
  reflect (disjoint s s') (disjoint_bool eqb s s').
Proof. eauto using disjoint_bool_spec, eqb_all_correct_1, Forall_True. Qed.

Lemma forall_ord_pairs_spec :
  forall A (s : set A) R f,
  (forall e e', reflect (R e e') (f e e')) ->
  reflect (ForallOrdPairs R s) (forall_ord_pairs f s).
Proof.
  intros A s R f H. induction s as [| e s IH]; repeat constructor.
  simpl. bdestruct (forallb (f e) s).
  - bdestruct (forall_ord_pairs f s); auto using reflect, ForallOrdPairs.
    constructor. intros Hc. inversion Hc. contradiction.
  - constructor. intros Hc. inversion Hc. contradiction.
Qed.

Lemma forall_ord_pairs_andb :
  forall A f f' (s : set A),
  forall_ord_pairs (fun e e' => f e e' && f' e e') s =
  forall_ord_pairs f s && forall_ord_pairs f' s.
Proof.
  intros A f f' s. induction s as [| e s IH]; auto.
  simpl. rewrite IH, forallb_andb.
  repeat rewrite andb_assoc. f_equal.
  repeat rewrite <- andb_assoc. f_equal.
  apply andb_comm.
Qed.

Lemma set_Exists_spec :
  forall A P (s : set A), set_Exists P s <-> Exists P s.
Proof.
  intros A P s. split.
  - induction s as [| e s IH].
    + contradiction.
    + intros [H | H]; auto.
  - intros H. induction H as [e s H | e s IH]; simpl; auto.
Qed.

Lemma set_Forall_spec :
  forall A P (s : set A), set_Forall P s <-> Forall P s.
Proof.
  intros A P s. split.
  - induction s as [| e s IH].
    + auto.
    + intros [He H]. auto.
  - intros H. induction H as [| e s He H IH]; constructor; assumption.
Qed.

Lemma IffAll_nil :
  forall A (P : A -> Prop), IffAll P [].
Proof.
  intros A P H. inversion H.
Qed.

Lemma IffAll_cons :
  forall A P s (e : A),
  IffAll P (e :: s) <-> (P e -> Forall P s) /\ (Exists P s -> P e).
Proof.
  intros A P s e. split.
  - intros H.
    split; intros He; assert (Exists P (e :: s)) as Hx by auto;
    apply H in Hx; inversion Hx; assumption.
  - intros [Hf He] H. inversion H; subst; auto.
Qed.

Lemma IffAll_cons_forall :
  forall A P s (e : A),
  IffAll P (e :: s) <-> Forall (fun e' => P e <-> P e') s.
Proof.
  intros A P s e. rewrite IffAll_cons. split.
  - intros [Hf He]. apply Forall_forall. intros e' H'.
    split; intros Hp.
    + apply Hf in Hp. rewrite Forall_forall in Hp. auto.
    + apply He, Exists_exists. exists e'. auto.
  - intros H. split; intros He; rewrite Forall_forall in *.
    + intros e' H'. apply H; assumption.
    + apply Exists_exists in He as [e' [Hi Hp]]. apply H in Hp; assumption.
Qed.

Lemma IffAll_cons_IffAll :
  forall A P s (e : A),
  IffAll P (e :: s) -> IffAll P s.
Proof.
  intros A P s e. rewrite IffAll_cons. intros [Hf He] H. auto.
Qed.

Lemma IffAll_ord :
  forall A P (s : set A),
  IffAll P s <-> ForallOrdPairs (fun e e' => P e <-> P e') s.
Proof.
  intros A P s. split.
  - intros H.
    induction s as [| e0 s IH]; constructor; eauto using IffAll_cons_IffAll.
    apply IffAll_cons in H as [Hf He].
    apply Forall_forall. intros e Hi. split.
    + intros H0. apply Forall_forall with (l := s); auto.
    + intros H. apply He, Exists_exists. exists e. auto.
  - intros H. induction H as [| e s Hf H IH].
    + apply IffAll_nil.
    + apply IffAll_cons_forall, Hf.
Qed.

Lemma IffAll_iff :
  forall A P (s : set A),
  IffAll P s <-> ForallPairs (fun e e' => P e <-> P e') s.
Proof.
  intros A P s. split.
  - intros H. apply ForallOrdPairs_ForallPairs.
    + intros e. reflexivity.
    + intros e e' He. split; apply He.
    + apply IffAll_ord, H.
  - intros H. apply IffAll_ord, ForallPairs_ForallOrdPairs, H.
Qed.

Lemma iff_all_spec :
  forall A (s : set A) P f,
  (forall e, reflect (P e) (f e)) ->
  reflect (IffAll P s) (iff_all f s).
Proof.
  intros A [| e s]; simpl; auto using reflect, IffAll_nil.
  intros P f Hr. bdestruct (f e).
  - bdestruct (forallb f s); constructor.
    + constructor; auto.
    + intros Hc. assert (Exists P (e :: s)) as He by auto. apply Hc in He.
      inversion He. contradiction.
  - bdestruct (existsb f s); constructor.
    + intros Hc. assert (Exists P (e :: s)) as He by auto. apply Hc in He.
      inversion He. contradiction.
    + intros Hc. inversion Hc; subst; contradiction.
Qed.

Lemma eq_set_spec :
  forall A (s s' : set A), eq_set s s' <-> forall e, set_In e s <-> set_In e s'.
Proof.
  intros A s s'. split.
  - intros [H H'] e. split.
    + generalize dependent e. apply Forall_forall, subset_spec, H.
    + generalize dependent e. apply Forall_forall, subset_spec, H'.
  - intros H.
    split; apply subset_spec, Forall_forall; intros e He; apply H, He.
Qed.

Lemma disjoint_spec :
  forall A s s', disjoint s s' <-> forall e : A, In e s -> In e s' -> False.
Proof.
  intros A s s'. split.
  - intros H.
    induction H; intros e' [] H'; subst; eauto.
  - induction s as [| e s IH]; intros H; constructor.
    + intros Hc. apply H in Hc; simpl; auto.
    + apply IH. intros e' Hi. apply H. simpl. auto.
Qed.

Lemma big_union_spec :
  forall A A' P eqb' (f : A -> set A') s e',
  eqb_correct P eqb' ->
  Forall (fun e => Forall P (f e)) s ->
  P e' ->
  set_In e' (big_union eqb' f s) <-> Exists (fun e => set_In e' (f e)) s.
Proof.
  intros A A' P eqb' f s e' Hq Hs He. split.
  - induction Hs as [| e s Hp Hs IH].
    + contradiction.
    + simpl. rewrite set_union_bool_spec by eauto using Forall_big_union.
      intros [H | H]; auto.
  - intros H.
    induction H as [e s H | e s IH]; inversion_clear Hs; simpl;
    rewrite set_union_bool_spec; eauto using Forall_big_union.
Qed.

Corollary big_union_all_spec :
  forall A A' eqb' (f : A -> set A') s e',
  eqb_all_correct eqb' ->
  set_In e' (big_union eqb' f s) <-> Exists (fun e => set_In e' (f e)) s.
Proof.
  intros A A' eqb' f s e' H.
  eapply big_union_spec; eauto using eqb_all_correct_1.
  induction s as [| e s IH]; constructor;
  auto using eqb_all_correct_1, Forall_True.
  constructor.
Qed.

#[export]
Hint Resolve
  eqb_list_spec set_mem_bool_spec forall_ord_pairs_spec iff_all_spec :
  bdestruct.
