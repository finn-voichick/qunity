From Coq Require Import
  Bool List Basics FunctionalExtensionality Relations RelationClasses Psatz.
From VFA Require Import Perm.
From Qunity Require Import BoolAux ListAux RealAux ComplexAux MapsAux.
Import Datatypes (id) ListNotations.

Set Bullet Behavior "Strict Subproofs".
Set Implicit Arguments.

Open Scope R_scope.
Open Scope C_scope.

Class vector_space V :=
  { zero : V;
    vplus : V -> V -> V;
    scale : C -> V -> V }.

Instance complex_space : vector_space C :=
  {| zero := 0 : C;
     vplus := Cplus;
     scale := Cmult |}.

Notation "0" := zero.
  
Infix "+" := vplus.

Infix "*" := scale.

Notation "- v" := ((-1) * v).

Notation "v - v'" := (v + -v').

Instance fun_space A {V} `(vector_space V) : vector_space (A -> V) :=
  {| zero e := 0;
     vplus v v' e := v e + v' e;
     scale z v e := z * v e |}.

Definition vec A := A -> C.

Instance vec_space A : vector_space (vec A) := fun_space A complex_space.

Fixpoint vsum {A V} `{vector_space V} f (b : set A) : V :=
  match b with
  | [] => 0
  | e :: b' => f e + vsum f b'
  end.

Definition vec_sum {A A'} (f : A -> vec A') b := vsum f b.

Record inner_space V :=
  { inner : V -> V -> C;
    basis : set V }.

Definition complex_inner : inner_space C :=
  {| inner z z' := Cconj z * z';
     basis := [1 : C] |}.

Definition delta {A} (d : A -> bool) : vec A :=
  fun e => if d e then 1 else 0.

Definition ket {A} (eqb : A -> A -> bool) e := delta (eqb e).

Section vec_inner.

  Notation "<( z | z' )>" := (inner complex_inner z z').

  Definition vec_inner {A} eqb (b : set A) : inner_space (vec A) :=
    {| inner v v' := csum ( fun e => <( v e | v' e )> ) b;
       basis := map (ket eqb) b |}.

End vec_inner.

Definition riesz {V} `{vector_space V} (s : inner_space V) (f : V -> C) : V :=
  vsum (fun v => Cconj (f v) * v) (basis s).

Section adjoint.

  Context {V V' : Type} `{vector_space V}
    (s : inner_space V) (s' : inner_space V').

  Notation "<( v | v' )>" := (inner s' v v').

  Definition adjoint u v' :=
    riesz s ( fun v => <( v' | u v )> ).

End adjoint.

Section inner.

  Context {V V'} `{vector_space V} `{vector_space V'}
    (s : inner_space V) (s' : inner_space V').

  Notation "<( v | v' )>" := (inner s v v').

  Definition trace (u : V -> V) :=
    csum ( fun v => <( v | u v )> ) (basis s).

  Definition outer v' v v1 :=
    <( v | v1 )> * v'.

  Notation "|( v' >< v )|" := (outer v' v).

  Definition op_inner : inner_space (V -> V') :=
    {| inner u u' := trace (compose (adjoint s s' u) u');
       basis :=
         flat_map
           (fun v => map ( fun v' => |( v' >< v )| ) (basis s')) (basis s) |}.

  Definition inner_eqb (v v' : V) :=
    forallb ( fun v1 => <( v1 | v )> =? <( v1 | v')> ) (basis s).

End inner.

Record tensor_space V :=
  { tensor : V -> V -> option V;
    tensor1 : V -> V;
    tensor2 : V -> V }.

Definition complex_tensor : tensor_space C :=
  {| tensor z z' := Some (z * z');
     tensor1 z := z;
     tensor2 z := z |}.

Section tensor.

  Context {A} (b1 b2 : set A) (t : tensor_space A).

  Notation "e1 <*> e2" := (tensor t e1 e2) (at level 40, left associativity).

  Definition vec_tensor : tensor_space (vec A) :=
    {| tensor v1 v2 :=
         if forallb (fun e1 =>
                     forallb (fun e2 => match e1 <*> e2 with
                                        | None => (v1 e1 =? 0) || (v2 e2 =? 0)
                                        | Some _ => true
                                        end) b2)
                    b1
         then Some (fun e => v1 (tensor1 t e) * v2 (tensor2 t e))
         else None;
       tensor1 v e1 := csum (fun e2 => match e1 <*> e2 with
                                       | None => 0
                                       | Some e => v e
                                       end) b2;
       tensor2 v e2 := csum (fun e1 => match e1 <*> e2 with
                                       | None => 0
                                       | Some e => v e
                                       end) b1 |}.

End tensor.

Section linearize.

  Context {V V'} `{vector_space V'} (s : inner_space V).

  Notation "<( v | v' )>" := (inner s v v').

  Definition linearize (f : V -> V') v :=
    vsum (fun v1 => <( v1 | v )> * f v1) (basis s).

End linearize.

Definition bilinearize {V1 V2 V} `{vector_space V} s1 s2 (f : V1 -> V2 -> V) :=
  linearize s1 (compose (linearize s2) f).

Definition op_tensor {V V'} `{vector_space V'}
  (s1 s2 : inner_space V) s1' s2' t t' :=
  {| tensor u1 u2 :=
       if forallb (fun v1 =>
                   forallb (fun v2 =>
                            match tensor t v1 v2 with
                            | None =>
                                inner_eqb s1' (u1 v1) 0 ||
                                inner_eqb s2' (u2 v2) 0
                            | Some v => true
                            end) (basis s2))
                  (basis s1)
       then Some (fun v =>
                  match tensor t' (u1 (tensor1 t v)) (u2 (tensor2 t v)) with
                  | None => 0
                  | Some v' => v'
                  end)
       else None;
     tensor1 u v1 := vsum (fun v2 => match tensor t v1 v2 with
                                     | None => 0
                                     | Some v => u v
                                     end) (basis s2);
     tensor2 u v2 := vsum (fun v1 => match tensor t v1 v2 with
                                     | None => 0
                                     | Some v => u v
                                     end) (basis s1)
     |}.

Definition tensor_inner {V} (s1 s2 : inner_space V) (t : tensor_space V) :
  inner_space V :=
  {| inner :=
       bilinearize s1 s2
         (fun v1 v2 =>
          inner s1 (tensor1 t v1) (tensor1 t v2) *
          inner s2 (tensor2 t v1) (tensor2 t v2));
     basis :=
       flat_map (fun v1 => map_option (tensor t v1) (basis s2)) (basis s1) |}.

Section eq.

  Variable V : Type.

  Variables R1 R2 R : relation V.

  Infix "=1=" := R1 (at level 70, no associativity).
  Infix "=2=" := R2 (at level 70, no associativity).
  Infix "==" := R (at level 70, no associativity).

  Class valid_vector `(vector_space V) :=
    { vplus_proper : Proper (R ==> R ==> R) vplus;
      vplus_comm : forall v v' : V, v + v' == v' + v;
      vplus_assoc : forall v1 v2 v3, v1 + (v2 + v3) == v1 + v2 + v3;
      vscale_proper : forall z, Proper (R ==> R) (scale z);
      scale_assoc : forall z z' v, z * (z' * v) == (z * z')%C * v;
      zero_spec : forall v, v + zero == v;
      vopp_spec : forall v, v - v == zero;
      one_spec : forall v, 1 * v == v;
      scale_vplus_distr_l : forall z v v', z * (v + v') == z * v + z * v';
      scale_vplus_distr_r : forall z z' v, (z + z')%C * v == z * v + z' * v }.

  Section linear.

    Variable V' : Type.

    Variable R' : relation V'.

    Infix "==" := R'.

    Class linear `(vector_space V) `(vector_space V') (u : V -> V') :=
      { additive : forall v v', u (v + v') == u v + u v';
        homogeneous : forall z v, u (z * v) == z * u v }.

  End linear.

  Definition linear_functional (s : vector_space V) (f : V -> C) :=
    linear eq s complex_space f.

  Class valid_inner `(vector_space V) `(inner_space V) :=
    { inner_proper : Proper (R ==> R ==> eq) inner;
      inner_linear : forall v, linear_functional _ <( v )|;
      inner_conj_sym : forall v v', Cconj <( v | v' )> = <( v' | v )>;
      norm_positive : forall v, 0 <= Re <( v | v )>;
      norm_definite : forall v, <( v | v )> = 0 -> v == zero }.

  Class valid_tensor `(tensor_space V) :=
    { tensor_proper : Proper (R1 ==> R2 ==> R) tensor;
      tensor_inv :
      forall v1 v2,
      let (v1', v2') := untensor (v1 <*> v2) in v1 =1= v1' /\ v2 =2= v2' }.

  Variables p1 p2 p : inner_space V.

  Class valid_eq (s : vector_space V) :=
    { R_equivalence : Equivalence R;
      tensor_proper : Proper (R ==> R ==> R) tensor }.

  Variable s : tensor_space V.

End eq.

  Theorem riesz_representation :
    forall (f : vec A -> C) (v : vec A),
    (forall e e', reflect (e = e') (eqb e e')) ->
    NoDup b ->
    linear (vec_space A) complex_space f ->
    finite_vec b v ->
    <( riesz f | v )> = f v.
  Proof.
    intros f v Hr Hd Hl Hf. symmetry.
    rewrite linear_as_vsum with (eqb := eqb) (s := complex_space) (b := b);
    auto using complex_valid.
    rewrite vsum_csum.
    unfold inner, riesz. f_equal. apply functional_extensionality. intros e.
    rewrite conj_idemp. apply Cmult_comm.
  Qed.

Definition tensor_space {V} (s s' : inner_space V) : 

Class splittable A :=
  {| split

Instance fun_tensor A V `(tensor_space V) : tensor_space (A -> V) :=
fun v v' e => v e <*> v' e.

Instance vec_tensor A A' eqb eqb' (b : set A) (b' : set A') : tensor_space (vec A) :=
  fun v v' =>


Class tensor_space V :=
  tensor : V -> V -> V.

Class vector_space A V :=
  { zero : V;
    vplus : V -> V -> V;
    scale : C -> V -> V;
    inner : set A -> V -> V -> C;
    tensor : set A -> set A -> V -> V -> V;
    eqb : set A -> V -> V -> bool }.

Instance complex_space : vector_space unit C :=
  {| zero := 0 : C;
     vplus := Cplus;
     scale := Cmult;
     inner _ z z' := Cconj z * z';
     tensor _ _ := Cmult;
     eqb _ := eqb_C |}.

Infix "+" := vplus.

Infix "*" := scale.

Notation "- v" := ((-1) * v).

Notation "v - v'" := (v + -v').

(*
Notation "<( v )|" := (inner v).
Notation "<( v | v' )>" := (inner v v').

Infix "<*>" := tensor (at level 40).

Infix "=?" := eqb.
 *)

Class splittable A :=
  split : 

Definition vec A := A -> C.

Instance vec_space A (eqb : A -> A -> bool) (b : set A) : vector_space (A -> C) :=
  {| zero e := (0 : C);
     vplus v v' e := v e + v' e;
     scale z v e := z * v e;
     basis := map (ket eqb) b;
     inner v v' := csum (fun e => Cconj (v e) * v' e) b;
     tensor v v' := 
  |}.

Section eq.

  Variable V : Type.

  Variable R : relation V.

  Infix "==" := R (at level 70, no associativity).

  Class valid_eq (s : vector_space V) :=
    { R_equivalence : Equivalence R;
      vplus_proper : Proper (R ==> R ==> R) vplus;
      vscale_proper : forall z, Proper (R ==> R) (scale z);
      inner_proper : Proper (R ==> R ==> eq) inner;
      tensor_proper : Proper (R ==> R ==> R) tensor }.

  Class valid_vector (s : vector_space V) :=
    { vplus_comm : forall v v' : V, v + v' == v' + v;
      vplus_assoc : forall v1 v2 v3, v1 + (v2 + v3) == v1 + v2 + v3;
      scale_assoc : forall z z' v, z * (z' * v) == (z * z')%C * v;
      zero_spec : forall v, v + zero == v;
      vopp_spec : forall v, v - v == zero;
      one_spec : forall v, 1 * v == v;
      scale_vplus_distr_l : forall z v v', z * (v + v') == z * v + z * v';
      scale_vplus_distr_r : forall z z' v, (z + z')%C * v == z * v + z' * v }.

  Section eq'.

    Variable V' : Type.

    Variable R' : relation V'.

    Infix "==" := R'.

    Class linear (s : vector_space V) (s' : vector_space V') (u : V -> V') :=
      { additive : forall v v', u (v + v') == u v + u v';
        homogeneous : forall z v, u (z * v) == z * u v }.

  End eq'.

  Class valid_inner s :=
    { inner_linear : forall v, linear eq s complex_space (inner v);
      inner_conj_sym : forall v v', Cconj <( v | v' )> = <( v' | v )>;
      norm_positive : forall v, 0 <= Re <( v | v )>;
      norm_definite : forall v, <( v | v )> = 0 -> v == zero
    }.

  Class valid_basis (s : vector_space V) :=
    { basis_normal : Forall (fun e => <( e | e )> = 1) basis;
      basis_orthogonal : ForallOrdPairs (fun e e' => <( e | e' )> = 0) basis
    }.

End eq.

Lemma complex_valid :
  valid_space eq complex_space.
Proof.
  split; intros; csimpl.
  apply Cplus_comm.
Qed.

Section eq.

  Variable V : Type.

  Variable R : relation V.

  Infix "==" := R (at level 70, no associativity).

  Lemma vplus_zero_l :
    forall s (v : V), valid_space R s -> zero + v == v.
  Proof.
    intros s v Hv. rewrite vplus_comm. by assumption. apply Hv.
  Qed.

Lemma vplus_vopp_l :
  forall V s (v : V), valid_space s -> -v + v = zero.
Proof.
  intros V s v Hv. rewrite vplus_comm by assumption. apply Hv.
Qed.

Lemma zero_unique :
  forall V s (v0 v : V),
  valid_space s -> v + v0 = v <-> v0 = zero.
Proof.
  intros V s v0 v Hv. split.
  - intros H. assert (v + v0 - v = v - v) by (f_equal; assumption).
    rewrite vopp_spec in H0 by apply Hv. rewrite <- H0.
    replace (v + v0) with (v0 + v) by apply Hv.
    rewrite <- vplus_assoc, vopp_spec, zero_spec by apply Hv.
    reflexivity.
  - intros H. subst. apply Hv.
Qed.

Hint Rewrite vplus_zero_l vplus_vopp_l zero_unique : vsimpl.

Ltac vsimpl :=
  repeat
    (autorewrite with vsimpl in *;
     try rewrite vplus_assoc; try rewrite scale_assoc;
     try rewrite zero_spec; try rewrite vopp_spec; try rewrite one_spec;
     try rewrite scale_vplus_distr_l; try rewrite scale_vplus_distr_r;
     csimpl).

Lemma vopp_unique :
  forall V s (v v' : V),
  valid_space s -> v + v' = zero <-> v' = -v.
Proof.
  intros V s v v' Hv. split.
  - intros H.
    replace v' with (v' + zero) by apply Hv. replace zero with (v - v) by apply Hv.
    rewrite vplus_assoc by assumption. rewrite vplus_comm in H by assumption.
    rewrite H. vsimpl.
  - intros H. subst. apply Hv.
Qed.

Lemma vplus_eq_reg_l :
  forall V s (v v1 v2 : V), valid_space s -> v + v1 = v + v2 <-> v1 = v2.
Proof.
  intros V s v v1 v2 Hv. split.
  - intros H.
    assert (-v + v + v1 = -v + v + v2).
    { rewrite <- vplus_assoc by assumption. rewrite H. vsimpl. }
    vsimpl.
  - intros H. subst. reflexivity.
Qed.

Lemma vplus_eq_reg_r :
  forall V s (v1 v2 v : V), valid_space s -> v1 + v = v2 + v <-> v1 = v2.
Proof.
  intros V s v1 v2 v Hv.
  replace (v1 + v) with (v + v1) by apply Hv.
  replace (v2 + v) with (v + v2) by apply Hv.
  apply vplus_eq_reg_l. assumption.
Qed.

Lemma scale_0 :
  forall V s (v : V), valid_space s -> 0 * v = zero.
Proof.
  intros V s v Hv.
  assert (0 * v + 0 * v = 0 * v) as H.
  { rewrite <- scale_vplus_distr_r by assumption. csimpl. }
  vsimpl.
Qed.

Lemma scale_zero :
  forall V (s : vector_space V) z, valid_space s -> z * zero = zero.
Proof.
  intros V s z H.
  assert (z * zero + z * zero = z * zero).
  { rewrite <- scale_vplus_distr_l by assumption. vsimpl. }
  vsimpl.
Qed.

Hint Rewrite
  vplus_zero_l vplus_vopp_l zero_unique vplus_eq_reg_l vplus_eq_reg_r
  scale_0 scale_zero :
  vsimpl.

Instance fun_space A {V} (s : vector_space V) :
  vector_space (A -> V) :=
  {| zero e := zero;
     vplus v v' e := v e + v' e;
     scale z v e := z * v e |}.

Definition tensor {A V} (v v' : A -> V) (e : A) :=
  v (fst e) <*> v' (snd e)

Class mergeable A :=
  { merge : A -> A -> A;
    unmerge : A -> A * A }.

(* d is the dimension of the second space *)
Instance nat_pairable d : mergeable nat :=
  {| merge n n' := n * d + n';
     unmerge n := (n / d, n mod d) |}%nat.

Instance map_pairable A : mergeable (partial_map A) :=
  {| merge := map_union;
     unmerge n := (n, n) |}.

Definition valid_pairable {A} (p : mergeable A) :=
  forall e1 e2 e, merge e1 e2 = e <-> unmerge e = (e1, e2).

Instance fun_space A {V} (s : vector_space V) :
  vector_space (A -> V) :=
  {| zero e := zero;
     vplus v v' e := v e + v' e;
     scale z v e := z * v e |}.

Lemma nat_valid_pairable :
  forall d, valid_pairable (nat_pairable (S d)).
Proof.
  intros d n1 n2 n. remember (S d) as d'. simpl. split; intros H.
  - f_equal. bdestruct (n2 <? d'); try discriminate.
    injection H as. subst. symmetry. f_equal.
    + apply Nat.div_unique with (r := n2); lia.
    + apply Nat.mod_unique with (q := n1); lia.
  - injection H as. subst. specialize (Nat.mod_upper_bound n (S d)) as H.
    rewrite <- Nat.ltb_lt in H. rewrite H; auto.
    f_equal. rewrite Nat.mul_comm. symmetry. auto using Nat.div_mod.
Qed.

Definition vec A := A -> C.

Instance vec_space A b `(p : pairable A) : vector_space (vec A) :=
  {| zero e := (0 : C);
     vplus v v' e := v e + v' e;
     scale z v e := z * v e;
     basis := b;
     tensor v v' e := match unpr e with
                      | None => 0
                      | Some (e1, e2) => v e1 * v' e2
                      end
  |}.

Instance fun_valid :
  forall A V (s : vector_space V), valid_space s -> valid_space (fun_space A s).
Proof.
  intros A V s H.
  split; intros; apply functional_extensionality; intros e; apply H.
Qed.

Lemma fun_zero_const :
  forall A V (s : vector_space V), zero = const zero :> (A -> V).
Proof. reflexivity. Qed.

Definition vec A := A -> C.

Instance vec_space A : vector_space (vec A) := fun_space A complex_space.

Instance vec_valid :
  forall A, valid_space (vec_space A).
Proof.
  intros A. apply fun_valid, complex_valid.
Qed.

Definition eq_vec {A} (b : set A) : relation (vec A) :=
  fun v v' => Forall (fun e => v e = v' e) b.

Instance eq_vec_equivalence :
  forall A (b : set A), Equivalence (eq_vec b).
Proof.
  intros A b. split.
  - intros v. apply Forall_forall. reflexivity.
  - intros v v' H. unfold eq_vec in *. rewrite Forall_forall in *.
    intros e He. symmetry. auto.
  - intros v1 v2 v3 H1 H3. unfold eq_vec in *. rewrite Forall_forall in *.
    intros e He. transitivity (v2 e); auto.
Qed.

Lemma eq_vec_nil :
  forall A (v v' : vec A), eq_vec [] v v'.
Proof. constructor. Qed.

(*
Instance eq_vec_vplus :
  forall A (b : set A), Proper (eq_vec b ==> eq_vec b ==> eq_vec b) vplus.
Proof.
  intros A b v1 v1' H1 v2 v2' H2.
  unfold eq_vec in *. rewrite Forall_forall in *.
  intros e He. simpl. f_equal; auto.
Qed.

Instance eq_vec_scale :
  forall A (b : set A) z, Proper (eq_vec b ==> eq_vec b) (scale z).
Proof.
  intros A b z v v' H. unfold eq_vec in *. rewrite Forall_forall in *.
  intros e He. simpl. f_equal; auto.
Qed.
 *)

Class subspace {V} (s : vector_space V) (R : relation V) :=
  { subspace_vplus :> Proper (R ==> R ==> R) vplus;
    subspace_scale z :> Proper (R ==> R) (scale z) }.

Instance subspace_eq_vec :
  forall A b, subspace (vec_space A) (eq_vec b).
Proof.
  intros A b. split.
  - intros v1 v1' H1 v2 v2' H2.
    unfold eq_vec in *. rewrite Forall_forall in *.
    intros e He. simpl. f_equal; auto.
  - intros z v v' H. unfold eq_vec in *. rewrite Forall_forall in *.
    intros e He. simpl. f_equal; auto.
Qed.

Definition eqb_vec {A} (b : set A) (v v' : vec A) :=
  forallb (fun e => v e =? v' e) b.

Lemma eqb_vec_spec :
  forall A (b : set A), reflect2 (eq_vec b) (eqb_vec b).
Proof.
  intros A b v v'. apply forallb_spec. intros e. apply eqb_C_correct.
Qed.

Section ket.

  Variable A : Type.
  
  Variable R : relation A.

  Variable eqb : A -> A -> bool.

  Definition delta (p : A -> bool) : vec A :=
    fun e' => if p e' then 1 else 0.

  Definition ket e := delta (eqb e).

  Notation "|( e )>" := (ket e).

  Lemma ket_inj :
    forall (b : set A) e e',
    Equivalence R ->
    reflect2 R eqb ->
    InA R e b ->
    InA R e' b ->
    eq_vec b |( e )> |( e' )> ->
    R e e'.
  Proof.
    intros b e e' Hq Hr H H' Hk. unfold eq_vec in Hk. rewrite Forall_forall in Hk.
    apply InA_alt in H' as [e0 [H0 H']].
    apply Hk in H'. unfold ket, delta in H'.
    destruct (Hr e e0); destruct (Hr e' e0); csimpl.
    - transitivity e0; auto. solve_relation.
    - lra.
  Qed.

  Lemma ket_eq :
    forall e, eqb_equiv eqb -> |( e )> e = 1.
  Proof.
    intros e H. unfold ket, delta. rewrite eqb_refl. reflexivity.
  Qed.

  Lemma ket_neq :
    forall e e', reflect2 R eqb -> ~ R e e' -> |( e )> e' = 0.
  Proof.
    intros e e' Hr H. unfold ket, delta. destruct (Hr e e'); congruence.
  Qed.

End ket.

Hint Rewrite ket_eq : vsimpl.

Instance ket_proper_app :
  forall A (R : relation A) eqb,
  Equivalence R -> reflect2 R eqb -> Proper (R ==> R ==> eq) (ket eqb).
Proof.
  intros A R eqb Hq Hr e1 e1' H1 e2 e2' H2. unfold ket, delta.
  destruct (Hr e1 e2), (Hr e1' e2'); auto; exfalso; apply n.
  - rewrite <- H1, <- H2. assumption.
  - rewrite H1, H2. assumption.
Qed.

Instance ket_proper_eq_vec :
  forall A R eqb (b : set A),
  Equivalence R -> reflect2 R eqb -> Proper (R ==> eq_vec b) (ket eqb).
Proof.
  intros A R eqb b Hq Hr e e' H. apply Forall_forall. intros e0 _.
  rewrite H. reflexivity.
Qed.

(*
Instance linear_subspace :
  forall V V' (s : vector_space V) (s' : vector_space V'),
  valid_space s ->
  valid_space s' ->
  subspace (fun_space V s') (linear s s').
Proof.
  intros V V' s s' H H'. split.
  - split.
    + intros v1 v2. vsimpl.
    + intros z v. vsimpl.
  - intros u u' [Ha Hh] [Ha' Hh']. split.
    + intros v1 v2. vsimpl. rewrite Ha, Ha'. vsimpl.
      repeat rewrite <- vplus_assoc by assumption. f_equal.
      auto using vplus_comm.
    + intros z v. vsimpl. rewrite Hh, Hh'. reflexivity.
  - intros z u [Ha Hh]. split.
    + intros v1 v2. vsimpl. rewrite Ha. vsimpl.
    + intros z' v. vsimpl. rewrite Hh. vsimpl.
      f_equal. apply Cmult_comm.
Qed.
 *)

Instance compose_linear :
  forall V1 V2 V3 s1 s2 s3 (u1 : V1 -> V2) (u3 : V2 -> V3),
  linear s1 s2 u1 ->
  linear s2 s3 u3 ->
  linear s1 s3 (compose u3 u1).
Proof.
  intros V1 V2 V3 s1 s2 s3 u1 u3 [Ha1 Hh1] [Ha3 Hh3]. split.
  - intros v v'. unfold compose.
    rewrite Ha1, Ha3. reflexivity.
  - intros z v. unfold compose.
    rewrite Hh1, Hh3. reflexivity.
Qed.

Definition functional A := vec A -> C.

Definition functional_space A : vector_space (functional A) :=
  fun_space (vec A) complex_space.

Instance functional_valid :
  forall A, valid_space (functional_space A).
Proof.
  intros A. apply fun_valid, complex_valid.
Qed.

(*
Definition eq_functional {A} (eqb : A -> A -> bool) (b : set A) :
  relation (functional A) :=
  fun f f' => Forall (fun e => f (ket eqb e) = f' (ket eqb e)) b.

Instance eq_functional_equivalence :
  forall A (b : set A), Equivalence (eq_functional b).
Proof.
  intros A b. split.
  - intros f v v' H. unfold eq_vec in H.

Class wf_functional {A} (b : set A) (f : functional A) :=
  { wf_fnl_linear : linear (vec_space A) complex_space f;
    wf_fnl_restricted : forall v : vec A, Forall (fun e => v e = 0) b -> f v = 0 }.

Instance wf_functional_subspace :
  forall A (b : set A), subspace (functional_space A) (wf_functional b).
Proof.
  intros A b. split.
  - split.
    + apply linear_subspace.
      * apply vec_valid.
      * apply complex_valid.
    + reflexivity.
  - intros f f' [Hl H0] [Hl' H0']. split.
    + apply linear_subspace; auto using vec_valid, complex_valid.
    + intros v Hv. simpl. rewrite H0, H0'; csimpl.
  - intros z f [Hl H0]. split.
    + apply linear_subspace; auto using vec_valid, complex_valid.
    + intros v Hv. simpl. rewrite H0; csimpl.
Qed.

Class wf_functional {A} (R : relation A) eqb (b : set A) (f : functional A) :=
  { wf_fnl_linear : linear (vec_space A) complex_space f;
    wf_fnl_restricted : forall e, Exists (R e) b \/ f (ket eqb e) = 0 }.

Instance wf_functional_subspace :
  forall A R eqb b,
  (forall e e', reflect (R e e') (eqb e e')) ->
  subspace (functional_space A) (wf_functional R eqb b).
Proof.
  intros A R eqb b Hr. split.
  - split.
    + apply linear_subspace.
      * apply vec_valid.
      * apply complex_valid.
    + intros e. right. csimpl.
  - intros f f' [Hl H0] [Hl' H0']. split.
    + apply linear_subspace; auto using vec_valid, complex_valid.
    + intros e.
      specialize (H0 e) as [He | H0]; specialize (H0' e) as [He' | H0']; auto.
      right. simpl. rewrite H0, H0'. csimpl.
  - intros z f [Hl H0]. split.
    + apply linear_subspace; auto using vec_valid, complex_valid.
    + intros e. specialize (H0 e) as [He | H0]; auto.
      right. csimpl. rewrite H0. csimpl.
Qed.
 *)

Definition op A A' := vec A -> vec A'.

Instance op_space A A' : vector_space (op A A') :=
  fun_space (vec A) (vec_space A').

(*
Class wf_op {A A'} (R : relation A) R' (b : set A) b' (u : op A A') :=
  { wf_op_linear : linear (vec_space A) (vec_space A') u;
    wf_op_restricted_in : forall v, Forall (fun e => v e = 0) b -> u v = zero;
    wf_op_restricted_out : forall v, wf_vec R b v -> wf_vec R' b' (u v) }.

Class wf_op {A A'} (R : relation A) R' eqb (b : set A) b' (u : op A A') :=
  { wf_op_linear : linear (vec_space A) (vec_space A') u;
    wf_op_restricted_in : forall e, Exists (R e) b \/ u (ket eqb e) = zero;
    wf_op_restricted_out : forall v, wf_vec R b v -> wf_vec R' b' (u v) }.

Instance wf_op_subspace :
  forall A A' R R' b b',
  subspace (op_space A A') (wf_op R R' b b').
Proof.
Abort.

Lemma linear_subspace :
  forall V V' (s : vector_space V) (s' : vector_space V'),
  valid_space s ->
  valid_space s' ->
  subspace (fun_space V s') (linear s s').
Proof.
  intros V V' s s' H H'. split.
  - split.
    + intros v1 v2. vsimpl.
    + intros z v. vsimpl.
  - intros u u' [Ha Hh] [Ha' Hh']. split.
    + intros v1 v2. vsimpl. rewrite Ha, Ha'. vsimpl.
      repeat rewrite <- vplus_assoc by assumption. f_equal.
      auto using vplus_comm.
    + intros z v. vsimpl. rewrite Hh, Hh'. reflexivity.
  - intros z u [Ha Hh]. split.
    + intros v1 v2. vsimpl. rewrite Ha. vsimpl.
    + intros z' v. vsimpl. rewrite Hh. vsimpl.
      f_equal. apply Cmult_comm.
Qed.
 *)

Section inner.

  Variable A : Type.

  Variable b : set A.

  Definition inner (v : vec A) : functional A :=
    fun v' => csum (fun e => Cconj (v e) * v' e) b.

  Notation "<( v )|" := (inner v).

  (*
  Lemma wf_bra :
    forall v, wf_functional b <( v )|.
  Proof.
    intros v. split.
    - split.
      + intros v1 v2. simpl. unfold inner. rewrite <- csum_additive.
        f_equal. apply functional_extensionality. intros e. csimpl.
      + intros z v'. simpl. unfold inner. rewrite <- csum_homogeneous.
        f_equal. apply functional_extensionality. intros e. csimpl.
        f_equal. apply Cmult_comm.
    - intros v' H. unfold inner. apply csum_0.
      induction H as [| e b He H IH]; constructor; auto.
      rewrite He. csimpl.
  Qed.
   *)

  Notation "<( v | v' )>" := (inner v v').

  Lemma inner_linear :
    forall v, linear (vec_space A) complex_space <( v )|.
  Proof.
    intros v. split.
    - intros v1 v2. simpl. unfold inner. rewrite <- csum_additive.
      f_equal. apply functional_extensionality. intros e. csimpl.
    - intros z v'. simpl. unfold inner. rewrite <- csum_homogeneous.
      f_equal. apply functional_extensionality. intros e. csimpl.
      f_equal. apply Cmult_comm.
  Qed.

  Lemma inner_conj_sym :
    forall v v', Cconj <( v | v' )> = <( v' | v )>.
  Proof.
    intros v v'. unfold inner. rewrite conj_csum. unfold compose. f_equal.
    apply functional_extensionality. intros e. vsimpl. apply Cmult_comm.
  Qed.

  Lemma norm_real :
    forall v, is_real <( v | v )>.
  Proof.
    intros v. apply real_iff_conj. symmetry. apply inner_conj_sym.
  Qed.

  Lemma norm_positive :
    forall v, 0 <= Re <( v | v )>.
  Proof.
    intros v. unfold inner. rewrite re_csum. apply rsum_ge_0, Forall_forall.
    intros e H. unfold compose. csimpl. unfold Rsqr. nra.
  Qed.

  Lemma norm_definite :
    forall v,
    <( v | v )> = 0 ->
    eq_vec b v zero.
  Proof.
    unfold inner. intros v H. rewrite csum_rsum in H.
    - csimpl. unfold compose in H. apply rsum_0_Forall in H.
      + unfold eq_vec. rewrite Forall_forall in *.
        intros e He. apply Cmod_eq_0, Rsqr_eq_0. specialize (H e He). csimpl.
      + apply Forall_forall. intros e _. csimpl.
        apply Rle_0_sqr.
    - unfold compose. apply Forall_forall. intros e _. csimpl.
  Qed.

End inner.

Instance inner_proper :
  forall A (b : set A), Proper (eq_vec b ==> eq_vec b ==> eq) (inner b).
Proof.
  intros A b v1 v1' H1 v2 v2' H2. unfold inner. apply csum_same.
  apply Forall_forall. intros e He.
  unfold eq_vec in *. rewrite Forall_forall in *. rewrite H1, H2; auto.
Qed.

Section basis.

  Variable A : Type.

  Variable R : relation A.
  
  Variable eqb : A -> A -> bool.

  Infix "=?" := eqb.

  Notation "|( e )>" := (ket eqb e).

  Variable b : set A.

  Notation "<( e | e' )>" := (inner b |( e )> |( e' )>).

  Definition basis b := map (ket eqb) b.

  Lemma basis_normal :
    Equivalence R ->
    reflect2 R eqb ->
    NoDupA R b ->
    Forall (fun e => <( e | e )> = 1) b.
  Proof.
    intros Hq Hr Hd. unfold inner.
    induction Hd as [| e b He Hd IH]; constructor.
    - vsimpl; eauto using Equivalence_eqb_equiv.
      rewrite csum_0; csimpl.
      apply Forall_forall. intros e' H'. erewrite ket_neq; csimpl.
      intros Hc. apply He, InA_alt. exists e'. auto.
    - rewrite Forall_forall in *. intros e' Hi. simpl. rewrite IH by assumption.
      erewrite ket_neq; csimpl.
      intros Hc. apply He, InA_alt. exists e'. auto with relations.
  Qed.

  Lemma basis_orthogonal :
    Equivalence R ->
    reflect2 R eqb ->
    NoDupA R b ->
    ForallOrdPairs (fun e e' => <( e | e' )> = 0) b.
  Proof.
    intros Hq Hr Hd. unfold inner.
    induction Hd as [| e b He Hd IH]; constructor; simpl.
    - apply Forall_forall. intros e' H'. rewrite csum_0.
      + unfold ket, delta. destruct (Hr e' e); csimpl.
        exfalso. apply He, InA_alt. exists e'. auto with relations.
      + apply Forall_forall. intros e0 H0. erewrite ket_neq; csimpl.
        intros Hc. apply He, InA_alt. exists e0. auto.
    - eapply ForallOrdPairs_impl; try apply IH.
      apply ForallPairs_ForallOrdPairs. intros e1 e2 H1 _ H0.
      erewrite ket_neq; csimpl.
      intros Hc. apply He, InA_alt. exists e1. auto with relations.
  Qed.

  Corollary basis_orthonormal :
    Equivalence R ->
    reflect2 R eqb ->
    NoDupA R b ->
    ForallPairs (fun e e' => <( e | e' )> = if e =? e' then 1 else 0) b.
  Proof.
    intros Hq Hr Hd e e' Hi H'. destruct (Hr e e').
    - rewrite r. apply basis_normal in Hd; auto.
      rewrite Forall_forall in Hd. auto.
    - apply basis_orthogonal in Hd; auto.
      apply ForallOrdPairs_In with (x:=e) (y:=e') in Hd; auto.
      destruct Hd as [Hd | [Hd | Hd]].
      + exfalso. subst. apply n. reflexivity.
      + assumption.
      + rewrite <- inner_conj_sym, Hd. csimpl.
  Qed.

End basis.

Section bra.

  Variable A : Type.

  Variable R : relation A.

  Variable eqb : A -> A -> bool.

  Infix "=?" := eqb.

  Variable b : set A.

  Notation "<( e | v )>" := (inner b (ket eqb e) v).

  Lemma bra_apply :
    forall v,
    Equivalence R ->
    reflect2 R eqb ->
    NoDupA R b ->
    Forall (fun e => <( e | v )> = v e) b.
  Proof.
    intros v Hq Hr Hd. unfold inner.
    induction Hd as [| e b Hn Hd IH]; constructor.
    - simpl. rewrite ket_eq; eauto using Equivalence_eqb_equiv.
      rewrite csum_0; csimpl.
      apply Forall_forall. intros e' H'. erewrite ket_neq; csimpl.
      intros Hc. apply Hn, InA_alt. exists e'. auto.
    - eapply Forall_impl_2; try apply IH.
      apply Forall_forall. intros e' H' H0. csimpl. rewrite H0.
      erewrite ket_neq; csimpl.
      intros Hc. apply Hn, InA_alt. exists e'. auto with relations.
  Qed.

End bra.

Section vsum.

  Variable A V : Type.

  Variable s : vector_space V.

  Fixpoint vsum f (b : set A) : V :=
    match b with
    | [] => zero
    | e :: b' => f e + vsum f b'
    end.

  Lemma vsum_0 :
    forall f b,
    valid_space s ->
    Forall (fun e => f e = zero) b ->
    vsum f b = zero.
  Proof.
    intros f b Hv H. induction H as [| e b H _ IH]; auto.
    simpl. rewrite H, IH. vsimpl.
  Qed.

End vsum.

Lemma vec_sum_0 :
  forall A A' (f : A -> vec A') (b : set A) (b' : set A'),
  Forall (fun e => eq_vec b' (f e) zero) b ->
  eq_vec b' (vsum (vec_space A') f b) zero.
Proof.
  intros A A' f b b' H. induction H as [| e b He H IH].
  - reflexivity.
  - simpl.
    replace (fun e0 => _) with (f e + vsum (vec_space A') f b) by reflexivity.
    rewrite He, IH. vsimpl. apply vec_valid.
Qed.

Lemma vsum_csum :
  forall A f (b : set A),
  vsum complex_space f b = csum f b.
Proof. reflexivity. Qed.

Lemma vsum_map :
  forall A A' V s b (f : A -> A') (g : A' -> V),
  valid_space s ->
  vsum s g (map f b) = vsum s (compose g f) b.
Proof.
  intros A A' V s b f g Hv. induction b as [| e b IH]; auto.
  simpl. f_equal. assumption.
Qed.

Lemma vsum_apply :
  forall A f (b : set A) e,
  vsum (vec_space A) f b e = csum (flip f e) b.
Proof.
  intros A f b e. induction b as [| e' b IH]; auto.
  simpl. f_equal. assumption.
Qed.

Lemma vector_as_vsum :
  forall A (R : relation A) (eqb : A -> A -> bool) b (v : vec A),
  Equivalence R ->
  reflect2 R eqb ->
  NoDupA R b ->
  eq_vec b v (vsum (vec_space A) (fun e e' => if eqb e e' then v e else 0) b).
Proof.
  intros A R eqb b v Hq Hr Hd.
  induction Hd as [| e b He Hd IH]; constructor.
  - simpl. rewrite eqb_refl, vsum_apply. rewrite csum_0; csimpl.
    apply Forall_forall. intros e' H'. unfold flip.
    destruct (Hr e' e); auto.
    exfalso. apply He, InA_alt. exists e'. auto with relations.
  - apply Forall_forall. intros e' H'. simpl.
    destruct (Hr e e').
    + exfalso. apply He, InA_alt. exists e'. auto.
    + unfold eq_vec in IH. rewrite Forall_forall in IH. vsimpl. apply IH, H'.
Qed.

(*
Definition finite_functional {A : Type}
  (eqb : A -> A -> bool) (b : set A) (f : vec A -> C) :=
  forall e, set_In e b \/ f (ket eqb e) = 0.

Lemma finite_functional_subspace :
  forall A eqb b,
  subspace (fun_space (vec A) complex_space) (finite_functional eqb b).
Proof.
  intros A eqb b. split; simpl.
  - intros e. auto.
  - intros f f' H H' e.
    specialize (H e) as [H | H]; specialize (H' e) as [H' | H']; auto.
    right. rewrite H, H'. csimpl.
  - intros z f H e. specialize (H e) as [H | H]; auto.
    right. rewrite H. csimpl.
Qed.

Definition finite_op {A A' : Type}
  (eqb : A -> A -> bool) (b : set A) (b' : set A') (u : op A A') :=
  (forall e, set_In e b \/ u (ket eqb e) = const 0) /\
  forall v,
  finite_vec b v -> 
  finite_vec b' (u v).

Lemma finite_op_subspace :
  forall A A' eqb b b',
  subspace (op_space A A') (finite_op eqb b b').
Proof.
  intros A A' eqb b b'. split; simpl.
  - split; auto.
    intros v H e. auto.
  - intros u u' [He Hv] [He' Hv']. split.
    + intros e.
      specialize (He e) as [He | He]; specialize (He' e) as [He' | He']; auto.
      right. rewrite He, He'.
      apply functional_extensionality. intros e'. csimpl.
    + intros v H. apply finite_vplus; auto.
  - intros z u [He Hv]. split.
    + intros e. specialize (He e) as [He | He]; auto.
      right. rewrite He. apply functional_extensionality. intros e'. csimpl.
    + intros v H. apply finite_scale; auto.
Qed.
 *)

Lemma vsum_additive :
  forall A V V' (s : vector_space V) (s' : vector_space V') f (b : set A) u,
  valid_space s ->
  valid_space s' ->
  linear s s' u ->
  u (vsum s f b) = vsum s' (fun e => u (f e)) b.
Proof.
  intros A V V' s s' f b u Hs H' Hl. induction b as [| e b IH].
  - apply linear_0; assumption.
  - simpl. destruct Hl as [Ha Hh].
    rewrite Ha, IH. reflexivity.
Qed.

Section linear_as_vsum.

  Variable A V : Type.

  Variable R : relation A.

  Variable eqb : A -> A -> bool.

  Notation "|( e )>" := (ket eqb e).

  Lemma linear_as_vsum :
    forall (s : vector_space V) b u v,
    valid_space s ->
    reflect2 R eqb ->
    NoDupA R b ->
    linear (vec_space A) s u ->
    eq_vec b (u v) (vsum s (fun e => v e * u |( e )>) b).
  Proof.
    intros b u v Hr Hv Hd Hl Hf.
    rewrite <- vector_as_vsum with (v := v) (eqb := eqb) (b := b); auto.
    rewrite vsum_linear with (s' := s); auto using vec_valid.
    f_equal. apply functional_extensionality. intros e.
    rewrite vsum_apply.
    rewrite <- homogeneous with (s := vec_space A) by assumption.
    f_equal. apply functional_extensionality. intros e'.
    unfold ket. simpl. bdestruct (eqb e e'); csimpl.
    subst. specialize (Hf e') as [Hi | H0].
    - induction Hd as [| e b Hn Hd IH]; try contradiction.
      simpl. bdestruct (eqb e e').
      + subst. rewrite csum_0; csimpl.
        apply Forall_forall. clear Hi. intros e Hi. bdestruct (eqb e e'); auto.
        subst. contradiction.
      + csimpl. apply IH. destruct Hi; auto.
        contradiction.
    - rewrite H0. symmetry. apply csum_0, Forall_forall. intros e Hi.
      bdestruct (eqb e e'); auto.
      subst. assumption.
  Qed.

End linear_as_vsum.

Section outer.

  Variable A A' : Set.

  Infix "*" := (scale (vec_space A)).

  Notation "v - v'" := (vplus (vec_space A) v (-1 * v')).

  Infix "*'" := (scale (vec_space A')) (at level 40).

  Variable eqb : A -> A -> bool.

  Variable b : set A.

  Notation "|( e )>" := (ket eqb e).
  Notation "<( v1 | v2 )>" := (inner b v1 v2).

  Definition riesz (f : vec A -> C) : vec A :=
    fun e => Cconj (f |( e )>).

  Theorem riesz_representation :
    forall (f : vec A -> C) (v : vec A),
    (forall e e', reflect (e = e') (eqb e e')) ->
    NoDup b ->
    linear (vec_space A) complex_space f ->
    finite_vec b v ->
    <( riesz f | v )> = f v.
  Proof.
    intros f v Hr Hd Hl Hf. symmetry.
    rewrite linear_as_vsum with (eqb := eqb) (s := complex_space) (b := b);
    auto using complex_valid.
    rewrite vsum_csum.
    unfold inner, riesz. f_equal. apply functional_extensionality. intros e.
    rewrite conj_idemp. apply Cmult_comm.
  Qed.

  Lemma riesz_finite :
    forall f, finite_functional eqb b f -> finite_vec b (riesz f).
  Proof.
    intros f H e. specialize (H e) as [H | H]; auto.
    right. unfold riesz. rewrite H. csimpl.
  Qed.

  Lemma riesz_unique :
    forall f v,
    (* TODO figure out if this eqb condition is valid or how to change it *)
    (forall e e', reflect (e = e') (eqb e e')) ->
    NoDup b ->
    linear (vec_space A) complex_space f ->
    finite_functional eqb b f ->
    finite_vec b v ->
    (forall v', finite_vec b v' -> <( v | v' )> = f v') <-> v = riesz f.
  Proof.
    intros f v Hr Hd Hl Hf Hv.
    split; intros H; subst; auto using riesz_representation.
    set (v' := v - riesz f). assert (finite_vec b v') as H'.
    { apply finite_vplus, finite_scale; auto using riesz_finite. }
    

      apply riesz_finite.
      finite_fun
      apply finite_scale.
    specialize (H v'). specialize (@riesz_representation f v' Hr Hd Hl) as Hf.
    erewrite additive, homogeneous in H by apply inner_linear.
    rewrite (additive Hl), (homogeneous Hl) in H. vsimpl.

  Variable b' : set A'.

  Notation "<( v' | u | v )>" := (inner b' v' (u v)).

  Definition outer v' v : op A A' :=
    fun v0 => <( v | v0 )> *' v'.

  Notation "|( v' >< v )|" := (outer v' v).

  Lemma outer_linear :
    forall v' v,
    linear (vec_space A) (vec_space A') |( v' >< v )|.
  Proof.
    intros v' v. split.
    - intros v1 v2. unfold outer. apply functional_extensionality. intros e.
      specialize (additive (inner_linear b v)) as Ha.
      vsimpl. rewrite Ha. vsimpl.
    - intros z v0. unfold outer. apply functional_extensionality. intros e.
      specialize (homogeneous (inner_linear b v)) as Hh.
      vsimpl; auto using fun_valid, complex_valid. rewrite Hh. reflexivity.
  Qed.

  Lemma outer_finite :
    forall v' v,
    finite_fun b' complex_space v' ->
    finite_op b b' |( v' >< v )|.
  Proof.
    intros v' v H v0 Hf. unfold outer. apply subspace_scale; auto.
    apply finite_subspace, complex_valid.
  Qed.

  Definition adjoint (u : op A A') : op A' A :=
    fun v' =>
    riesz (fun v => <( v' | u | v )>).

  Lemma adjoint_spec :
    forall (u : op A A') v v',
    (forall e e', reflect (e = e') (eqb e e')) ->
    NoDup b ->
    linear (vec_space A) (vec_space A') u ->
    finite_fun b complex_space v ->
    <( v' | u | v )> = <( adjoint u v' | v )>.
  Proof.
    intros u v v' Hr Hd Hl Hf. unfold adjoint.
    symmetry. apply riesz_representation; auto.
    apply (compose_linear Hl (inner_linear b' v')).
  Qed.

  Lemma adjoint_unique :
    forall u u',
    (forall v v', <( v' | u | v )> = <( u' v' | v )>) ->
    u' = adjoint u.
  Proof.



End outer.

Section positive.

  Variable A : Set.

  Variable eqb : A -> A -> bool.

  Variable b : set A.

  Notation adj := (adjoint eqb b).

  Definition hermitian u := adj u = u.

  Definition normal u :=
    compose u (adj u) = compose (adj u) u.

  Lemma hermitian_normal :
    forall u, hermitian u -> normal u.
  Proof.
    intros u H. unfold normal. rewrite H. reflexivity.
  Qed.

  Notation "<( v | u | v' )>" := (inner b v (u v')).

  Definition positive (u : op A A) :=
    forall v, is_real <( v | u | v )> /\ 0 <= Re <( v | u | v )>.

  Lemma positive_hermitian :
    forall u, positive u -> hermitian u.
  Proof.
  Abort.

End positive.

Section trace.

  Variable A : Set.

  Variable eqb : A -> A -> bool.

  Variable b : set A.

  Notation "<( e | u | e' )>" := (inner b (ket eqb e) (u (ket eqb e'))).

  Definition trace (u : op A A) :=
    csum (fun e => <( e | u | e )>) b.

  Lemma trace_linear :
    linear (op_space A A) complex_space trace.
  Proof.
    split.
    - intros u u'. unfold trace. csimpl. rewrite <- csum_additive. f_equal.
      apply functional_extensionality. intros e.
      apply (additive (inner_linear b (ket eqb e))).
    - intros z u. unfold trace. csimpl. rewrite <- csum_homogeneous. f_equal.
      apply functional_extensionality. intros e.
      apply (homogeneous (inner_linear b (ket eqb e))).
  Qed.

  Lemma positive_trace_real :
    forall u, positive b u -> is_real (trace u).
  Proof.
    intros u H. unfold trace. apply is_real_csum, Forall_forall.
    intros e _. apply H.
  Qed.

  Lemma positive_trace :
    forall u, positive b u -> 0 <= Re (trace u).
  Proof.
    intros u H. unfold trace. apply nonnegative_csum, Forall_forall.
    intros e _. apply H.
  Qed.

End trace.

Section density.

  Variable A : Set.

  Definition density_space := op_space A A.

  Definition density := op A A.

  Variable eqb : A -> A -> bool.

  Variable b : set A.

  Notation tr := (trace eqb b).

  Record valid_density (p : density) :=
    { bounded_trace : Re (tr p) <= 1;
      density_positive : positive b p }.

End density.

Section superop.

  Variable A A' : Set.

  Definition superop_space := fun_space (A -> A) (density_space A').

  Definition superop := density A -> density A'.
  
  Record valid_superop (eqb : A -> A -> bool) (E : superop) :=
    { trace_decreasing : forall p, valid_density p
      superop_linear : linear (density_space A) (density_space A') E }.
