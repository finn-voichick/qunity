From Coq Require Import Basics Arith PArith NArith Reals Lia List ListSet.
From Coquelicot Require Import Complex.
Import ListNotations.

Declare Scope qunity_scope.
Open Scope N_scope.
Open Scope positive_scope.
Open Scope nat_scope.
Open Scope bool_scope.
Open Scope qunity_scope.

(* Are two reals equal? *)
Definition eqb_R a a' := if Req_appart_dec a a' then true else false.

(* OpenQASM/IBM's three-parameter U3 gate *)
Record gate := U3 { theta : R; phi : R; lambda : R }.

(* Are two gates equal? *)
Definition eqb_gate '(U3 theta phi lambda) '(U3 theta' phi' lambda') :=
  eqb_R theta theta' && eqb_R phi phi' && eqb_R lambda lambda'.

(* Adjoint (conjugate transpose) of a gate *)
Definition adj_gate '(U3 theta phi lambda) := U3 (- theta) (- phi) (- lambda).

(* Commonly-used Pauli-X gate *)
Example pauli_x := U3 PI 0 PI.

(* A "register" is just a list of qubit indices *)
Definition reg := set nat.

(* A basic "command" at the level of OpenQASM *)
Inductive com :=
  | skip (* no-op *)
  | phase (gamma : R) (* global phase *)
  | pgate (i : reg) (g : gate) (* apply gate individually to each qubit *)
  | pctrl (i0 i1 : reg) (c : com) (* negctrl on i0, ctrl on i1 *)
  | seq (c c' : com). (* sequence commands: c, then c' *)

Infix ";" := seq (at level 50) : qunity_scope.

(* A single CNOT with control index c and target index i *)
Definition cnot c i :=
  pctrl [c] [] (pgate [i] pauli_x).

(* SWAP through CNOTS (maybe add to language itself?) *)
Definition pswap i j :=
  cnot i j; cnot j i; cnot i j.

(* Adjoint (conjugate transpose) of a command *)
Fixpoint padj c :=
  match c with
  | skip => skip
  | phase gamma => phase (-gamma)
  | pgate i g => pgate i (adj_gate g)
  | pctrl i0 i1 c => pctrl i0 i1 (padj c)
  | c; c' => seq (padj c') (padj c) (* reverse order *)
  end.

(* apply transformation f to each index appearing in c *)
Fixpoint reindex f c :=
  match c with
  | skip | phase _ => c
  | pgate i g => pgate (map f i) g
  | pctrl i0 i1 c => pctrl (map f i0) (map f i1) (reindex f c)
  | c; c' => reindex f c; reindex f c'
  end.

(* Rotate n qubits one index to the left and move qubit #i n to the right *)
Fixpoint rotl i n :=
  match n with
  | 0 => skip
  | S n' => pswap i (S i); rotl (S i) n'
  end.

(* Rotate right. This applies a sequence of SWAP gates to move n qubits to the
   next-highest index starting with qubit i, so the value originally at index
   (i+n) will end up at i. *)
Fixpoint rotr i n :=
  match n with
  | 0 => skip
  | S n' => rotr (S i) n'; pswap i (S i)
  end.

(* Permute n qubits starting with index i. Qubit l[j] will store the value originally at location j. *)
Fixpoint permute i n l :=
  match n, l with
  | S n', j :: l' =>
      pswap i j;
      permute (S i) n' (map (fun k => if Nat.eqb k i then j else k) l')
  | _, _ => skip
  end.

(* shift n1 qubits n0 to the left through swaps *)
Fixpoint move_left i n0 n1 :=
  match n1 with
  | 0 => skip
  | S n' => pswap i (i + n0); move_left (S i) n0 n'
  end.

(* shift n0 qubits n1 to the right through swaps *)
Fixpoint move_right i n0 n1 :=
  match n0 with
  | 0 => skip
  | S n' => move_right (S i) n' n1; pswap i (i + n1)
  end.

(* use CNOTs to share n qubits from "from" to "to" *)
Fixpoint share n from to :=
  match n with
  | 0 => skip
  | S n' => cnot from to; share n' (S from) (S to)
  end.
