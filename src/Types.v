From Coq Require Import
  String List Basics Reals RelationClasses FunctionalExtensionality.
From PLF Require Import Typechecking.
From VFA Require Import Perm.
From Qunity Require Import BoolAux ListSetAux MapsAux Syntax.
Import ListNotations.

Set Implicit Arguments.

(* Typing judgements and relevant lemmas *)

Open Scope list_scope.

(* A tactic to use after some of my custom inversion lemmas *)
Ltac post_invert :=
  repeat
  match goal with
  | [ H : exists _, _ |- _ ] => destruct H
  | [ H : _ /\ _ |- _ ] => destruct H
  end;
  subst.

(* Is x a free variable in the given pattern? *)
Inductive free_in_pat (x : string) : expr -> Prop :=
  | free_in_pat_var :
      free_in_pat x x
  | free_in_pat_left T p :
      free_in_pat x p ->
      free_in_pat x (lef T p)
  | free_in_pat_right T p :
      free_in_pat x p ->
      free_in_pat x (rit T p)
  | free_in_pat_pair_1 p1 p2 :
      free_in_pat x p1 ->
      free_in_pat x (| p1, p2 |)
  | free_in_pat_pair_2 p1 p2 :
      free_in_pat x p2 ->
      free_in_pat x (| p1, p2 |).

(* Gets a listing of all of the pattern's free variables *)
Fixpoint pat_free_vars p :=
  match p with
  | expr_var x => [x]
  | lef _ p' | rit _ p' => pat_free_vars p'
  | (| p1, p2 |) => var_set_union (pat_free_vars p1) (pat_free_vars p2)
  | _ => []
  end.

(* equivalence between free_in_pat and pat_free_vars *)
Lemma pat_free_vars_spec :
  forall p x, set_In x (pat_free_vars p) <-> free_in_pat x p.
Proof.
  intros p x. split.
  - induction p; simpl; intros H; try contradiction; auto using free_in_pat.
    + destruct H; subst; auto using free_in_pat.
      contradiction.
    + apply var_set_union_spec in H as []; auto using free_in_pat.
  - intros H. induction H; simpl; auto; apply var_set_union_spec; auto.
Qed.

(* free_in_pat as a computable boolean instead of Prop *)
Definition free_in_pat_bool x e := set_mem_bool eqb_string x (pat_free_vars e).

(* free_in_pat_bool reflects free_in_pat *)
Lemma free_in_pat_bool_spec :
  forall x e, reflect (free_in_pat x e) (free_in_pat_bool x e).
Proof.
  intros x e. unfold free_in_pat_bool.
  bdestruct (set_mem_bool eqb_string x (pat_free_vars e)); constructor.
  - apply pat_free_vars_spec, H.
  - intros Hc. apply H, pat_free_vars_spec, Hc.
Qed.

(* Is x a free variable in the given expression? *)
Inductive appears_free_in (x : string) : expr -> Prop :=
  | appears_free_in_var :
      appears_free_in x x
  | appears_free_in_left T e :
      appears_free_in x e ->
      appears_free_in x (lef T e)
  | appears_free_in_right T e :
      appears_free_in x e ->
      appears_free_in x (rit T e)
  | appears_free_in_pair_1 e1 e2 :
      appears_free_in x e1 ->
      appears_free_in x (| e1, e2 |)
  | appears_free_in_pair_2 e1 e2 :
      appears_free_in x e2 ->
      appears_free_in x (| e1, e2 |)
  | appears_free_in_match_1 e l :
      appears_free_in x e ->
      appears_free_in x (expr_match e l)
  | appears_free_in_match_2 e l :
      (exists p e',
       In (p =>> e') l /\ appears_free_in x e' /\ ~ free_in_pat x p) ->
      appears_free_in x (expr_match e l)
  | appears_free_in_case_1 e l :
      appears_free_in x e ->
      appears_free_in x (expr_case e l)
  | appears_free_in_case_2 e l :
      (exists p e',
       In (p =>> e') l /\ appears_free_in x e' /\ ~ free_in_pat x p) ->
      appears_free_in x (expr_case e l)
  | appears_free_in_gphase e gamma :
      appears_free_in x e ->
      appears_free_in x (e |> gphase gamma)
  | appears_free_in_gate e g :
      appears_free_in x e ->
      appears_free_in x (e |> g)
  | appears_free_in_oracle_1 e1 e2 e3 :
      appears_free_in x e1 ->
      appears_free_in x (oracle (e1, e2 (+) e3))
  | appears_free_in_oracle_2 e1 e2 e3 :
      appears_free_in x e2 ->
      appears_free_in x (oracle (e1, e2 (+) e3))
  | appears_free_in_oracle_3 e1 e2 e3 :
      appears_free_in x e3 ->
      appears_free_in x (oracle (e1, e2 (+) e3)).

(* Alternative induction principle that uses Exists *)
Lemma appears_free_in_ind' :
  forall (x : string) (P : expr -> Prop),
  P x ->
  (forall T e, appears_free_in x e -> P e -> P (lef T e)) ->
  (forall T e, appears_free_in x e -> P e -> P (rit T e)) ->
  (forall e1 e2 : expr, appears_free_in x e1 -> P e1 -> P (|e1, e2|)) ->
  (forall e1 e2 : expr, appears_free_in x e2 -> P e2 -> P (|e1, e2|)) ->
  (forall e l, appears_free_in x e -> P e -> P (expr_match e l)) ->
  (forall e l,
   Exists
     (fun '(p =>> e') => appears_free_in x e' /\ P e' /\ ~ free_in_pat x p) l ->
   P (expr_match e l)) ->
  (forall e l, appears_free_in x e -> P e -> P (expr_case e l)) ->
  (forall e l,
   Exists
     (fun '(p =>> e') => appears_free_in x e' /\ P e' /\ ~ free_in_pat x p) l ->
   P (expr_case e l)) ->
  (forall e gamma, appears_free_in x e -> P e -> P (e |> gphase gamma)) ->
  (forall e g, appears_free_in x e -> P e -> P (e |> g)) ->
  (forall e1 e2 e3, appears_free_in x e1 -> P e1 -> P oracle (e1, e2 (+) e3)) ->
  (forall e1 e2 e3, appears_free_in x e2 -> P e2 -> P oracle (e1, e2 (+) e3)) ->
  (forall e1 e2 e3, appears_free_in x e3 -> P e3 -> P oracle (e1, e2 (+) e3)) ->
  forall e, appears_free_in x e -> P e.
Proof.
  intros x P H_var H_left H_right H_pair1 H_pair2 h_match1 H_match2
    H_case1 H_case2 H_gphase H_gate H_oracle1 H_oracle2 H_oracle3 e H.
  induction e using expr_ind'; inversion H; subst; auto.
  - destruct H3 as [p [e' [Hi [Hf Hn]]]].
    rewrite Forall_forall in H1. specialize (H1 _ Hi).
    apply H_match2, Exists_exists. exists (p =>> e'). auto.
  - destruct H3 as [p [e' [Hi [Hf Hn]]]].
    rewrite Forall_forall in H1. specialize (H1 _ Hi).
    apply H_case2, Exists_exists. exists (p =>> e'). auto.
Qed.

(* Compute a listing of all of the free variables in the expression *)
Fixpoint free_vars e : var_set :=
  match e with
  | () => []
  | expr_var x => [x]
  | lef _ e' | rit _ e' | e' |> gphase _ | e' |> _ => free_vars e'
  | (| e1, e2 |) => var_set_union (free_vars e1) (free_vars e2)
  | expr_match e' l | expr_case e' l =>
      var_set_union (free_vars e') (big_var_set_union free_vars_pat_match l)
  | oracle (e1, e2 (+) e3) =>
      var_set_union (var_set_union (free_vars e1) (free_vars e2)) (free_vars e3)
  end
with free_vars_pat_match pm : var_set :=
  let '(p =>> e) := pm in var_set_diff (free_vars e) (pat_free_vars p).

(* Relationship between appears_free_in and free_vars *)
Lemma free_vars_spec :
  forall e x, set_In x (free_vars e) <-> appears_free_in x e.
Proof.
  intros e x. split.
  - induction e using expr_ind'; simpl; intros H'; auto using appears_free_in.
    + contradiction.
    + destruct H'.
      * subst. constructor.
      * contradiction.
    + apply var_set_union_spec in H' as [H | H]; auto using appears_free_in.
    + apply var_set_union_spec in H' as [H' | H']; auto using appears_free_in.
      apply appears_free_in_match_2. apply big_var_set_union_spec in H'.
      clear H. induction H' as [[p e'] l Hd | [p2 e2] l He IH].
      * exists p, e'. inversion_clear H0. split; simpl; auto.
        apply var_set_diff_spec in Hd as [Hi Hn]. split; auto.
        intros Hc. apply Hn, pat_free_vars_spec, Hc.
      * inversion_clear H0.
        destruct IH as [p [e' [Hi [Hf Hp]]]]; auto.
        exists p, e'. simpl. auto.
    + apply var_set_union_spec in H' as [H' | H']; auto using appears_free_in.
      apply appears_free_in_case_2. apply big_var_set_union_spec in H'.
      clear H. induction H' as [[p e'] l Hd | [p2 e2] l He IH].
      * exists p, e'. inversion_clear H0. split; simpl; auto.
        apply var_set_diff_spec in Hd as [Hi Hn]. split; auto.
        intros Hc. apply Hn, pat_free_vars_spec, Hc.
      * inversion_clear H0.
        apply IH in H1 as [p [e' [Hi [Hf Hp]]]]. exists p, e'. simpl. auto.
    + repeat rewrite var_set_union_spec in H'.
      destruct H' as [[Hi | Hi] | Hi]; auto using appears_free_in.
  - intros H.
    induction H using appears_free_in_ind'; simpl;
    repeat rewrite var_set_union_spec; auto.
    + right. apply big_var_set_union_spec.
      induction H as [[p e'] l [H [IH Hn]] |]; auto.
      left. apply var_set_diff_spec. split; auto.
      intros Hc. apply Hn, pat_free_vars_spec, Hc.
    + right. apply big_var_set_union_spec.
      induction H as [[p e'] l [H [IH Hn]] |]; auto.
      left. apply var_set_diff_spec. split; auto.
      intros Hc. apply Hn, pat_free_vars_spec, Hc.
Qed.

(* appears_free_in as a computable boolean *)
Definition appears_free_bool x e := set_mem_bool eqb_string x (free_vars e).

Lemma appears_free_bool_spec :
  forall x e, reflect (appears_free_in x e) (appears_free_bool x e).
Proof.
  intros x e. unfold appears_free_bool.
  bdestruct (set_mem_bool eqb_string x (free_vars e)); constructor.
  - apply free_vars_spec. assumption.
  - intros Hc. apply H, free_vars_spec, Hc.
Qed.

#[export]
Hint Resolve free_in_pat_bool_spec appears_free_bool_spec : bdestruct.

Lemma free_in_pat_appears_free :
  forall p x, free_in_pat x p -> appears_free_in x p.
Proof.
  intros p x H. induction H; simpl; auto using appears_free_in.
Qed.

(* A value stores primitive data: unit, tagged value, or value pair *)
Inductive val : expr -> type -> Prop :=
  | val_unit :
      val () Unit
  | val_left e T0 T1 :
      val e T0 ->
      val (lef T1 e) (T0 <+> T1)
  | val_right e T0 T1 :
      val e T1 ->
      val (rit T0 e) (T0 <+> T1)
  | val_pair e1 e2 T1 T2 :
      val e1 T1 ->
      val e2 T2 ->
      val (| e1, e2 |) (T1 <*> T2).

(* Get the (deterministic) type of the value *)
Fixpoint val_check e :=
  match e with
  | () =>
      return Unit
  | lef T1 e' =>
      T0 <- val_check e';;
      return T0 <+> T1
  | rit T0 e' =>
      T1 <- val_check e';;
      return T0 <+> T1
  | (| e1, e2 |) =>
      T1 <- val_check e1;;
      T2 <- val_check e2;;
      return T1 <*> T2
  | _ =>
      fail
  end.

Lemma val_check_spec :
  forall v T, val_check v = Some T <-> val v T.
Proof.
  intros v T. split.
  - generalize dependent T. induction v; simpl; intros T' H; try discriminate.
    + injection H as. subst. constructor.
    + destruct (val_check v) as [T0 |]; try discriminate.
      injection H as. subst. auto using val.
    + destruct (val_check v) as [T1 |]; try discriminate.
      injection H as. subst. auto using val.
    + destruct (val_check v1) as [T1 |]; destruct (val_check v2) as [T2 |];
      try discriminate.
      injection H as. subst. auto using val.
  - intros H.
    induction H; simpl; try rewrite IHval; try rewrite IHval1, IHval2; auto.
Qed.

Lemma not_free_in_val :
  forall v T x,
  val v T ->
  ~ free_in_pat x v.
Proof.
  intros v T x H. induction H; intros Hc; inversion Hc; contradiction.
Qed.

(* Patterns are like values, but can contain variables *)
Inductive pat : expr -> type -> Prop :=
  | pat_unit :
      pat () Unit
  | pat_var (x : string) T :
      pat x T
  | pat_left p T0 T1 :
      pat p T0 ->
      pat (lef T1 p) (T0 <+> T1)
  | pat_right p T0 T1 :
      pat p T1 ->
      pat (rit T0 p) (T0 <+> T1)
  | pat_pair p1 p2 T1 T2 :
      (forall x, free_in_pat x p1 -> free_in_pat x p2 -> False) ->
      pat p1 T1 ->
      pat p2 T2 ->
      pat (| p1, p2 |) (T1 <*> T2).

Lemma free_in_pat_spec :
  forall p T x,
  pat p T ->
  free_in_pat x p <-> appears_free_in x p.
Proof.
  intros p T x H.
  induction H; simpl; try destruct IHpat; try (destruct IHpat1, IHpat2); split;
  intros H'; inversion_clear H'; auto using appears_free_in, free_in_pat.
Qed.

(* true if e is a pat of type T, otherwise false *)
Fixpoint pat_bool e T :=
  match e, T with
  | (), Unit => true
  | expr_var _, _ => true
  | lef T' e', T0 <+> T1 => eqb_type T' T1 && pat_bool e' T0
  | rit T' e', T0 <+> T1 => eqb_type T' T0 && pat_bool e' T1
  | (| e1, e2 |), T1 <*> T2 =>
      var_set_disjoint (pat_free_vars e1) (pat_free_vars e2) &&
      pat_bool e1 T1 && pat_bool e2 T2
  | _, _ => false
  end.

Lemma pat_bool_spec :
  forall e T, reflect (pat e T) (pat_bool e T).
Proof.
  intros e T. apply iff_reflect. split.
  - intros H. induction H; simpl; try rewrite eqb_type_refl; auto.
    bdestruct (var_set_disjoint (pat_free_vars p1) (pat_free_vars p2));
    auto with bool.
    exfalso. apply H2, disjoint_spec.
    intros x. repeat rewrite pat_free_vars_spec; eauto.
  - generalize dependent T. induction e; simpl; intros T' H; try discriminate.
    + destruct T'; auto using pat; discriminate.
    + constructor.
    + destruct T' as [| T0 T1 |]; try discriminate.
      bdestruct (T =? T1); try discriminate.
      subst. auto using pat.
    + destruct T' as [| T0 T1 |]; try discriminate.
      bdestruct (T =? T0); try discriminate.
      subst. auto using pat.
    + destruct T' as [| | T1 T2]; try discriminate.
      bdestruct (var_set_disjoint (pat_free_vars e1) (pat_free_vars e2));
      try discriminate.
      apply andb_prop in H as [H1 H2]. constructor; eauto.
      intros x. intros Hf1 Hf2. apply free_in_pat_appears_free in Hf1, Hf2.
      rewrite <- free_in_pat_spec in Hf1, Hf2; eauto.
      eapply disjoint_spec in H0; auto; apply pat_free_vars_spec; eauto.
Qed.

Lemma val_is_pat :
  forall v T, val v T -> pat v T.
Proof.
  intros v T H. induction H; auto using pat.
  constructor; auto.
  intros x Hx. eapply not_free_in_val in Hx; eauto.
Qed.

(* Is there an assignment to the pattern variables so the two are the same? *)
Inductive fits_pattern : expr -> expr -> Prop :=
  | fits_pattern_unit :
      fits_pattern () ()
  | fits_pattern_var (x : string) v :
      fits_pattern x v
  | fits_pattern_left T p v :
      fits_pattern p v ->
      fits_pattern (lef T p) (lef T v)
  | fits_pattern_right T p v :
      fits_pattern p v ->
      fits_pattern (rit T p) (rit T v)
  | fits_pattern_pair p1 p2 v1 v2 :
      fits_pattern p1 v1 ->
      fits_pattern p2 v2 ->
      fits_pattern (| p1, p2 |) (| v1, v2 |).

(* true iff fits_pattern *)
Fixpoint fits_pattern_bool p v :=
  match p, v with
  | (), () | expr_var _, _ => true
  | lef T p', lef T' v' | rit T p', rit T' v' =>
      eqb_type T T' && fits_pattern_bool p' v'
  | (| p1, p2 |), (| v1, v2 |) =>
      fits_pattern_bool p1 v1 && fits_pattern_bool p2 v2
  | _, _ => false
  end.

Lemma fits_pattern_bool_spec :
  forall p v, reflect (fits_pattern p v) (fits_pattern_bool p v).
Proof.
  intros p; induction p; intros []; simpl;
  repeat constructor; try (intros Hc; inversion Hc).
  - bdestruct (T =? T0); bdestruct (fits_pattern_bool p e); constructor;
    subst; auto using fits_pattern; intros Hc; inversion Hc; contradiction.
  - bdestruct (T =? T0); bdestruct (fits_pattern_bool p e); constructor;
    subst; auto using fits_pattern; intros Hc; inversion Hc; contradiction.
  - bdestruct (fits_pattern_bool p1 e1); bdestruct (fits_pattern_bool p2 e2);
    constructor; auto using fits_pattern;
    intros Hc; inversion Hc; contradiction.
Qed.

(* Are two expressions orthogonal? *)
Inductive ortho : expr -> expr -> Prop :=
  | ortho_left_right T0 T1 e e' :
      ortho (lef T1 e) (rit T0 e')
  | ortho_right_left T0 T1 e e' :
      ortho (rit T0 e) (lef T1 e')
  | ortho_pair_1 e1 e2 e1' e2' :
      ortho e1 e1' ->
      ortho (| e1, e2 |) (| e1', e2' |)
  | ortho_pair_2 e1 e2 e1' e2' :
      ortho e2 e2' ->
      ortho (| e1, e2 |) (| e1', e2' |)
  | ortho_gphase_1 e e' gamma :
      ortho e e' ->
      ortho (e |> gphase gamma) e'
  | ortho_gphase_2 e e' gamma :
      ortho e e' ->
      ortho e (e' |> gphase gamma).

Instance ortho_sym :
  Symmetric ortho.
Proof.
  intros e e' H. induction H; auto using ortho.
Qed.

Lemma ortho_gphase_iff_1 :
  forall e e' gamma, ortho (e |> gphase gamma) e' <-> ortho e e'.
Proof.
  intros e e' gamma. split; auto using ortho.
  induction e'; intros H; inversion_clear H; eauto using ortho.
Qed.

Lemma ortho_gphase_iff_2 :
  forall e e' gamma, ortho e (e' |> gphase gamma) <-> ortho e e'.
Proof.
  intros e e' gamma. split; auto using ortho.
  intros H. symmetry. symmetry in H. apply ortho_gphase_iff_1 in H. assumption.
Qed.

(* ortho as a computable boolean *)
Fixpoint ortho_bool e : expr -> bool :=
  match e with
  | lef _ _ => fix ortho_bool' e' := match e' with
                                     | rit _ _ => true
                                     | e1' |> gphase _ => ortho_bool' e1'
                                     | _ => false
                                     end
  | rit _ _ => fix ortho_bool' e' := match e' with
                                     | lef _ _ => true
                                     | e1' |> gphase _ => ortho_bool' e1'
                                     | _ => false
                                     end
  | (| e1, e2 |) =>
      fix ortho_bool' e' :=
      match e' with
      | (| e1', e2' |) => ortho_bool e1 e1' || ortho_bool e2 e2'
      | e1' |> gphase _ => ortho_bool' e1'
      | _ => false
      end
  | e1 |> gphase _ => ortho_bool e1
  | _ => const false
  end.

Lemma ortho_bool_sym :
  forall e e', ortho_bool e e' = ortho_bool e' e.
Proof.
  intros e.
  induction e; intros e'; induction e'; simpl in *; unfold const in *;
  try rewrite IHe; try rewrite IHe1, IHe2; auto.
  simpl. rewrite <- IHe. assumption.
Qed.

Lemma ortho_bool_spec :
  forall e e', reflect (ortho e e') (ortho_bool e e').
Proof.
  intros e e'. apply iff_reflect. split.
  - intros H. induction H; simpl; auto with bool.
    rewrite ortho_bool_sym in *. assumption.
  - generalize dependent e'.
    induction e; intros e'; induction e'; simpl; unfold const;
    intros H; try discriminate; try apply IHe'; auto using ortho.
    apply orb_prop in H as [H | H]; auto using ortho.
Qed.

#[export]
Hint Resolve pat_bool_spec ortho_bool_spec : bdestruct.

Lemma free_var_subset :
  forall e e',
  subset (free_vars e) (free_vars e') <->
  forall x, appears_free_in x e -> appears_free_in x e'.
Proof.
  intros e e'. split.
  - intros H x Hx. apply subset_spec in H. rewrite Forall_forall in H.
    apply free_vars_spec, H, free_vars_spec, Hx.
  - intros H. apply subset_spec, Forall_forall. intros x Hx.
    apply free_vars_spec, H, free_vars_spec, Hx.
Qed.

(* A typing context is a partial map from variables to types *)
Definition ctx := partial_map type.

(* Update a context with a pattern rather than a single variable *)
Reserved Notation "p |=> T ; Gamma"
    (at level 100, T at next level, right associativity).
Fixpoint ctx_update (Gamma : ctx) (p : expr) (T : type) :=
  match p, T with
  | expr_var x, _ => (x |-> T; Gamma)
  | lef _ p', T0 <+> _ => (p' |=> T0; Gamma)
  | rit _ p', _ <+> T1 => (p' |=> T1; Gamma)
  | (| p1, p2 |), T1 <*> T2 => (p1 |=> T1; p2 |=> T2; Gamma)
  | _, _ => Gamma
  end
where "p |=> T ; Gamma" := (ctx_update Gamma p T).

Notation "p |=> T" := (p |=> T; empty) (at level 100).

Lemma defined_ctx_update :
  forall Gamma p T x, defined_on x Gamma -> defined_on x (p |=> T; Gamma).
Proof.
  intros Gamma p. generalize dependent Gamma.
  induction p; intros Gamma [] x' H; simpl; auto using defined_update_neq.
Qed.

Lemma defined_ctx_update_free :
  forall Gamma p T x,
  pat p T -> free_in_pat x p -> defined_on x (p |=> T; Gamma).
Proof.
  intros Gamma p T x H. generalize dependent x. generalize dependent Gamma.
  induction H; simpl; intros Gamma x' Hf; inversion_clear Hf;
  auto using defined_update_eq, defined_ctx_update.
Qed.

Lemma update_ctx_update_permute :
  forall Gamma x p T T',
  ~ free_in_pat x p ->
  (x |-> T; p |=> T'; Gamma) = (p |=> T'; x |-> T; Gamma).
Proof.
  intros Gamma x p. generalize dependent Gamma.
  induction p; simpl; intros Gamma Tx Tp H; auto.
  - apply update_permute. intros Hc. subst. apply H. constructor.
  - destruct Tp; auto.
    apply IHp. intros Hc. apply H. constructor. assumption.
  - destruct Tp; auto.
    apply IHp. intros Hc. apply H. constructor. assumption.
  - destruct Tp; auto.
    rewrite IHp1, IHp2; auto using free_in_pat.
Qed.

Lemma ctx_update_permute :
  forall Gamma p p' T T',
  (forall x, free_in_pat x p -> free_in_pat x p' -> False) ->
  (p |=> T; p' |=> T'; Gamma) = (p' |=> T'; p |=> T; Gamma).
Proof.
  intros Gamma p. generalize dependent Gamma.
  induction p; simpl; intros Gamma p' [] T' H;
  eauto using update_ctx_update_permute, free_in_pat.
  rewrite IHp2, IHp1; eauto using free_in_pat.
Qed.

Lemma defined_ctx_update_iff :
  forall Gamma p T x,
  pat p T ->
  defined_on x (p |=> T; Gamma) <-> free_in_pat x p \/ defined_on x Gamma.
Proof.
  intros Gamma p T x H.
  split;
  try (intros [|]; auto using defined_ctx_update, defined_ctx_update_free).
  generalize dependent Gamma.
  induction H; simpl; intros Gamma; auto.
  - intros [T' Hx]. unfold update, t_update in Hx. bdestruct (eqb_string x0 x).
    + subst. auto using free_in_pat.
    + right. exists T'. assumption.
  - intros Hd. apply IHpat in Hd. destruct Hd; auto using free_in_pat.
  - intros Hd. apply IHpat in Hd. destruct Hd; auto using free_in_pat.
  - intros Hd. edestruct IHpat1; eauto using free_in_pat.
    edestruct IHpat2; eauto using free_in_pat.
Qed.

Lemma defined_ctx_update_empty :
  forall p T x,
  pat p T ->
  defined_on x (p |=> T) <-> free_in_pat x p.
Proof.
  intros p T x Hp. split.
  - intros H. apply defined_ctx_update_iff in H as [| []]; auto.
    discriminate.
  - apply defined_ctx_update_free. assumption.
Qed.

Lemma inclusion_ctx_update :
  forall Gamma Gamma' p T,
  inclusion Gamma Gamma' ->
  inclusion (p |=> T; Gamma) (p |=> T; Gamma').
Proof.
  intros m m' p. generalize dependent m'. generalize dependent m.
  induction p; simpl; auto using inclusion_update.
  - intros m m' [| T0 |]; auto.
  - intros m m' [| _ T1 |]; auto.
  - intros m m' [| | T1 T2]; auto.
Qed.

Lemma map_union_ctx_update :
  forall p T Gamma,
  map_union Gamma (p |=> T) = (p |=> T; Gamma).
Proof.
  intros p.
  induction p; simpl; intros [] Gamma;
  auto using map_union_empty_2, map_union_update.
  rewrite <- IHp1, map_union_assoc, IHp2, IHp1. reflexivity.
Qed.

Lemma map_union_ctx_update_2 :
  forall p T Gamma,
  (forall x, free_in_pat x p -> defined_on x Gamma -> False) ->
  map_union (p |=> T) Gamma = (p |=> T; Gamma).
Proof.
  intros p.
  induction p; simpl; intros [] Gamma H;
  try (apply map_union_update_2, undefined_None; unfold not; apply H);
  try (apply IHp; intros x Hx; apply H);
  auto using map_union_empty_1, free_in_pat.
  rewrite <- map_union_ctx_update, <- map_union_assoc, IHp1.
  - replace (p1 |=> T1; Gamma) with (map_union Gamma (p1 |=> T1))
                               by apply map_union_ctx_update.
    rewrite map_union_assoc, IHp2, map_union_ctx_update; auto.
    intros x Hx. apply H. auto using free_in_pat.
  - intros x Hx. apply H. auto using free_in_pat.
Qed.

Lemma ctx_update_twice :
  forall Gamma p T,
  (p |=> T; p |=> T; Gamma) = (p |=> T; Gamma).
Proof.
  intros Gamma p T. rewrite <- map_union_ctx_update.
  apply union_inclusion_1, inclusion_ctx_update, inclusion_empty.
Qed.

Lemma filter_ctx_update_true :
  forall Gamma p T f,
  (forall x, free_in_pat x p -> f x = true) ->
  filter_map f (p |=> T; Gamma) = (p |=> T; filter_map f Gamma).
Proof.
  intros Gamma p. generalize dependent Gamma.
  induction p; simpl; intros Gamma [] f H;
  try rewrite filter_update, H; try rewrite IHp1, IHp2; auto using free_in_pat.
Qed.

Lemma ctx_update_filter_unchanged :
  forall Gamma p T,
  pat p T ->
  (p |=> T; Gamma) =
  (p |=> T; filter_map (fun x => negb (free_in_pat_bool x p)) Gamma).
Proof.
  intros Gamma p T H. generalize dependent Gamma.
  induction H; intros Gamma; simpl; auto.
  - apply functional_extensionality. intros x'.
    unfold update, t_update. bdestruct (eqb_string x x'); auto.
    unfold filter_map. bdestruct (free_in_pat_bool x' x); auto.
    inversion H0. subst. contradiction.
  - rewrite IHpat2, IHpat1. f_equal. rewrite filter_ctx_update_true.
    + f_equal. rewrite filter_andb. f_equal.
      apply functional_extensionality. intros x. rewrite <- negb_orb. f_equal.
      bdestruct (free_in_pat_bool x p1); bdestruct (free_in_pat_bool x p2);
      bdestruct (free_in_pat_bool x (| p1, p2 |)); auto;
      exfalso; auto using free_in_pat.
      inversion_clear H4; contradiction.
    + intros x Hx. bdestruct (free_in_pat_bool x p1); eauto.
Qed.

Lemma filter_ctx_update_true_2 :
  forall Gamma p T f,
  pat p T ->
  (forall x, free_in_pat x p -> f x = true) ->
  filter_map f (p |=> T; Gamma) =
  (p |=> T; filter_map (fun x => f x && negb (free_in_pat_bool x p)) Gamma).
Proof.
  intros Gamma p T f Hp Hf. rewrite <- filter_andb, filter_permute.
  rewrite <- ctx_update_filter_unchanged by assumption.
  apply filter_ctx_update_true, Hf.
Qed.

Lemma filter_ctx_update_false :
  forall Gamma p T f,
  (forall x, free_in_pat x p -> f x = false) ->
  filter_map f (p |=> T; Gamma) = filter_map f Gamma.
Proof.
  intros Gamma p. generalize dependent Gamma.
  induction p; simpl; intros Gamma [] f H;
  try rewrite filter_update, H; try rewrite IHp1, IHp2; auto using free_in_pat.
Qed.

Lemma ctx_update_unfree :
  forall Gamma p T x,
  ~ free_in_pat x p ->
  (p |=> T; Gamma) x = Gamma x.
Proof.
  intros Gamma p. generalize dependent Gamma.
  induction p; simpl; intros Gamma T' x' H; auto.
  - rewrite update_neq; auto.
    intros Hc. subst. apply H. constructor.
  - destruct T'; auto using free_in_pat.
  - destruct T'; auto using free_in_pat.
  - destruct T' as [| | T1 T2]; auto.
    rewrite IHp1, IHp2; auto using free_in_pat.
Qed.

Lemma ctx_update_free :
  forall Gamma Gamma' p T T' x,
  pat p T ->
  free_in_pat x p ->
  (p |=> T; Gamma) x = Some T' ->
  (p |=> T; Gamma') x = Some T'.
Proof.
  intros Gamma Gamma' p T T' x H.
  generalize dependent Gamma'. generalize dependent Gamma.
  induction H; simpl; intros Gamma Gamma' Hf Ht; inversion Hf; subst; eauto.
  - rewrite update_eq in Ht. injection Ht as. subst. apply update_eq.
  - bdestruct (free_in_pat_bool x p1).
    + eapply IHpat1; eauto.
    + rewrite ctx_update_unfree in *; eauto.
Qed.

Lemma inclusion_filter_ctx_update :
  forall Gamma Gamma' p T f f',
  inclusion Gamma Gamma' ->
  (forall x, f x = true -> f' x = true) ->
  inclusion (filter_map f (p |=> T; Gamma)) (p |=> T; filter_map f' Gamma').
Proof.
  intros Gamma Gamma' p.
  generalize dependent Gamma'. generalize dependent Gamma.
  induction p; simpl; intros Gamma Gamma' [] f f' Hi Hf;
  auto using inclusion_filter_2, inclusion_filter_update.
  transitivity (p1 |=> T1; filter_map f (p2 |=> T2; Gamma)).
  - apply IHp1; auto.
    apply inclusion_ctx_update. reflexivity.
  - apply inclusion_ctx_update, IHp2; assumption.
Qed.

Lemma filter_ctx_update_unfree :
  forall Gamma p T f x,
  ~ free_in_pat x p ->
  filter_map f (p |=> T; Gamma) x = filter_map f Gamma x.
Proof.
  intros Gamma p. generalize dependent Gamma.
  induction p; simpl; intros Gamma T' f x' H; auto.
  - rewrite filter_update. destruct (f x); auto.
    apply update_neq. intros Hc. subst. apply H. constructor.
  - destruct T'; auto using free_in_pat.
  - destruct T'; auto using free_in_pat.
  - destruct T' as [| | T1 T2]; auto.
    rewrite IHp1, IHp2; auto using free_in_pat.
Qed.

Lemma filter_ctx_update_free :
  forall Gamma Gamma' p T T' f x,
  pat p T ->
  free_in_pat x p ->
  filter_map f (p |=> T; Gamma) x = Some T' ->
  (p |=> T; filter_map f Gamma') x = Some T'.
Proof.
  intros Gamma Gamma' p T T' f x H.
  destruct (f x) eqn:E.
  - generalize dependent Gamma'. generalize dependent Gamma.
    induction H; simpl; intros Gamma Gamma' Hf H'; inversion Hf; subst; eauto.
    + rewrite filter_update, E, update_eq in H'. injection H' as. subst.
      apply update_eq.
    + eapply ctx_update_free; eauto.
    + bdestruct (free_in_pat_bool x p1).
      * eapply ctx_update_free; eauto.
      * rewrite ctx_update_unfree; auto.
        rewrite filter_ctx_update_unfree in H'; eauto.
  - intros Hf H'. unfold filter_map in H'. rewrite E in H'. discriminate.
  Unshelve. all: eauto.
Qed.

Lemma inclusion_filter_ctx_update_2 :
  forall Gamma Gamma' p T f f',
  pat p T ->
  inclusion Gamma Gamma' ->
  (forall x, ~ free_in_pat x p -> f x = true -> f' x = true) ->
  inclusion (filter_map f (p |=> T; Gamma)) (p |=> T; filter_map f' Gamma').
Proof.
  intros Gamma Gamma' p T f f' Hp Hi Hf x T' Hx.
  bdestruct (free_in_pat_bool x p).
  - eapply filter_ctx_update_free in Hx; auto.
    eapply ctx_update_free; eauto.
  - unfold filter_map in Hx. destruct (f x) eqn:E; try discriminate.
    rewrite ctx_update_unfree in *; auto.
    apply Hf in E; auto.
    unfold filter_map. rewrite E. apply Hi, Hx.
  Unshelve. assumption.
Qed.

(* pure for operators, mixed for superoperators *)
Inductive purity :=
  | pure
  | mixed.

(* The main typing relation, dependent on purity *)
Reserved Notation "Gamma |- e : ( a ) T" (at level 40, e at next level).
Reserved Notation "|- e : ( a ) T" (at level 40, e at next level).
Inductive has_type : ctx -> expr -> purity -> type -> Prop :=
  | has_type_unit_pure :
      |- () : (pure) Unit
  | has_type_unit_mixed Gamma :
      Gamma |- () : (mixed) Unit
  | has_type_var_pure x T :
      (x |-> T) |- x : (pure) T
  | has_type_var_mixed Gamma x T :
      Gamma x = Some T ->
      Gamma |- x : (mixed) T
  | has_type_left Gamma e a T0 T1 :
      Gamma |- e : (a) T0 ->
      Gamma |- lef T1 e : (a) (T0 <+> T1)
  | has_type_right Gamma e a T0 T1 :
      Gamma |- e : (a) T1 ->
      Gamma |- rit T0 e : (a) (T0 <+> T1)
  | has_type_pair_pure Gamma Gamma' e1 e2 T1 T2 :
      no_conflict Gamma Gamma' ->
      Gamma |- e1 : (pure) T1 ->
      Gamma' |- e2 : (pure) T2 ->
      map_union Gamma Gamma' |- (| e1, e2 |) : (pure) (T1 <*> T2)
  | has_type_pair_mixed Gamma e1 e2 T1 T2 :
      Gamma |- e1 : (mixed) T1 ->
      Gamma |- e2 : (mixed) T2 ->
      Gamma |- (| e1, e2 |) : (mixed) (T1 <*> T2)
  | has_type_match_pure Gamma Gamma' e p0 e0 l T T' :
      no_conflict Gamma Gamma' ->
      Forall (fun '(p =>> _) => pat p T) (p0 =>> e0 :: l) ->
      ForallOrdPairs
        (fun '(p1 =>> _) '(p2 =>> _) => ortho p1 p2) ((p0 =>> e0) :: l) ->
      ForallOrdPairs
        (fun '(_ =>> e1) '(_ =>> e2) => ortho e1 e2) ((p0 =>> e0) :: l) ->
      (forall x,
       Forall
         (fun '(p =>> _) => free_in_pat x p -> Gamma' x = None)
         (p0 =>> e0 :: l)) ->
      Gamma |- e : (pure) T ->
      (forall p e',
       In (p =>> e') ((p0 =>> e0) :: l) ->
         (p |=> T; Gamma') |- e' : (pure) T') ->
      map_union Gamma Gamma' |- expr_match e ((p0 =>> e0) :: l) : (pure) T'
  | has_type_match_mixed Gamma Gamma' e p0 e0 l T T' :
      inclusion Gamma' Gamma ->
      Forall (fun '(p =>> _) => pat p T) (p0 =>> e0 :: l) ->
      ForallOrdPairs
        (fun '(p1 =>> _) '(p2 =>> _) => ortho p1 p2) ((p0 =>> e0) :: l) ->
      ForallOrdPairs
        (fun '(_ =>> e1) '(_ =>> e2) => ortho e1 e2) ((p0 =>> e0) :: l) ->
      (forall x,
       Forall
         (fun '(p =>> _) => free_in_pat x p -> Gamma' x = None)
         (p0 =>> e0 :: l)) ->
      Gamma |- e : (mixed) T ->
      (forall p e',
       In (p =>> e') ((p0 =>> e0) :: l) ->
         (p |=> T; Gamma') |- e' : (pure) T') ->
      Gamma |- expr_match e ((p0 =>> e0) :: l) : (mixed) T'
  | has_type_case Gamma e p0 e0 l T T' :
      Forall (fun '(p =>> _) => pat p T) (p0 =>> e0 :: l) ->
      ForallOrdPairs
        (fun '(p1 =>> _) '(p2 =>> _) => ortho p1 p2) ((p0 =>> e0) :: l) ->
      Gamma |- e : (mixed) T ->
      (forall p e',
       In (p =>> e') ((p0 =>> e0) :: l) ->
       (p |=> T; Gamma) |- e' : (mixed) T') ->
      Gamma |- expr_case e ((p0 =>> e0) :: l) : (mixed) T'
  | has_type_let Gamma Gamma' p e e' T T' :
      no_conflict Gamma Gamma' ->
      pat p T ->
      (forall x, free_in_pat x p -> Gamma' x = None) ->
      Gamma |- e : (pure) T ->
      (p |=> T; Gamma') |- e' : (pure) T' ->
      map_union Gamma Gamma' |- qlet p := e in e' : (pure) T'
  | has_type_gphase Gamma e gamma T a :
      Gamma |- e : (a) T ->
      Gamma |- e |> gphase gamma : (a) T
  | has_type_gate Gamma e g a :
      Gamma |- e : (a) Bit ->
      Gamma |- e |> g : (a) Bit
  | has_type_oracle_pure Gamma Gamma' e1 e2 e3 T :
      no_conflict Gamma Gamma' ->
      Gamma |- e1 : (pure) T ->
      Gamma' |- e2 : (pure) Bit ->
      Gamma |- e3 : (mixed) Bit ->
      map_union Gamma Gamma' |- oracle (e1, e2 (+) e3) : (pure) (T <*> Bit)
  | has_type_oracle_mixed Gamma e1 e2 e3 T :
      Gamma |- e1 : (mixed) T ->
      Gamma |- e2 : (mixed) Bit ->
      Gamma |- e3 : (mixed) Bit ->
      Gamma |- oracle (e1, e2 (+) e3) : (mixed) (T <*> Bit)
where "Gamma |- e : ( a ) T" := (has_type Gamma e a T)
and "|- e : ( a ) T" := (empty |- e : (a) T).

(* An alternative induction principle using Forall *)
Lemma has_type_ind' :
  forall (P : ctx -> expr -> purity -> type -> Prop),
  P empty () pure Unit ->
  (forall Gamma : ctx, P Gamma () mixed Unit) ->
  (forall x T, P (x |-> T) x pure T) ->
  (forall Gamma x T, Gamma x = Some T -> P Gamma x mixed T) ->
  (forall Gamma e a T0 T1,
   Gamma |- e : (a) T0 -> P Gamma e a T0 -> P Gamma (lef T1 e) a (T0 <+> T1)) ->
  (forall Gamma e a T0 T1,
   Gamma |- e : (a) T1 -> P Gamma e a T1 -> P Gamma (rit T0 e) a (T0 <+> T1)) ->
  (forall Gamma Gamma' e1 e2 T1 T2,
   no_conflict Gamma Gamma' ->
   Gamma |- e1 : (pure) T1 -> P Gamma e1 pure T1 ->
   Gamma' |- e2 : (pure) T2 -> P Gamma' e2 pure T2 ->
   P (map_union Gamma Gamma') (|e1, e2|) pure (T1 <*> T2)) ->
  (forall Gamma e1 e2 T1 T2,
   Gamma |- e1 : (mixed) T1 -> P Gamma e1 mixed T1 ->
   Gamma |- e2 : (mixed) T2 -> P Gamma e2 mixed T2 ->
   P Gamma (|e1, e2|) mixed (T1 <*> T2)) ->
  (forall Gamma Gamma' e e0 p0 l T T',
   no_conflict Gamma Gamma' ->
   Forall (fun '(p =>> _) => pat p T) (p0 =>> e0 :: l) ->
   ForallOrdPairs (fun '(p1 =>> _) '(p2 =>> _) => ortho p1 p2)
     (p0 =>> e0 :: l) ->
   ForallOrdPairs (fun '(_ =>> e1) '(_ =>> e2) => ortho e1 e2)
     (p0 =>> e0 :: l) ->
   (forall x : string,
    Forall
      (fun '(p =>> _) => free_in_pat x p -> Gamma' x = None)
      (p0 =>> e0 :: l)) ->
   Gamma |- e : (pure) T -> P Gamma e pure T ->
   Forall
     (fun '(p =>> e') => (p |=> T; Gamma') |- e' : (pure) T')
     (p0 =>> e0 :: l) ->
   Forall
     (fun '(p =>> e') => P (p |=> T; Gamma') e' pure T')
     (p0 =>> e0 :: l) ->
   P (map_union Gamma Gamma') (expr_match e ((p0 =>> e0) :: l)) pure T') ->
  (forall Gamma Gamma' e e0 p0 l T T',
   inclusion Gamma' Gamma ->
   Forall (fun '(p =>> _) => pat p T) (p0 =>> e0 :: l) ->
   ForallOrdPairs (fun '(p1 =>> _) '(p2 =>> _) => ortho p1 p2)
     (p0 =>> e0 :: l) ->
   ForallOrdPairs (fun '(_ =>> e1) '(_ =>> e2) => ortho e1 e2)
     (p0 =>> e0 :: l) ->
   (forall x : string,
    Forall
      (fun '(p =>> _) => free_in_pat x p -> Gamma' x = None)
      (p0 =>> e0 :: l)) ->
   Gamma |- e : (mixed) T -> P Gamma e mixed T ->
   Forall
     (fun '(p =>> e') => (p |=> T; Gamma') |- e' : (pure) T')
     (p0 =>> e0 :: l) ->
   Forall
     (fun '(p =>> e') => P (p |=> T; Gamma') e' pure T')
     (p0 =>> e0 :: l) ->
   P Gamma (expr_match e ((p0 =>> e0) :: l)) mixed T') ->
  (forall Gamma e e0 p0 l T T',
   Forall (fun '(p =>> _) => pat p T) (p0 =>> e0 :: l) ->
   ForallOrdPairs
     (fun '(p1 =>> _) '(p2 =>> _) => ortho p1 p2)
     ((p0 =>> e0) :: l) ->
   Gamma |- e : (mixed) T -> P Gamma e mixed T ->
   Forall
     (fun '(p =>> e') => (p |=> T; Gamma) |- e' : (mixed) T')
     (p0 =>> e0 :: l) ->
   Forall
     (fun '(p =>> e') => P (p |=> T; Gamma) e' mixed T')
     (p0 =>> e0 :: l) ->
   P Gamma (expr_case e ((p0 =>> e0) :: l)) mixed T') ->
  (forall Gamma Gamma' p e e' T T',
   no_conflict Gamma Gamma' ->
   pat p T ->
   Gamma |- e : (pure) T -> P Gamma e pure T ->
   (forall x, free_in_pat x p -> Gamma' x = None) ->
   (p |=> T; Gamma') |- e' : (pure) T' -> P (p |=> T; Gamma') e' pure T' ->
   P (map_union Gamma Gamma') (qlet p := e in e') pure T') ->
  (forall Gamma e gamma T a,
   Gamma |- e : (a) T -> P Gamma e a T -> P Gamma (e |> gphase gamma) a T) ->
  (forall Gamma e g a,
   Gamma |- e : (a) Bit -> P Gamma e a Bit -> P Gamma (e |> g) a Bit) ->
  (forall Gamma Gamma' e1 e2 e3 T,
   no_conflict Gamma Gamma' ->
   Gamma |- e1 : (pure) T -> P Gamma e1 pure T ->
   Gamma' |- e2 : (pure) Bit -> P Gamma' e2 pure Bit ->
   Gamma |- e3 : (mixed) Bit -> P Gamma e3 mixed Bit ->
   P (map_union Gamma Gamma') oracle (e1, e2 (+) e3) pure (T <*> Bit)) ->
  (forall Gamma e1 e2 e3 T,
   Gamma |- e1 : (mixed) T -> P Gamma e1 mixed T ->
   Gamma |- e2 : (mixed) Bit -> P Gamma e2 mixed Bit ->
   Gamma |- e3 : (mixed) Bit -> P Gamma e3 mixed Bit ->
   P Gamma oracle (e1, e2 (+) e3) mixed (T <*> Bit)) ->
  forall Gamma e a T,
  Gamma |- e : (a) T -> P Gamma e a T.
Proof.
  intros P H_unit_pure H_unit_mixed H_var_pure H_var_mixed H_left H_right
    H_pair_pure H_pair_mixed H_match_pure H_match_mixed H_case H_let H_gphase
    H_gate H_oracle_pure H_oracle_mixed Gamma e a T H.
  induction H; eauto.
  - eapply H_match_pure; eauto; apply Forall_forall; intros [p e'] Hi; auto.
  - eapply H_match_mixed; eauto; apply Forall_forall; intros [p e'] Hi; auto.
  - eapply H_case; eauto; apply Forall_forall; intros [p e'] Hi; auto.
Qed.

Lemma weakening :
  forall Gamma Gamma' e T,
  inclusion Gamma Gamma' ->
  Gamma |- e : (mixed) T ->
  Gamma' |- e : (mixed) T.
Proof.
  intros Gamma Gamma' e.
  generalize dependent Gamma'. generalize dependent Gamma.
  induction e using expr_ind'; intros Gamma Gamma' T' Hi H';
  inversion H'; subst; eauto using has_type.
  - eapply has_type_match_mixed; try transitivity Gamma; eauto.
  - econstructor; eauto.
    intros p e' Hl. specialize (H7 _ _ Hl).
    rewrite Forall_forall in H0. specialize (H0 _ Hl).
    eapply H0; eauto using inclusion_ctx_update.
Qed.

Lemma has_type_unit :
  forall a, |- () : (a) Unit.
Proof.
  intros []; eauto using has_type.
Qed.

Lemma has_type_var :
  forall x T a,
  (x |-> T) |- x : (a) T.
Proof.
  intros x T []; eauto using has_type, update_eq.
Qed.

Lemma has_type_pair :
  forall Gamma Gamma' e1 e2 T1 T2 a,
  no_conflict Gamma Gamma' ->
  Gamma |- e1 : (a) T1 ->
  Gamma' |- e2 : (a) T2 ->
  map_union Gamma Gamma' |- (| e1, e2 |) : (a) (T1 <*> T2).
Proof.
  intros Gamma Gamma' e1 e2 T1 T2 [] Hc H1 H2; eauto using has_type.
  constructor; eapply weakening; eauto.
  - apply inclusion_union_r1; auto.
    reflexivity.
  - apply inclusion_union_r2. reflexivity.
Qed.

Lemma has_type_match_mixed' :
  forall Gamma Gamma' e p0 e0 l T T',
  inclusion Gamma' Gamma ->
  Forall (fun '(p =>> _) => pat p T) (p0 =>> e0 :: l) ->
  ForallOrdPairs (fun '(p1 =>> _) '(p2 =>> _) => ortho p1 p2)
    (p0 =>> e0 :: l) ->
  ForallOrdPairs (fun '(_ =>> e1) '(_ =>> e2) => ortho e1 e2)
    (p0 =>> e0 :: l) ->
  (forall x : string,
   Forall (fun '(p =>> _) => free_in_pat x p -> Gamma' x = fail)
     (p0 =>> e0 :: l)) ->
  Gamma |- e : (mixed) T ->
  Forall
    (fun '(p =>> e') => (p |=> T; Gamma') |- e' : (pure) T')
    (p0 =>> e0 :: l) ->
  Gamma |- expr_match e (p0 =>> e0 :: l) : (mixed) T'.
Proof.
  intros Gamma Gamma' e p0 e0 l T T' Hi Hp Ho He Hd Ht Hf.
  eapply has_type_match_mixed; eauto.
  intros p e' H'. rewrite Forall_forall in Hf. apply (Hf _ H').
Qed.

Lemma has_type_match_mixed_iff :
  forall Gamma e l T',
  Gamma |- expr_match e l : (mixed) T' <->
  exists Gamma' p0 e0 l' T,
  l = p0 =>> e0 :: l' /\
  inclusion Gamma' Gamma /\
  Forall (fun '(p =>> _) => pat p T) l /\
  ForallOrdPairs (fun '(p1 =>> _) '(p2 =>> _) => ortho p1 p2) l /\
  ForallOrdPairs (fun '(_ =>> e1) '(_ =>> e2) => ortho e1 e2) l /\
  (forall x : string,
   Forall (fun '(p =>> _) => free_in_pat x p -> Gamma' x = fail) l) /\
  Gamma |- e : (mixed) T /\
  Forall (fun '(p =>> e') => (p |=> T; Gamma') |- e' : (pure) T') l.
Proof.
  intros Gamma e l T'. split.
  - intros H. inversion H. subst. rename e1 into e0.
    exists Gamma', p0, e0, l0, T. repeat split; auto.
    apply Forall_forall. intros [p e']. auto.
  - intros [Gamma' [p0 [e0 [l' [T [Hq [Hi [Hp [Ho [He [Hd [Ht H']]]]]]]]]]]].
    subst. eapply has_type_match_mixed'; eauto.
Qed.

Lemma has_type_match :
  forall Gamma Gamma' e p0 e0 l T T' a,
  no_conflict Gamma Gamma' ->
  Forall (fun '(p =>> _) => pat p T) (p0 =>> e0 :: l) ->
  ForallOrdPairs
    (fun '(p1 =>> _) '(p2 =>> _) => ortho p1 p2)
    (p0 =>> e0 :: l) ->
  ForallOrdPairs
    (fun '(_ =>> e1) '(_ =>> e2) => ortho e1 e2)
    (p0 =>> e0 :: l) ->
  (forall x : string,
   Forall (fun '(p =>> _) => free_in_pat x p -> Gamma' x = fail)
     (p0 =>> e0 :: l)) ->
  Gamma |- e : (a) T ->
  Forall
    (fun '(p =>> e') => (p |=> T; Gamma') |- e' : (pure) T')
    (p0 =>> e0 :: l) ->
  map_union Gamma Gamma' |- expr_match e (p0 =>> e0 :: l) : (a) T'.
Proof.
  intros Gamma Gamma' e p0 e0 l T T' [] Hn Hp Ho He Hd Ht Hf.
  - eapply has_type_match_pure; eauto.
    intros p e' Hi. rewrite Forall_forall in Hf. apply (Hf _ Hi).
  - eapply has_type_match_mixed'; eauto.
    + apply inclusion_union_r2. reflexivity.
    + eapply weakening; eauto.
      apply inclusion_union_r1; auto.
      reflexivity.
Qed.

Lemma has_type_match_iff :
  forall Gamma e l T' a,
  Gamma |- expr_match e l : (a) T' <->
  exists Gamma1 Gamma2 p0 e0 l' T,
  Gamma = map_union Gamma1 Gamma2 /\
  l = p0 =>> e0 :: l' /\
  no_conflict Gamma1 Gamma2 /\
  Forall (fun '(p =>> _) => pat p T) l /\
  ForallOrdPairs (fun '(p1 =>> _) '(p2 =>> _) => ortho p1 p2) l /\
  ForallOrdPairs (fun '(_ =>> e1) '(_ =>> e2) => ortho e1 e2) l /\
  (forall x : string,
   Forall (fun '(p =>> _) => free_in_pat x p -> Gamma2 x = fail) l) /\
  Gamma1 |- e : (a) T /\
  Forall (fun '(p =>> e') => (p |=> T; Gamma2) |- e' : (pure) T') l.
Proof.
  intros Gamma e l T' a. split.
  - intros H. inversion H; subst; rename e1 into e0.
    + exists Gamma0, Gamma', p0, e0, l0, T. repeat split; auto.
      apply Forall_forall. intros [p e']. auto.
    + exists Gamma, Gamma', p0, e0, l0, T. repeat split; auto.
      * symmetry. apply union_inclusion_1, H2.
      * symmetry. apply no_conflict_inclusion, H2.
      * apply Forall_forall. intros [p e']. auto.
  - intros
      [Gamma1 [Gamma2 [p0 [e0 [l' [T
      [Hu [Hl [Hn [Hp [Ho [He [Hd [Ht H']]]]]]]]]]]]]].
    subst. eapply has_type_match; eauto.
Qed.

Lemma has_type_case' :
  forall Gamma e p0 e0 l T T',
  Forall (fun '(p =>> _) => pat p T) (p0 =>> e0 :: l) ->
  ForallOrdPairs
    (fun '(p1 =>> _) '(p2 =>> _) => ortho p1 p2)
    ((p0 =>> e0) :: l) ->
  Gamma |- e : (mixed) T ->
  Forall
    (fun '(p =>> e') => (p |=> T; Gamma) |- e' : (mixed) T')
    (p0 =>> e0 :: l) ->
  Gamma |- expr_case e ((p0 =>> e0) :: l) : (mixed) T'.
Proof.
  intros Gamma e p0 e0 l T T' Hp Ho Ht Hf.
  eapply has_type_case; eauto.
  intros p e' H'. rewrite Forall_forall in Hf. apply (Hf _ H').
Qed.

Lemma has_type_case_iff :
  forall Gamma e l T',
  Gamma |- expr_case e l : (mixed) T' <->
  exists p0 e0 l' T,
  l = p0 =>> e0 :: l' /\
  Forall (fun '(p =>> _) => pat p T) l /\
  ForallOrdPairs (fun '(p1 =>> _) '(p2 =>> _) => ortho p1 p2) l /\
  Gamma |- e : (mixed) T /\
  Forall (fun '(p =>> e') => (p |=> T; Gamma) |- e' : (mixed) T') l.
Proof.
  intros Gamma e l T'. split.
  - intros H. inversion H. subst. rename e1 into e0.
    exists p0, e0, l0, T. repeat split; auto.
    apply Forall_forall. intros [p e']. auto.
  - intros [p0 [e0 [l' [T [Hl [Hp [Ho [Ht H']]]]]]]].
    subst. eapply has_type_case'; eauto.
Qed.

Lemma has_type_let' :
  forall Gamma Gamma' p e e' T T' a,
  no_conflict Gamma Gamma' ->
  pat p T ->
  (forall x, free_in_pat x p -> Gamma' x = None) ->
  Gamma |- e : (a) T ->
  (p |=> T; Gamma') |- e' : (a) T' ->
  map_union Gamma Gamma' |- qlet p := e in e' : (a) T'.
Proof.
  intros Gamma Gamma' p e e' T T' [] Hn Hp Hg Ht H'; eauto using has_type.
  eapply has_type_case'; eauto using ForallOrdPairs.
  - eapply weakening; eauto.
    apply inclusion_union_r1; auto.
    reflexivity.
  - repeat constructor. eapply weakening; eauto.
    apply inclusion_ctx_update, inclusion_union_r2. reflexivity.
Qed.

Lemma has_type_oracle :
  forall Gamma Gamma' e1 e2 e3 T a,
  no_conflict Gamma Gamma' ->
  Gamma |- e1 : (a) T ->
  Gamma' |- e2 : (a) Bit ->
  Gamma |- e3 : (mixed) Bit ->
  map_union Gamma Gamma' |- oracle (e1, e2 (+) e3) : (a) (T <*> Bit).
Proof.
  intros Gamma Gamma' e1 e2 e3 T [] Hn H1 H2 H3; eauto using has_type.
  econstructor; eapply weakening; eauto.
  - apply inclusion_union_r1; auto.
    reflexivity.
  - apply inclusion_union_r2. reflexivity.
  - apply inclusion_union_r1; auto.
    reflexivity.
Qed.

Lemma val_has_type :
  forall v T a, val v T -> |- v : (a) T.
Proof.
  intros v T [] H; induction H; eauto using has_type.
  replace empty with (map_union (A:=type) empty empty);
  eauto using has_type, no_conflict_empty_1.
Qed.

Lemma pat_has_type :
  forall p T a, pat p T -> (p |=> T) |- p : (a) T.
Proof.
  intros p T a H.
  induction H; simpl; eauto using has_type, has_type_unit, has_type_var.
  rewrite <- map_union_ctx_update_2.
  - apply has_type_pair; auto.
    intros x T T' Ht H'. exfalso. eapply H.
    + eapply defined_ctx_update_empty; eauto.
      exists T. apply Ht.
    + eapply defined_ctx_update_empty; eauto.
      exists T'. assumption.
  - intros x Hf Hd.
    eapply H; try apply Hf. eapply defined_ctx_update_empty; eauto.
Qed.

Lemma empty_pat_is_val :
  forall v T a, pat v T /\ |- v : (a) T <-> val v T.
Proof.
  intros v T a. split.
  - intros [Hp Ht]. induction Hp; inversion Ht; subst; auto using val.
    + apply update_neq_empty in H0 as [].
    + discriminate.
    + apply map_union_empty_iff in H0 as []. subst. auto using val.
  - intros H. split; auto using val_is_pat.
    induction H; destruct a; eauto using has_type.
    replace empty with (map_union (A := type) empty empty);
    eauto using has_type, no_conflict_empty_1.
Qed.

Lemma type_mixing :
  forall Gamma e T a,
  Gamma |- e : (a) T ->
  Gamma |- e : (mixed) T.
Proof.
  intros Gamma e T []; auto.
  generalize dependent T. generalize dependent Gamma.
  induction e using expr_ind'; intros Gamma T' H';
  try now (inversion H';
           eauto using has_type, has_type_pair, has_type_oracle, update_eq).
  - apply has_type_match_iff in H'. post_invert.
    eapply has_type_match_mixed'; eauto.
    + apply inclusion_union_r2. reflexivity.
    + eapply weakening; eauto.
      apply inclusion_union_r1; auto.
      reflexivity.
  - inversion H'. subst. econstructor; eauto.
    + repeat constructor.
    + eapply weakening; eauto.
      apply inclusion_union_r1; auto.
      reflexivity.
    + intros p1 e1 [H1 | Hf]; try contradiction.
      injection H1 as. subst. inversion_clear H0.
      eapply weakening; eauto.
      apply inclusion_ctx_update, inclusion_union_r2. reflexivity.
Qed.

Lemma has_type_free_defined :
  forall Gamma e a T x,
  Gamma |- e : (a) T ->
  appears_free_in x e ->
  defined_on x Gamma.
Proof.
  intros Gamma e a T x H. generalize dependent x.
  induction H using has_type_ind'; simpl; intros x' Hf; inversion Hf; subst;
  try rewrite map_union_defined_iff; auto using defined_update_eq.
  - exists T. assumption.
  - right. destruct H8 as [p [e' [Hi [H' Hn]]]].
    rewrite Forall_forall in *.
    apply (H6 _ Hi), defined_ctx_update_iff in H' as [].
    + contradiction.
    + assumption.
    + apply (H0 _ Hi).
  - eapply inclusion_defined; eauto.
    destruct H8 as [p [e' [Hi [H' Hn]]]].
    rewrite Forall_forall in *.
    apply (H6 _ Hi), defined_ctx_update_iff in H' as [].
    + contradiction.
    + assumption.
    + apply (H0 _ Hi).
  - destruct H5 as [p [e' [Hi [H' Hn]]]].
    rewrite Forall_forall in *.
    apply (H3 _ Hi), defined_ctx_update_iff in H' as [].
    + contradiction.
    + assumption.
    + apply (H _ Hi).
  - destruct H5 as [p0 [e0 [He [Hi Hn]]]]. destruct He; try contradiction.
    injection H4 as. subst.
    specialize (IHhas_type2 x' Hi).
    apply defined_ctx_update_iff in IHhas_type2 as []; auto.
    contradiction.
Qed.

Lemma has_type_defined_free :
  forall Gamma e T x,
  Gamma |- e : (pure) T ->
  defined_on x Gamma <-> appears_free_in x e.
Proof.
  intros Gamma e T x Ht. split; eauto using has_type_free_defined.
  generalize dependent x. generalize dependent T. generalize dependent Gamma.
  induction e using expr_ind'; simpl;
  intros Gamma T0 Ht x' H';
  try now (inversion Ht; subst; eauto using appears_free_in).
  - inversion Ht. subst. destruct H'. discriminate.
  - inversion Ht. subst. destruct H' as [T' H']. unfold update, t_update in H'.
    bdestruct (eqb_string x x'); try discriminate.
    subst. constructor.
  - inversion Ht. subst.
    apply map_union_defined_iff in H' as [H' | H']; eauto using appears_free_in.
  - apply has_type_match_iff in Ht. post_invert.
    apply map_union_defined_iff in H' as [H' | H']; eauto using appears_free_in.
    apply appears_free_in_match_2. exists x1, x2. simpl. split; auto.
    inversion_clear H0. inversion_clear H9. split.
    + eapply H1; eauto using defined_ctx_update.
    + specialize (H7 x'). inversion_clear H7. intros Hc.
      apply H9 in Hc. destruct H'. congruence.
  - inversion Ht. subst.
    apply map_union_defined_iff in H' as [H' | H']; eauto using appears_free_in.
    apply appears_free_in_case_2. exists p, e'. repeat split; simpl; auto.
    + inversion_clear H0.
      eapply H1; eauto using defined_ctx_update.
    + intros Hc. apply H5 in Hc. destruct H'. congruence.
  - inversion Ht. subst.
    apply map_union_defined_iff in H' as [H' | H']; eauto using appears_free_in.
Qed.

Lemma filtered_well_typed :
  forall Gamma e a T,
  Gamma |- e : (a) T ->
  filter_map (fun x => appears_free_bool x e) Gamma |- e : (a) T.
Proof.
  assert (forall Gamma e T,
          Gamma |- e : (pure) T ->
          filter_map (fun x => appears_free_bool x e) Gamma |- e : (pure) T)
    as Hp.
  { intros Gamma e T H. replace (filter_map _ Gamma) with Gamma; auto.
    symmetry. apply filter_unchanged. intros x Hx.
    bdestruct (appears_free_bool x e); auto.
    exfalso. apply H0. eapply has_type_defined_free; eauto. }
  intros Gamma e a T H. induction H using has_type_ind'; eauto using has_type.
  - constructor. unfold filter_map.
    bdestruct (appears_free_bool x x); auto.
    exfalso. apply H0. constructor.
  - constructor.
    + eapply weakening; try apply IHhas_type1.
      apply inclusion_filter_2; try reflexivity.
      intros x Hx. bdestruct (appears_free_bool x (| e1, e2 |)); auto.
      exfalso. apply H1. constructor. bdestruct (appears_free_bool x e1); auto.
      discriminate.
    + eapply weakening; try apply IHhas_type2.
      apply inclusion_filter_2; try reflexivity.
      intros x Hx. bdestruct (appears_free_bool x (| e1, e2 |)); auto.
      exfalso. apply H1. apply appears_free_in_pair_2.
      bdestruct (appears_free_bool x e2); auto.
      discriminate.
  - apply Hp. econstructor; eauto.
    intros p1 e1 Hi. rewrite Forall_forall in H5.
    apply (H5 _ Hi).
  - eapply has_type_match_mixed' with (T := T); try assumption.
    + apply inclusion_filter_2.
      * intros x Hx. apply Hx.
      * apply H.
    + intros x. specialize (H3 x). rewrite Forall_forall in *.
      intros [p e'] Hi Hf. unfold filter_map.
      bdestruct (appears_free_bool x (expr_match e (p0 =>> e0 :: l))); auto.
      apply (H3 _ Hi). assumption.
    + eapply weakening; try apply IHhas_type.
      apply inclusion_filter_2; try reflexivity.
      intros x Hx.
      bdestruct (appears_free_bool x (expr_match e ((p0 =>> e0) :: l))); auto.
      exfalso. apply H7. constructor. bdestruct (appears_free_bool x e); auto.
      discriminate.
    + rewrite filter_unchanged; auto.
      intros x Hx.
      bdestruct (appears_free_bool x (expr_match e (p0 =>> e0 :: l))); auto.
      exfalso. apply H7. apply appears_free_in_match_2.
      exists p0, e0. repeat split.
      * simpl. auto.
      * inversion_clear H5. apply has_type_defined_free with (x := x) in H8.
        apply H8, defined_ctx_update, Hx.
      * intros Hc. specialize (H3 x). inversion_clear H3. apply H8 in Hc.
        apply undefined_None in Hc. apply Hc, Hx.
  - apply has_type_case' with (T := T); auto.
    + eapply weakening; eauto.
      apply inclusion_filter_2; try reflexivity.
      intros x Hx. bdestruct (appears_free_bool x e); try discriminate.
      bdestruct (appears_free_bool x (expr_case e (p0 =>> e0 :: l)));
      auto using appears_free_in.
    + rewrite Forall_forall in *. intros [p e'] Hi.
      specialize (H3 _ Hi). specialize (H _ Hi). simpl in *.
      eapply weakening; try apply H3.
      apply inclusion_filter_ctx_update_2; auto; try reflexivity.
      intros x Hn Hf.
      eapply reflect_iff in Hf; auto with bdestruct.
      bdestruct (appears_free_bool x (expr_case e (p0 =>> e0 :: l))); auto.
      exfalso. apply H4, appears_free_in_case_2. exists p, e'.
      repeat split; auto.
  - constructor.
    + eapply weakening; try apply IHhas_type1.
      apply inclusion_filter_2; try reflexivity.
      intros x Hx.
      bdestruct (appears_free_bool x oracle (e1, e2 (+) e3));
      bdestruct (appears_free_bool x e1); auto using appears_free_in.
    + eapply weakening; try apply IHhas_type2.
      apply inclusion_filter_2; try reflexivity.
      intros x Hx.
      bdestruct (appears_free_bool x oracle (e1, e2 (+) e3));
      bdestruct (appears_free_bool x e2); auto using appears_free_in.
    + eapply weakening; try apply IHhas_type3.
      apply inclusion_filter_2; try reflexivity.
      intros x Hx.
      bdestruct (appears_free_bool x oracle (e1, e2 (+) e3));
      bdestruct (appears_free_bool x e3); auto using appears_free_in.
Qed.

Lemma has_type_let_iff :
  forall Gamma p e e' T' a,
  Gamma |- qlet p := e in e' : (a) T' <->
  exists Gamma1 Gamma2 T,
  Gamma = map_union Gamma1 Gamma2 /\
  no_conflict Gamma1 Gamma2 /\
  pat p T /\
  (forall x, free_in_pat x p -> Gamma2 x = None) /\
  Gamma1 |- e : (a) T /\
  (p |=> T; Gamma2) |- e' : (a) T'.
Proof.
  intros Gamma p e e' T' a. split.
  - intros H. destruct a.
    + inversion H. subst. exists Gamma0, Gamma', T. auto 6.
    + apply has_type_case_iff in H as [p0 [e0 [l' [T [He [Hp [Ho [Ht H']]]]]]]].
      injection He as. subst.
      inversion_clear Hp. inversion_clear H'.
      exists
        Gamma, (filter_map (fun x => negb (free_in_pat_bool x p0)) Gamma), T.
      repeat split; auto.
      * symmetry. apply union_inclusion_1, inclusion_filter_1. reflexivity.
      * apply no_conflict_filter_r. reflexivity.
      * intros x Hx. unfold filter_map.
        bdestruct (free_in_pat_bool x p0); auto.
        contradiction.
      * eapply weakening; try apply filtered_well_typed; eauto.
        replace (fun x : string => negb (free_in_pat_bool x p0))
          with (fun x : string => true && negb (free_in_pat_bool x p0))
          by reflexivity.
        rewrite <- filter_ctx_update_true_2; auto.
        apply inclusion_filter_2; auto.
        reflexivity.
  - intros [Gamma1 [Gamma2 [T [Hg [Hn [Hp [Hu [Ht H']]]]]]]]. subst.
    eapply has_type_let'; eauto.
Qed.

(* A notion of purity that only cares about an expression, not context *)
Inductive discard_free : expr -> Prop :=
  | discard_free_unit :
      discard_free ()
  | discard_free_var (x : string) :
      discard_free x
  | discard_free_left T e :
      discard_free e ->
      discard_free (lef T e)
  | discard_free_right T e :
      discard_free e ->
      discard_free (rit T e)
  | discard_free_pair e1 e2 :
      discard_free e1 ->
      discard_free e2 ->
      discard_free (| e1, e2 |)
  | discard_free_match e l :
      discard_free e ->
      discard_free (expr_match e l)
  | discard_free_let p e e' :
      (forall x, free_in_pat x p -> appears_free_in x e') ->
      discard_free e ->
      discard_free e' ->
      discard_free (qlet p := e in e')
  | discard_free_gphase e gamma :
      discard_free e ->
      discard_free (e |> gphase gamma)
  | discard_free_gate e g :
      discard_free e ->
      discard_free (e |> g)
  | discard_free_oracle e1 e2 e3 :
      (forall x, appears_free_in x e3 -> appears_free_in x e1) ->
      discard_free e1 ->
      discard_free e2 ->
      discard_free (oracle (e1, e2 (+) e3)).

(* A computable boolean for discard_free *)
Fixpoint discard_free_bool e :=
  match e with
  | () | expr_var _ => true
  | lef _ e' | rit _ e' | expr_match e' _ | e' |> gphase _ | e' |> _ =>
      discard_free_bool e'
  | (| e1, e2 |) => discard_free_bool e1 && discard_free_bool e2
  | qlet p := e1 in e2 =>
      var_subset (pat_free_vars p) (free_vars e2) &&
      discard_free_bool e1 &&
      discard_free_bool e2
  | expr_case _ _ => false
  | oracle (e1, e2 (+) e3) =>
      var_subset (free_vars e3) (free_vars e1) &&
      discard_free_bool e1 &&
      discard_free_bool e2
  end.

Lemma discard_free_bool_spec :
  forall e, reflect (discard_free e) (discard_free_bool e).
Proof.
  intros e. apply iff_reflect. split.
  - intros H. induction H; simpl; auto.
    + rewrite IHdiscard_free1. assumption.
    + bdestruct (var_subset (pat_free_vars p) (free_vars e')); auto with bool.
      exfalso. apply H2, subset_spec, Forall_forall. intros x Hx.
      apply free_vars_spec, H, pat_free_vars_spec, Hx.
    + bdestruct (var_subset (free_vars e3) (free_vars e1)); auto with bool.
      exfalso. apply H2, subset_spec, Forall_forall. intros x Hx.
      apply free_vars_spec, H, free_vars_spec, Hx.
  - induction e using expr_ind'; simpl; intros H';
    repeat apply andb_prop in H' as [H' ?]; auto using discard_free.
    + destruct l as [| [p e'] []]; try discriminate.
      repeat apply andb_prop in H' as [H' ?].
      inversion_clear H0.
      constructor; auto.
      intros x Hx.
      bdestruct (var_subset (pat_free_vars p) (free_vars e')); try discriminate.
      apply subset_spec in H0. rewrite Forall_forall in H0.
      apply free_vars_spec, H0, pat_free_vars_spec, Hx.
    + constructor; auto.
      intros x Hx.
      bdestruct (var_subset (free_vars e3) (free_vars e1)); try discriminate.
      apply subset_spec in H1. rewrite Forall_forall in H1.
      apply free_vars_spec, H1, free_vars_spec, Hx.
Qed.

#[export]
Hint Resolve discard_free_bool_spec : bdestruct.

Lemma discard_free_pure :
  forall Gamma e T,
  Gamma |- e : (mixed) T ->
  discard_free e ->
  (forall x, defined_on x Gamma -> appears_free_in x e) ->
  Gamma |- e : (pure) T.
Proof.
  intros Gamma e T Ht H. generalize dependent T. generalize dependent Gamma.
  induction H using discard_free_ind;
  intros Gamma T' Ht Hf; inversion Ht; subst.
  - replace Gamma with (empty (A:=type)); eauto using has_type.
    apply functional_extensionality. intros x. rewrite apply_empty.
    symmetry. apply undefined_None. intros Hc. apply Hf in Hc. inversion Hc.
  - replace Gamma with (x |-> T'); eauto using has_type.
    apply functional_extensionality. intros x'.
    unfold update, t_update. bdestruct (eqb_string x x'); subst; auto.
    rewrite apply_empty. symmetry. apply undefined_None. intros Hc.
    specialize (Hf x' Hc). inversion Hf. congruence.
  - constructor. apply IHdiscard_free. repeat split; auto.
    intros x Hx. apply Hf in Hx. inversion Hx. assumption.
  - constructor. apply IHdiscard_free. repeat split; auto.
    intros x Hx. apply Hf in Hx. inversion Hx. assumption.
  - replace Gamma with
      (map_union (filter_map (fun x => appears_free_bool x e1) Gamma)
                 (filter_map (fun x => appears_free_bool x e2) Gamma)).
    + constructor.
      * apply no_conflict_filter_l, no_conflict_filter_r. reflexivity.
      * apply IHdiscard_free1; auto using filtered_well_typed.
        intros x [T Hx].
        unfold filter_map in Hx. bdestruct (appears_free_bool x e1); congruence.
      * apply IHdiscard_free2; auto using filtered_well_typed.
        intros x [T Hx].
        unfold filter_map in Hx. bdestruct (appears_free_bool x e2); congruence.
    + rewrite filter_union_orb. apply filter_unchanged. intros x Hx.
      apply Hf in Hx. inversion_clear Hx.
      * bdestruct (appears_free_bool x e1); auto.
      * bdestruct (appears_free_bool x e2); auto with bool.
  - replace Gamma
      with
      (map_union (filter_map (fun x => appears_free_bool x e) Gamma) Gamma').
    + apply has_type_match with (T := T); auto.
      * apply no_conflict_filter_l. symmetry. apply no_conflict_inclusion, H2.
      * apply IHdiscard_free; eauto using filtered_well_typed.
        intros x Hx. apply defined_on_filter in Hx as [Hx _].
        bdestruct (appears_free_bool x e); congruence.
      * rewrite Forall_forall in *.
        intros [p2 e2] Hi. apply (H9 _ _ Hi).
    + assert (no_conflict Gamma Gamma') as Hc.
      { symmetry. auto using no_conflict_inclusion. }
      apply inclusion_eq.
      * intros x1 T1 Hu. apply map_union_spec in Hu as [Hu | Hu];
        auto using no_conflict_filter_l.
        eapply inclusion_filter_1; try reflexivity.
        eauto.
      * intros x1 T1 Hu. apply map_union_spec; auto using no_conflict_filter_l.
        assert (defined_on x1 Gamma) as Hd. { exists T1. assumption. }
        apply Hf in Hd. inversion_clear Hd.
        -- left. unfold filter_map. bdestruct (appears_free_bool x1 e); auto.
           contradiction.
        -- right. destruct H0 as [p2 [e2 [Hi [He Hn]]]].
           specialize (H9 _ _ Hi).
           eapply has_type_free_defined in H9; eauto.
           rewrite Forall_forall in H3. specialize (H3 _ Hi).
           apply defined_ctx_update_iff in H9 as [H0 | [T0 H0]]; auto;
           try contradiction.
           apply H2 in H0 as H'. rewrite Hu in H'. injection H' as. subst.
           assumption.
  - inversion_clear H7. specialize (H11 p e'). simpl in H11.
    replace Gamma with
      (map_union (filter_map (fun x => appears_free_bool x e) Gamma)
      (filter_map
      (fun x => appears_free_bool x e' && negb (free_in_pat_bool x p)) Gamma)).
    + econstructor; eauto.
      * apply no_conflict_filter_l, no_conflict_filter_r. reflexivity.
      * intros x Hx. unfold filter_map.
        bdestruct (free_in_pat_bool x p); try contradiction.
        simpl. rewrite andb_false_r. reflexivity.
      * apply IHdiscard_free1; auto using filtered_well_typed.
        intros x [Tx Hx].
        unfold filter_map in Hx. bdestruct (appears_free_bool x e); congruence.
      * rewrite <- filter_ctx_update_true_2; auto.
        -- apply IHdiscard_free2; auto using filtered_well_typed.
           intros x Hx. apply defined_on_filter in Hx as [Hx _].
           bdestruct (appears_free_bool x e'); congruence.
        -- intros x Hx. apply H in Hx.
           bdestruct (appears_free_bool x e'); congruence.
    + apply inclusion_eq.
      * intros x1 T1 Hu.
        apply map_union_spec in Hu as [Hu | Hu];
        try eapply inclusion_filter_1;
        try apply no_conflict_filter_l, no_conflict_filter_r;
        eauto; reflexivity.
      * intros x1 T1 Hg. apply map_union_spec.
        -- apply no_conflict_filter_l, no_conflict_filter_r. reflexivity.
        -- assert (defined_on x1 Gamma) as Hd. { exists T1. assumption. }
           apply Hf in Hd. inversion_clear Hd.
           ++ left. unfold filter_map. bdestruct (appears_free_bool x1 e); auto.
              contradiction.
           ++ right.
              destruct H4 as [p1 [e1 [[Hi |] [Ha Hn]]]]; try contradiction.
              injection Hi as. subst. unfold filter_map.
              bdestruct (appears_free_bool x1 e1);
              bdestruct (free_in_pat_bool x1 p1);
              auto; contradiction.
  - constructor. apply IHdiscard_free; auto.
    intros x Hx. specialize (Hf x Hx). inversion Hf. assumption.
  - constructor. apply IHdiscard_free; auto.
    intros x Hx. specialize (Hf x Hx). inversion Hf. assumption.
  - replace Gamma
    with (map_union (filter_map (fun x => appears_free_bool x e1) Gamma)
                    (filter_map (fun x => appears_free_bool x e2) Gamma)).
    + constructor.
      * apply no_conflict_filter_l, no_conflict_filter_r. reflexivity.
      * apply IHdiscard_free1; auto using filtered_well_typed.
        intros x Hx. apply defined_on_filter in Hx as [Hx _].
        bdestruct (appears_free_bool x e1); congruence.
      * apply IHdiscard_free2; auto using filtered_well_typed.
        intros x Hx. apply defined_on_filter in Hx as [Hx _].
        bdestruct (appears_free_bool x e2); congruence.
      * eapply weakening.
        -- apply inclusion_filter_2 with (f := fun x => appears_free_bool x e3);
           try reflexivity.
           intros x Hx. bdestruct (appears_free_bool x e1); auto.
           exfalso. apply H2, H.
           bdestruct (appears_free_bool x e3); auto; discriminate.
        -- apply filtered_well_typed, H9.
    + apply inclusion_eq.
      * intros x' T' Hu. apply map_union_spec in Hu as [Hu | Hu];
        try eapply inclusion_filter_1;
        try apply no_conflict_filter_l, no_conflict_filter_r;
        eauto; reflexivity.
      * intros x' T' H'. apply map_union_spec.
        -- apply no_conflict_filter_l, no_conflict_filter_r. reflexivity.
        -- assert (defined_on x' Gamma) as Hd. { exists T'. assumption. }
           apply Hf in Hd. inversion_clear Hd.
           ++ left. unfold filter_map.
              bdestruct (appears_free_bool x' e1); auto.
              contradiction.
           ++ right. unfold filter_map.
              bdestruct (appears_free_bool x' e2); auto.
              contradiction.
           ++ left. unfold filter_map.
              bdestruct (appears_free_bool x' e1); auto.
              exfalso. auto.
Qed.

Lemma discard_free_pure_iff :
  forall Gamma e T,
  Gamma |- e : (pure) T <->
  Gamma |- e : (mixed) T /\
    discard_free e /\
    forall x, defined_on x Gamma -> appears_free_in x e.
Proof.
  intros Gamma e T.
  split; try (intros [Ht [Hd Hf]]; auto using discard_free_pure).
  intros Ht. split; eauto using type_mixing. 
  split; try (intros x Hx; eapply has_type_defined_free in Hx; eauto).
  generalize dependent T. generalize dependent Gamma.
  induction e using expr_ind'; intros Gamma T' Ht;
  inversion Ht; subst; constructor; eauto using discard_free.
  - intros x Hx. rewrite <- has_type_defined_free; eauto.
    apply defined_ctx_update_iff; auto.
  - inversion_clear H0. eapply H1; eauto.
  - intros x Hx. eapply has_type_defined_free; eauto.
    eapply has_type_free_defined; eauto.
Qed.

(* An alternative typing relation, useful for typechecking *)
Reserved Notation "Gamma ||- e : T" (at level 40, e at next level).
Inductive has_type_alt : ctx -> expr -> type -> Prop :=
  | has_type_alt_unit Gamma :
      Gamma ||- () : Unit
  | has_type_alt_var Gamma x T :
      Gamma x = Some T ->
      Gamma ||- x : T
  | has_type_alt_left Gamma e T0 T1 :
      Gamma ||- e : T0 ->
      Gamma ||- lef T1 e : (T0 <+> T1)
  | has_type_alt_right Gamma e T0 T1 :
      Gamma ||- e : T1 ->
      Gamma ||- rit T0 e : (T0 <+> T1)
  | has_type_alt_pair Gamma e1 e2 T1 T2 :
      Gamma ||- e1 : T1 ->
      Gamma ||- e2 : T2 ->
      Gamma ||- (| e1, e2 |) : (T1 <*> T2)
  | has_type_alt_match Gamma e p0 e0 l T T' :
      Forall (fun '(p =>> _) => pat p T) (p0 =>> e0 :: l) ->
      ForallOrdPairs (fun '(p1 =>> _) '(p2 =>> _) => ortho p1 p2)
        (p0 =>> e0 :: l) ->
      ForallOrdPairs (fun '(_ =>> e1) '(_ =>> e2) => ortho e1 e2)
        (p0 =>> e0 :: l) ->
      (forall x,
       IffAll
         (fun '(p1 =>> e1) =>
          appears_free_in x e1 /\ ~ free_in_pat x p1)
         ((p0 =>> e0) :: l)) ->
      (forall x,
       Forall
         (fun '(p =>> e') => free_in_pat x p -> appears_free_in x e')
         (p0 =>> e0 :: l)) ->
      Forall (fun '(_ =>> e') => discard_free e') (p0 =>> e0 :: l) ->
      Gamma ||- e : T ->
      (forall p e',
       set_In (p =>> e') ((p0 =>> e0) :: l) -> (p |=> T; Gamma) ||- e' : T') ->
      Gamma ||- expr_match e ((p0 =>> e0) :: l) : T'
  | has_type_alt_case Gamma e p0 e0 l T T' :
      Forall (fun '(p =>> _) => pat p T) (p0 =>> e0 :: l) ->
      ForallOrdPairs (fun '(p1 =>> _) '(p2 =>> _) => ortho p1 p2)
        (p0 =>> e0 :: l) ->
      Gamma ||- e : T ->
      (forall p e',
       set_In (p =>> e') ((p0 =>> e0) :: l) -> (p |=> T; Gamma) ||- e' : T') ->
      Gamma ||- expr_case e ((p0 =>> e0) :: l) : T'
  | has_type_alt_gphase Gamma e gamma T :
      Gamma ||- e : T ->
      Gamma ||- e |> gphase gamma : T
  | has_type_alt_gate Gamma e g :
      Gamma ||- e : Bit ->
      Gamma ||- e |> g : Bit
  | has_type_alt_oracle Gamma e1 e2 e3 T :
      Gamma ||- e1 : T ->
      Gamma ||- e2 : Bit ->
      Gamma ||- e3 : Bit ->
      Gamma ||- oracle (e1, e2 (+) e3) : (T <*> Bit)
where "Gamma ||- e : T" := (has_type_alt Gamma e T).

Lemma has_type_alt_match' :
  forall Gamma e p0 e0 l T T',
  Forall (fun '(p =>> _) => pat p T) (p0 =>> e0 :: l) ->
  ForallOrdPairs (fun '(p1 =>> _) '(p2 =>> _) => ortho p1 p2)
    (p0 =>> e0 :: l) ->
  ForallOrdPairs (fun '(_ =>> e1) '(_ =>> e2) => ortho e1 e2)
    (p0 =>> e0 :: l) ->
  (forall x,
   IffAll
     (fun '(p1 =>> e1) =>
      appears_free_in x e1 /\ ~ free_in_pat x p1)
     ((p0 =>> e0) :: l)) ->
  (forall x,
   Forall
     (fun '(p =>> e') => free_in_pat x p -> appears_free_in x e')
     (p0 =>> e0 :: l)) ->
  Forall (fun '(_ =>> e') => discard_free e') (p0 =>> e0 :: l) ->
  Gamma ||- e : T ->
  Forall (fun '(p =>> e') => (p |=> T; Gamma) ||- e' : T') (p0 =>> e0 :: l) ->
  Gamma ||- expr_match e ((p0 =>> e0) :: l) : T'.
Proof.
  intros Gamma e p0 e0 l T T' Hp Ho He Hi Hf Hd Ht H'.
  eapply has_type_alt_match; eauto.
  intros p e' Hs. rewrite Forall_forall in H'. specialize (H' _ Hs). apply H'.
Qed.

Lemma has_type_alt_case' :
  forall Gamma e p0 e0 l T T',
  Forall (fun '(p =>> _) => pat p T) (p0 =>> e0 :: l) ->
  ForallOrdPairs (fun '(p1 =>> _) '(p2 =>> _) => ortho p1 p2)
    (p0 =>> e0 :: l) ->
  Gamma ||- e : T ->
  Forall (fun '(p =>> e') => (p |=> T; Gamma) ||- e' : T') (p0 =>> e0 :: l) ->
  Gamma ||- expr_case e ((p0 =>> e0) :: l) : T'.
Proof.
  intros Gamma e p0 e0 l T T' Hp Ho He H'. eapply has_type_alt_case; eauto.
  intros p e' Hi. rewrite Forall_forall in H'. specialize (H' _ Hi). apply H'.
Qed.

(* An alternative induction principle that uses Forall *)
Lemma has_type_alt_ind' :
  forall P : ctx -> expr -> type -> Prop,
  (forall Gamma, P Gamma () Unit) ->
  (forall Gamma x T, Gamma x = Some T -> P Gamma x T) ->
  (forall Gamma e T0 T1,
   Gamma ||- e : T0 -> P Gamma e T0 -> P Gamma (lef T1 e) (T0 <+> T1)) ->
  (forall Gamma e T0 T1,
   Gamma ||- e : T1 -> P Gamma e T1 -> P Gamma (rit T0 e) (T0 <+> T1)) ->
  (forall Gamma e1 e2 T1 T2,
   Gamma ||- e1 : T1 -> P Gamma e1 T1 ->
   Gamma ||- e2 : T2 -> P Gamma e2 T2 ->
   P Gamma (|e1, e2|) (T1 <*> T2)) ->
  (forall Gamma e p0 e0 l T T',
   Forall (fun '(p =>> _) => pat p T) (p0 =>> e0 :: l) ->
   ForallOrdPairs (fun '(p1 =>> _) '(p2 =>> _) => ortho p1 p2)
     (p0 =>> e0 :: l) ->
   ForallOrdPairs (fun '(_ =>> e1) '(_ =>> e2) => ortho e1 e2)
     (p0 =>> e0 :: l) ->
   (forall x,
    IffAll
      (fun '(p1 =>> e1) => appears_free_in x e1 /\ ~ free_in_pat x p1)
      (p0 =>> e0 :: l)) ->
   (forall x,
    Forall (fun '(p =>> e') => free_in_pat x p -> appears_free_in x e')
      (p0 =>> e0 :: l)) ->
   Forall (fun '(_ =>> e') => discard_free e') (p0 =>> e0 :: l) ->
   Gamma ||- e : T -> P Gamma e T ->
   Forall (fun '(p =>> e') => (p |=> T; Gamma) ||- e' : T') (p0 =>> e0 :: l) ->
   Forall (fun '(p =>> e') => P (p |=> T; Gamma) e' T') (p0 =>> e0 :: l) ->
   P Gamma (expr_match e (p0 =>> e0 :: l)) T') ->
  (forall Gamma e p0 e0 l T T',
   Forall (fun '(p =>> _) => pat p T) (p0 =>> e0 :: l) ->
   ForallOrdPairs (fun '(p1 =>> _) '(p2 =>> _) => ortho p1 p2)
     (p0 =>> e0 :: l) ->
   Gamma ||- e : T -> P Gamma e T ->
   Forall (fun '(p =>> e') => (p |=> T; Gamma) ||- e' : T') (p0 =>> e0 :: l) ->
   Forall (fun '(p =>> e') => P (p |=> T; Gamma) e' T') (p0 =>> e0 :: l) ->
   P Gamma (expr_case e (p0 =>> e0 :: l)) T') ->
  (forall Gamma e gamma T,
   Gamma ||- e : T -> P Gamma e T ->
   P Gamma (e |> gphase gamma) T) ->
  (forall Gamma e g,
   Gamma ||- e : Bit -> P Gamma e Bit ->
   P Gamma (e |> g) Bit) ->
  (forall Gamma e1 e2 e3 T,
   Gamma ||- e1 : T -> P Gamma e1 T ->
   Gamma ||- e2 : Bit -> P Gamma e2 Bit ->
   Gamma ||- e3 : Bit -> P Gamma e3 Bit ->
   P Gamma oracle (e1, e2 (+) e3) (T <*> Bit)) ->
  forall Gamma e T, Gamma ||- e : T -> P Gamma e T.
Proof.
  intros
    P H_unit H_var H_left H_right H_pair H_match H_case H_gphase H_gate H_oracle
    Gamma e T H.
  induction H; auto.
  - apply H_match with (T := T); auto;
    apply Forall_forall; intros [p e'] Hi; auto.
  - apply H_case with (T := T); auto;
    apply Forall_forall; intros [p e'] Hi; auto.
Qed.

Lemma has_type_alt_spec :
  forall e Gamma T,
  Gamma ||- e : T <->
  Gamma |- e : (mixed) T.
Proof.
  intros e Gamma T. split.
  - intros H. induction H using has_type_alt_ind'; eauto using has_type.
    + apply has_type_match_mixed with
        (Gamma' :=
         filter_map (fun x =>
                     appears_free_bool x e0 &&
                     negb (free_in_pat_bool x p0)) Gamma)
        (T := T);
      auto; try (apply inclusion_filter_1; reflexivity).
      * intros x. apply Forall_forall. intros [p e'] Hi Hp.
        unfold filter_map.
        bdestruct (appears_free_bool x e0); auto.
        bdestruct (free_in_pat_bool x p0); auto.
        exfalso.
        specialize (H2 x). unfold IffAll in H2. rewrite Forall_forall in H2.
        apply H2 in Hi as [_ Hc]; auto.
      * intros p e' Hi. rewrite Forall_forall in *.
        specialize (H _ Hi). specialize (H4 _ Hi).
        specialize (H6 _ Hi). specialize (H7 _ Hi).
        simpl in *.
        replace (fun x => _) with
          (fun x => appears_free_bool x e' && negb (free_in_pat_bool x p)).
        -- rewrite <- filter_ctx_update_true_2.
           ++ apply discard_free_pure; auto using filtered_well_typed.
              intros x Hx. apply defined_on_filter in Hx as [Hx _].
              bdestruct (appears_free_bool x e'); auto. discriminate.
           ++ assumption.
           ++ intros x Hx. specialize (H3 x). rewrite Forall_forall in H3.
              specialize (H3 _ Hi).
              bdestruct (appears_free_bool x e'); auto.
        -- apply functional_extensionality. intros x. specialize (H2 x).
           bdestruct (appears_free_bool x e0 && negb (free_in_pat_bool x p0));
           bdestruct (appears_free_bool x e' && negb (free_in_pat_bool x p));
           auto.
           ++ exfalso. apply H9.
              unfold IffAll in H2. rewrite Forall_forall in H2.
              apply H2 in Hi; auto.
           ++ exfalso. apply H8.
              unfold IffAll in H2.
              assert
                (Exists (fun '(p1 =>> e1) =>
                         appears_free_in x e1 /\
                         ~ free_in_pat x p1) (p0 =>> e0 :: l)) as He.
              { apply Exists_exists. exists (p =>> e'). auto. }
              apply H2 in He. inversion He. assumption.
    + apply has_type_case with (T := T); auto.
      intros p1 e1 Hi.
      rewrite Forall_forall in H3. apply (H3 _ Hi).
  - generalize dependent T. generalize dependent Gamma.
    induction e using expr_ind'; intros Gamma T' H';
    inversion H'; subst; eauto using has_type_alt.
    + rename e1 into e0. apply has_type_alt_match with (T := T); auto.
      * intros x Hx. apply Exists_exists in Hx as [[p1 e1] [Hi [Hf Hn]]].
        specialize (H7 x). rewrite Forall_forall in *.
        specialize (H10 _ _ Hi) as Ht. specialize (H4 _ Hi) as Hp.
        apply has_type_defined_free with (x := x) in Ht.
        apply Ht in Hf.
        apply defined_ctx_update_iff in Hf as [| Hd]; auto; try contradiction.
        intros [p2 e2] Hi2.
        apply H10 in Hi2 as Hc. apply H4 in Hi2 as Ha. apply H7 in Hi2 as Hu.
        apply has_type_defined_free with (x := x) in Hc.
        rewrite <- Hc, defined_ctx_update_iff; auto.
        split; auto.
        intros Hf. apply Hu in Hf. apply undefined_None in Hf. contradiction.
      * intros x. apply Forall_forall. intros [p e'] Hi Hp.
        eapply has_type_defined_free; eauto.
        apply defined_ctx_update_free; auto.
        rewrite Forall_forall in H4. apply (H4 _ Hi).
      * apply Forall_forall. intros [p e'] Hi.
        specialize (H10 _ _ Hi). apply discard_free_pure_iff in H10 as [_ []].
        assumption.
      * intros p e' Hi. specialize (H10 _ _ Hi).
        rewrite Forall_forall in H0. specialize (H0 _ Hi). apply H0.
        eapply weakening; eauto using type_mixing.
        apply inclusion_ctx_update, H3.
    + rename e1 into e0. apply has_type_alt_case with (T := T); auto.
      intros p e' Hi. specialize (H7 _ _ Hi).
      rewrite Forall_forall in H0. specialize (H0 _ Hi). auto.
Qed.

(* A typechecker for mixed expressions only *)
Fixpoint type_check_mixed (Gamma : ctx) e :=
  match e with
  | () =>
      return Unit
  | expr_var x =>
      Gamma x
  | lef T1 e' =>
      T0 <- type_check_mixed Gamma e';;
      return T0 <+> T1
  | rit T0 e' =>
      T1 <- type_check_mixed Gamma e';;
      return T0 <+> T1
  | (| e1, e2 |) =>
      T1 <- type_check_mixed Gamma e1;;
      T2 <- type_check_mixed Gamma e2;;
      return T1 <*> T2
  | expr_match e [] =>
      fail
  | expr_match e (p0 =>> e0 :: l) =>
      T <- type_check_mixed Gamma e;;
      T' <- type_check_mixed (p0 |=> T; Gamma) e0;;
      if forallb (fun '(p =>> _) => pat_bool p T) (p0 =>> e0 :: l) &&
         forall_ord_pairs
           (fun '(p1 =>> _) '(p2 =>> _) => ortho_bool p1 p2)
           (p0 =>> e0 :: l) &&
         forall_ord_pairs
           (fun '(_ =>> e1) '(_ =>> e2) => ortho_bool e1 e2)
           (p0 =>> e0 :: l) &&
         forallb
           (fun '(p =>> e') =>
            eqb_var_set (var_set_diff (free_vars e0) (pat_free_vars p0))
                        (var_set_diff (free_vars e') (pat_free_vars p)))
           l &&
         forallb
           (fun '(p =>> e') => var_subset (pat_free_vars p) (free_vars e'))
           (p0 =>> e0 :: l) &&
         forallb (fun '(_ =>> e') => discard_free_bool e') (p0 =>> e0 :: l) &&
         forallb
           (fun '(p =>> e') => match type_check_mixed (p |=> T; Gamma) e' with
                               | None => false
                               | Some T0 => eqb_type T' T0
                               end)
           l
      then return T'
      else fail
  | expr_case e [] =>
      fail
  | expr_case e (p0 =>> e0 :: l) =>
      T <- type_check_mixed Gamma e;;
      T' <- type_check_mixed (p0 |=> T; Gamma) e0;;
      if forallb (fun '(p =>> e') => pat_bool p T) (p0 =>> e0 :: l) &&
         forall_ord_pairs
           (fun '(p1 =>> _) '(p2 =>> _) => ortho_bool p1 p2)
           (p0 =>> e0 :: l) &&
         forallb
           (fun '(p =>> e') => match type_check_mixed (p |=> T; Gamma) e' with
                               | None => false
                               | Some T0 => eqb_type T' T0
                               end)
           l
      then return T'
      else fail
  | e' |> gphase _ =>
      type_check_mixed Gamma e'
  | e' |> _ =>
      match type_check_mixed Gamma e' with
      | Some Bit => return Bit
      | _ => fail
      end
  | oracle (e1, e2 (+) e3) => 
      T <- type_check_mixed Gamma e1;;
      match type_check_mixed Gamma e2, type_check_mixed Gamma e3 with
      | Some Bit, Some Bit => return (T <*> Bit)
      | _, _ => fail
      end
  end.

Lemma type_check_mixed_spec :
  forall e Gamma T,
  type_check_mixed Gamma e = Some T <-> Gamma |- e : (mixed) T.
Proof.
  intros e Gamma T. rewrite <- has_type_alt_spec. split.
  - generalize dependent T. generalize dependent Gamma.
    induction e using expr_ind'; cbn - [ forall_ord_pairs forallb ];
    intros Gamma T' H'; eauto using has_type_alt.
    + injection H' as. subst. constructor.
    + destruct (type_check_mixed Gamma e) as [T0 |] eqn:E; try discriminate.
      injection H' as. subst. constructor. apply IHe, E.
    + destruct (type_check_mixed Gamma e) as [T1 |] eqn:E; try discriminate.
      injection H' as. subst. constructor. apply IHe, E.
    + destruct (type_check_mixed Gamma e1) as [T1 |] eqn:E1; try discriminate.
      destruct (type_check_mixed Gamma e2) as [T2 |] eqn:E2; try discriminate.
      apply IHe1 in E1. apply IHe2 in E2.
      injection H' as. subst. eauto using has_type_alt.
    + destruct l as [| [p0 e0] l]; try discriminate.
      destruct (type_check_mixed _ _) as [T |] eqn:Et; try discriminate.
      destruct (type_check_mixed _ e0) as [T0 |] eqn:E0; try discriminate.
      destruct (forallb _ _) eqn:Ep; try discriminate.
      destruct (forall_ord_pairs _ _) eqn:Eo; try discriminate.
      destruct
        (forall_ord_pairs (fun '(_ =>> e1) '(_ =>> e2) => ortho_bool e1 e2) _)
        eqn:Ee;
      try discriminate.
      destruct (forallb _ l) eqn:El; try discriminate.
      destruct (forallb (fun '(_ =>> _) => var_subset _ _) _) eqn:Es;
      try discriminate.
      destruct (forallb (fun '(_ =>> _) => discard_free_bool _) _) eqn:Ed;
      try discriminate.
      destruct (forallb (fun '(_ =>> _) => match type_check_mixed _ _ with 
                                           | Some _ => _
                                           | None => _ end) l) eqn:E';
      try discriminate.
      injection H' as. subst.
      apply has_type_alt_match with (T := T).
      * eapply reflect_iff; try apply Ep.
        apply forallb_spec. intros [p e']. auto with bdestruct.
      * eapply reflect_iff; try apply Eo.
        apply forall_ord_pairs_spec.
        intros [p1 e1] [p2 e2]. auto with bdestruct.
      * eapply reflect_iff; try apply Ee.
        apply forall_ord_pairs_spec.
        intros [p1 e1] [p2 e2]. auto with bdestruct.
      * intros x. apply IffAll_cons_forall.
        apply Forall_forall. intros [p e'] Hi.
        rewrite forallb_forall in El. specialize (El _ Hi). simpl in El.
        eapply reflect_iff in El; auto with bdestruct.
        rewrite eq_set_spec in El. specialize (El x).
        repeat rewrite
          var_set_diff_spec, free_vars_spec, pat_free_vars_spec in El.
        assumption.
      * intros x. apply Forall_forall. intros [p e'] Hi.
        rewrite forallb_forall in Es. specialize (Es _ Hi). simpl in Es.
        eapply reflect_iff in Es; auto with bdestruct.
        apply subset_spec in Es. rewrite Forall_forall in Es.
        rewrite <- free_vars_spec, <- pat_free_vars_spec. apply Es.
      * eapply reflect_iff; try apply Ed.
        apply forallb_spec. intros [p e']. auto with bdestruct.
      * apply IHe, Et.
      * intros p e' Hi. rewrite Forall_forall in H0. apply (H0 _ Hi).
        destruct Hi as [He | Hi]; try (injection He as; subst; assumption).
        rewrite forallb_forall in E'. specialize (E' _ Hi). simpl in E'.
        destruct (type_check_mixed (p |=> T; Gamma) e'); try discriminate.
        bdestruct (T' =? t); subst; auto.
        discriminate.
    + destruct l as [| [p0 e0] l]; try discriminate.
      destruct (type_check_mixed _ _) as [T |] eqn:Et; try discriminate.
      destruct (type_check_mixed _ e0) as [T0 |] eqn:E0; try discriminate.
      destruct (forallb _ _) eqn:Ep; try discriminate.
      destruct (forall_ord_pairs _ _) eqn:Eo; try discriminate.
      destruct (forallb (fun '(_ =>> _) => match type_check_mixed _ _ with 
                                           | Some _ => _
                                           | None => _ end) l) eqn:E';
      try discriminate.
      injection H' as. subst.
      apply has_type_alt_case with (T := T).
      * eapply reflect_iff; try apply Ep.
        apply forallb_spec. intros [p e']. auto with bdestruct.
      * eapply reflect_iff; try apply Eo.
        apply forall_ord_pairs_spec.
        intros [p1 e1] [p2 e2]. auto with bdestruct.
      * apply IHe, Et.
      * intros p e' Hi. rewrite Forall_forall in H0. apply (H0 _ Hi).
        destruct Hi as [He | Hi]; try (injection He as; subst; assumption).
        rewrite forallb_forall in E'. specialize (E' _ Hi). simpl in E'.
        destruct (type_check_mixed (p |=> T; Gamma) e'); try discriminate.
        bdestruct (T' =? t); subst; auto.
        discriminate.
    + destruct (type_check_mixed Gamma e) as [[| [] [] |] |] eqn:Et;
      try discriminate.
      injection H' as. subst. constructor. auto.
    + destruct (type_check_mixed Gamma e1) as [T |] eqn:E1;
      destruct (type_check_mixed Gamma e2) as [[| [] [] |] |] eqn:E2;
      destruct (type_check_mixed Gamma e3) as [[| [] [] |] |] eqn:E3;
      try discriminate.
      injection H' as. subst. constructor; auto.
  - intros H.
    induction H using has_type_alt_ind'; cbn - [ forallb forall_ord_pairs ];
    try congruence.
    + rewrite IHhas_type_alt. reflexivity.
    + rewrite IHhas_type_alt. reflexivity.
    + rewrite IHhas_type_alt1, IHhas_type_alt2. reflexivity.
    + rewrite IHhas_type_alt. inversion_clear H7. rewrite H8.
      destruct (_ && _) eqn:E; try reflexivity.
      exfalso. repeat rewrite andb_false_iff in E.
      destruct E as [[[[[[E | E] | E] | E] | E] | E] | E];
      apply not_true_iff_false in E; apply E.
      * erewrite <- reflect_iff; try apply H.
        apply forallb_spec. intros [p e']. auto with bdestruct.
      * erewrite <- reflect_iff; try apply H0.
        apply forall_ord_pairs_spec.
        intros [p1 e1] [p2 e2]. auto with bdestruct.
      * erewrite <- reflect_iff; try apply H1.
        apply forall_ord_pairs_spec.
        intros [p1 e1] [p2 e2]. auto with bdestruct.
      * apply forallb_forall. intros [p e'] Hi.
        erewrite <- reflect_iff; try apply eqb_var_set_spec.
        apply eq_set_spec. intros x.
        repeat rewrite var_set_diff_spec, free_vars_spec, pat_free_vars_spec.
        specialize (H2 x). rewrite IffAll_cons_forall, Forall_forall in H2.
        apply (H2 _ Hi).
      * apply forallb_forall. intros [p e'] Hi.
        erewrite <- reflect_iff; try apply var_subset_spec.
        apply subset_spec, Forall_forall.
        intros x Hx.
        specialize (H3 x). rewrite Forall_forall in H3.
        apply free_vars_spec, (H3 _ Hi), pat_free_vars_spec, Hx.
      * erewrite <- reflect_iff; try apply H4.
        apply forallb_spec. intros [p e']. auto with bdestruct.
      * erewrite <- reflect_iff; try apply H9.
        apply forallb_spec. intros [p e'].
        destruct (type_check_mixed (p |=> T; Gamma) e') as [T0 |];
        try (constructor; discriminate).
        bdestruct (T' =? T0); subst; constructor; congruence.
    + rewrite IHhas_type_alt. inversion_clear H3. rewrite H4.
      destruct (_ && _) eqn:E; try reflexivity.
      exfalso. repeat rewrite andb_false_iff in E.
      destruct E as [[E | E] | E]; apply not_true_iff_false in E; apply E.
      * erewrite <- reflect_iff; try apply H.
        apply forallb_spec. intros [p e']. auto with bdestruct.
      * erewrite <- reflect_iff; try apply H0.
        apply forall_ord_pairs_spec.
        intros [p1 e1] [p2 e2]. auto with bdestruct.
      * erewrite <- reflect_iff; try apply H5.
        apply forallb_spec. intros [p e'].
        destruct (type_check_mixed (p |=> T; Gamma) e') as [T0 |];
        try (constructor; discriminate).
        bdestruct (T' =? T0); subst; constructor; congruence.
    + rewrite IHhas_type_alt. reflexivity.
    + rewrite IHhas_type_alt1, IHhas_type_alt2, IHhas_type_alt3. reflexivity.
Qed.

(* Simple combination of discard_free_bool and type_check_mixed *)
Definition type_check_pure Gamma e :=
  if discard_free_bool e
  then type_check_mixed Gamma e
  else None.

Lemma type_check_pure_spec :
  forall Gamma e T,
  (forall x, defined_on x Gamma -> appears_free_in x e) ->
  type_check_pure Gamma e = Some T <-> Gamma |- e : (pure) T.
Proof.
  intros Gamma e T H. unfold type_check_pure. bdestruct (discard_free_bool e).
  - rewrite discard_free_pure_iff. split.
    + intros Ht. repeat split; auto. apply type_check_mixed_spec, Ht.
    + intros [Ht [Hd Hf]]. apply type_check_mixed_spec, Ht.
  - split; try discriminate.
    intros Ht. apply discard_free_pure_iff in Ht as [Ht [Hd Hf]]. contradiction.
Qed.

Lemma has_type_deterministic :
  forall Gamma e a a' T T',
  Gamma |- e : (a) T ->
  Gamma |- e : (a') T' ->
  T = T'.
Proof.
  intros Gamma e a a' T T' H H'.
  apply type_mixing, type_check_mixed_spec in H, H'. congruence.
Qed.

(* Typing of functions *)
Definition has_fun_type f T a T' :=
  forall Gamma e,
  Gamma |- e : (a) T ->
  Gamma |- f e : (a) T'.

Notation "|-- f : T ->> ( a ) T'" :=
    (has_fun_type f T a T') (at level 40, f at next level).

Lemma has_fun_type_mixed :
  forall f T T' Gamma x,
  |-- f : T ->> (mixed) T' ->
  Gamma x = Some T ->
  Gamma |- f x : (mixed) T'.
Proof. eauto using has_type. Qed.

Lemma has_fun_type_pure :
  forall f T T' x,
  |-- f : T ->> (pure) T' ->
  (x |-> T) |- f x : (pure) T'.
Proof. eauto using has_type. Qed.

Lemma has_fun_type_abs :
  forall p e T T' a,
  pat p T ->
  (p |=> T) |- e : (a) T' <-> |-- (\p ->> e) : T ->> (a) T'.
Proof.
  intros p e T T' a Hp. split.
  - intros H Gamma e' H'.
    replace Gamma with (map_union Gamma empty) by apply map_union_empty_2.
    eapply has_type_let'; eauto using no_conflict_empty_2.
  - intros H. specialize (H (p |=> T) p (pat_has_type a Hp)).
    apply has_type_let_iff in H
      as [Gamma [Gamma' [T0 [Hg [Hn [Ha [Hu [Ht He]]]]]]]].
    assert (Gamma' = empty).
    { apply functional_extensionality. intros x.
      bdestruct (free_in_pat_bool x p); auto.
      apply undefined_None. intros Hc.
      apply map_union_defined_2 with (m := Gamma) in Hc.
      rewrite <- Hg in Hc. apply defined_ctx_update_empty in Hc; auto. }
    subst. rewrite map_union_empty_2 in Hg. subst.
    specialize (pat_has_type a Hp) as H2.
    specialize (has_type_deterministic Ht H2) as Hd. subst. assumption.
Qed.

(* A listing of all of the values of a given type *)
Fixpoint value_listing T : set expr :=
  match T with
  | Unit => [()]
  | T0 <+> T1 =>
      map (expr_left T1) (value_listing T0) ++
      map (expr_right T0) (value_listing T1)
  | T1 <*> T2 =>
      map of_pair (list_prod (value_listing T1) (value_listing T2))
  end.

Lemma value_listing_correct :
  forall T,
  Forall (fun v => val v T) (value_listing T).
Proof.
  intros T. induction T; simpl.
  - repeat constructor.
  - rewrite Forall_forall in *. intros v Hv.
    apply in_app_or in Hv as [Hv | Hv];
    apply in_map_iff in Hv as [v' [Hv Hi]]; subst; auto using val.
  - rewrite Forall_forall in *. intros v Hv.
    apply in_map_iff in Hv as [[v1 v2] [Hv Hi]]. unfold of_pair in Hv. subst.
    apply in_prod_iff in Hi as [H1 H2]. auto using val.
Qed.

(* Choose the "first" value of the given type *)
Fixpoint choose_val T :=
  match T with
  | Unit => ()
  | T0 <+> T1 => lef T1 (choose_val T0)
  | T1 <*> T2 => (| choose_val T1, choose_val T2 |)
  end.

Lemma choose_val_correct :
  forall T, val (choose_val T) T.
Proof.
  intros T. induction T; simpl; auto using val.
Qed.

