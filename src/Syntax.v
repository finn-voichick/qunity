From Coq Require Import
  Basics List Bool String Reals FinFun RelationClasses FunctionalExtensionality
  Psatz.
From PLF Require Import Typechecking.
From VFA Require Import Perm.
From Qunity Require Import ListSetAux RealAux MapsAux.
Import ListNotations List (length).

Set Implicit Arguments.

Open Scope list_scope.
Open Scope string_scope.
Open Scope bool_scope.
Open Scope type_scope.

(* A set of variables *)
Definition var_set := set string.

(* A convenient definition for set_add_bool *)
Definition var_set_add : string -> var_set -> var_set :=
  set_add_bool eqb_string.

(* A convenient definition for set_mem_bool *)
Definition var_set_mem : string -> var_set -> bool :=
  set_mem_bool eqb_string.

(* A convenient definition for set_union_bool *)
Definition var_set_union : var_set -> var_set -> var_set :=
  set_union_bool eqb_string.

(* A convenient definition for set_diff_bool *)
Definition var_set_diff : var_set -> var_set -> var_set :=
  set_diff_bool eqb_string.

(* A convenient definition for subset_bool *)
Definition var_subset : var_set -> var_set -> bool :=
  subset_bool eqb_string.

(* A convenient definition for eqb_set *)
Definition eqb_var_set : var_set -> var_set -> bool :=
  eqb_set eqb_string.

(* A convenient definition for disjoint_bool *)
Definition var_set_disjoint : var_set -> var_set -> bool :=
  disjoint_bool eqb_string.

(* A convenient definition for big_union *)
Definition big_var_set_union {A} : (A -> var_set) -> set A -> var_set :=
  big_union eqb_string.

#[export]
Hint Unfold
  var_set_add var_set_mem var_set_union var_set_diff var_subset eqb_var_set
  var_set_disjoint big_var_set_union :
  core.

Lemma var_set_mem_spec :
  forall s x, reflect (set_In x s) (var_set_mem x s).
Proof.
  unfold var_set_mem. eauto with bdestruct.
Qed.

Lemma var_set_union_spec :
  forall s s' e, set_In e (var_set_union s s') <-> set_In e s \/ set_In e s'.
Proof.
  intros s s' e. unfold var_set_union.
  apply set_union_bool_all_spec, eqb_string_spec.
Qed.

Lemma var_set_diff_spec :
  forall s s' e, set_In e (var_set_diff s s') <-> set_In e s /\ ~ set_In e s'.
Proof.
  intros s s' e. unfold var_set_diff.
  apply set_diff_bool_all_spec, eqb_string_spec.
Qed.

Lemma var_set_mem_var_set_diff :
  forall s s' e,
  var_set_mem e (var_set_diff s s') =
  var_set_mem e s && negb (var_set_mem e s').
Proof.
  intros s s' e. unfold var_set_mem, var_set_diff.
  apply set_mem_set_diff_all, eqb_string_spec.
Qed.

Lemma var_subset_spec :
  forall s s', reflect (subset s s') (var_subset s s').
Proof.
  intros s s'. unfold var_subset. apply subset_bool_all_spec, eqb_string_spec.
Qed.

Lemma eqb_var_set_spec :
  forall s s', reflect (eq_set s s') (eqb_var_set s s').
Proof.
  intros s s'. unfold eqb_var_set. apply eqb_set_all_spec, eqb_string_spec.
Qed.

Lemma var_set_disjoint_spec :
  forall s s', reflect (disjoint s s') (var_set_disjoint s s').
Proof.
  intros s s'. unfold var_set_disjoint.
  apply disjoint_bool_all_spec, eqb_string_spec.
Qed.

Lemma big_var_set_union_spec :
  forall A f e' (s : set A),
  set_In e' (big_var_set_union f s) <-> Exists (fun e => set_In e' (f e)) s.
Proof.
  intros A f e' s. unfold big_var_set_union.
  apply big_union_all_spec, eqb_string_spec.
Qed.

(* No function types, just unit, sum types, and product types *)
Inductive type :=
  | type_unit
  | type_sum (T0 T1 : type)
  | type_prod (T1 T2 : type).

Declare Scope qunit_scope.

Notation "'Unit'" := type_unit : qunit_scope.
Infix "<+>" := type_sum (at level 50) : qunit_scope.
Infix "<*>" := type_prod (at level 40) : qunit_scope.

Open Scope qunit_scope.

Notation "'Bit'" := (Unit <+> Unit) : qunit_scope.

(* true if T = T', otherwise false *)
Fixpoint eqb_type T T' :=
  match T, T' with
  | Unit, Unit => true
  | T0 <+> T1, T0' <+> T1' => eqb_type T0 T0' && eqb_type T1 T1'
  | T1 <*> T2, T1' <*> T2' => eqb_type T1 T1' && eqb_type T2 T2'
  | _, _ => false
  end.

Infix "=?" := eqb_type : qunit_scope.

Lemma eqb_type_spec :
  forall T T', reflect (T = T') (T =? T').
Proof.
  intros T.
  induction T as [| T0 IH0 T1 IH1 | T1 IH1 T2 IH2 ];
  intros [| T0' T1' | T1' T2']; simpl;
  try bdestruct (T0 =? T0');
  try bdestruct (T1 =? T1');
  try bdestruct (T2 =? T2');
  constructor; congruence.
Qed.

Lemma eqb_type_refl :
  forall T, T =? T = true.
Proof.
  intros T; induction T; simpl; auto with bool.
Qed.

Inductive gate := gate_u (theta phi lambda : R).

(* true if the gates are equal, otherwise false *)
Definition eqb_gate '(gate_u theta phi lambda) '(gate_u theta' phi' lambda') :=
  eqb_r theta theta' && eqb_r phi phi' && eqb_r lambda lambda'.

Lemma eqb_gate_spec :
  forall g g', reflect (g = g') (eqb_gate g g').
Proof.
  intros [theta phi lambda] [theta' phi' lambda']; simpl.
  bdestruct (eqb_r theta theta');
  bdestruct (eqb_r phi phi');
  bdestruct (eqb_r lambda lambda');
  constructor; congruence.
Qed.

#[export]
Hint Resolve
  eqb_string_spec
  var_set_mem_spec var_subset_spec eqb_var_set_spec var_set_disjoint_spec
  eqb_type_spec eqb_gate_spec :
  bdestruct.

Lemma eqb_gate_refl :
  forall g, eqb_gate g g = true.
Proof.
  intros g. bdestruct (eqb_gate g g); congruence.
Qed.

(* No functions, just expressions *)
Inductive expr :=
  | expr_unit
  | expr_var (x : string)
  | expr_left (T : type) (e : expr)
  | expr_right (T : type) (e : expr)
  | expr_pair (e1 e2 : expr)
  | expr_match (e : expr) (l : set pat_match)
  | expr_case (e : expr) (l : set pat_match)
  | expr_gphase (e : expr) (gamma : R)
  | expr_gate (e : expr) (g : gate)
  | expr_oracle (e1 e2 e3 : expr)
with pat_match := pat_arrow (p e : expr).

Notation "()" := expr_unit : qunit_scope.
Coercion expr_var : string >-> expr.
Notation "'lef'" := expr_left (at level 0) : qunit_scope.
Notation "'rit'" := expr_right (at level 0) : qunit_scope.
Notation "(| x , y , .. , z |)" :=
    (expr_pair .. (expr_pair x y) .. z) : qunit_scope.
Notation "'qmatch' e 'with' | m0 | m1 | .. | mn" :=
    (expr_match e (cons m0 (cons m1 .. (cons mn nil) ..))) (at level 10) :
    qunit_scope.
Notation "'case' e 'of' | m1 | m2 | .. | mn" :=
  (expr_case e (cons m1 (cons m2 .. (cons mn nil) .. ))) (at level 10) :
  qunit_scope.
Notation "e |> 'gphase' gamma" :=
    (expr_gphase e gamma) (at level 20) : qunit_scope.
Notation "e |> g" := (expr_gate e g) (at level 20) : qunit_scope.
Notation "'oracle' ( e1 , e2 (+) e3 )" := (expr_oracle e1 e2 e3).

Notation "p =>> e" := (pat_arrow p e) (at level 2).

Notation "#0" := (lef Unit ()) : qunit_scope.
Notation "#1" := (rit Unit ()) : qunit_scope.
Notation "'assert' e' == v 'in' e" :=
  (expr_case e' [v =>> e]) (at level 10) : qunit_scope.
Notation "'qlet' p := e 'in' e'" :=
  (expr_case e [p =>> e']) (at level 10) : qunit_scope.
Notation "\ p ->> e'" :=
    (fun e => qlet p := e in e') (at level 90, e' at level 99) : qunit_scope.

(* An induction principle that uses Forall *)
Fixpoint expr_ind'
  (P : expr -> Prop)
  (f_unit : P ())
  (f_var : forall x : string, P x)
  (f_left : forall T e, P e -> P (lef T e))
  (f_right : forall T e, P e -> P (rit T e))
  (f_pair : forall e1, P e1 -> forall e2, P e2 -> P (|e1, e2|))
  (f_match :
   forall e, P e ->
   forall l,
   Forall (fun '(p =>> _) => P p) l ->
   Forall (fun '(_ =>> e') => P e') l ->
   P (expr_match e l))
  (f_case :
   forall e, P e ->
   forall l,
   Forall (fun '(p =>> _) => P p) l ->
   Forall (fun '(_ =>> e') => P e') l ->
   P (expr_case e l))
  (f_gphase : forall e, P e -> forall gamma, P (e |> gphase gamma))
  (f_gate : forall e, P e -> forall g, P (e |> g))
  (f_oracle :
   forall e1, P e1 ->
   forall e2, P e2 ->
   forall e3, P e3 ->
   P (oracle (e1, e2 (+) e3)))
  (e : expr) :
  P e.
Proof.
  destruct e; auto.
  - apply f_left, expr_ind'; assumption.
  - apply f_right, expr_ind'; assumption.
  - apply f_pair; apply expr_ind'; assumption.
  - apply f_match; try (apply expr_ind'; assumption);
    induction l as [| [p e'] l IH]; constructor; auto;
    apply expr_ind'; assumption.
  - apply f_case; try (apply expr_ind'; assumption);
    induction l as [| [p e'] l IH]; constructor; auto;
    apply expr_ind'; assumption.
  - apply f_gphase, expr_ind'; assumption.
  - apply f_gate, expr_ind'; assumption.
  - apply f_oracle; apply expr_ind'; assumption.
Qed.

(* true if e = e', otherwise false *)
Fixpoint eqb_expr e e' :=
  match e, e' with
  | (), () => true
  | expr_var x, expr_var x' => eqb_string x x'
  | lef T e, lef T' e' => (T =? T') && eqb_expr e e'
  | rit T e, rit T' e' => (T =? T') && eqb_expr e e'
  | (| e1, e2 |), (| e1', e2' |)  => eqb_expr e1 e1' && eqb_expr e2 e2'
  | expr_match e l, expr_match e' l' =>
      eqb_expr e e' && eqb_list eqb_pat_match l l'
  | expr_case e l, expr_case e' l' =>
      eqb_expr e e' && eqb_list eqb_pat_match l l'
  | e |> gphase gamma, e' |> gphase gamma' =>
      eqb_expr e e' && eqb_r gamma gamma'
  | e |> g, e' |> g' => eqb_expr e e' && eqb_gate g g'
  | oracle (e1, e2 (+) e3), oracle (e1', e2' (+) e3') =>
      eqb_expr e1 e1' && eqb_expr e2 e2' && eqb_expr e3 e3'
  | _, _ => false
  end
with eqb_pat_match m m' :=
  let '(p =>> e) := m in
  let '(p' =>> e') := m' in
  eqb_expr p p' && eqb_expr e e'.

Lemma eqb_expr_refl :
  forall e, eqb_expr e e = true.
Proof.
  intros e. induction e using expr_ind'; simpl; auto using eqb_string_refl.
  - rewrite eqb_type_refl. assumption.
  - rewrite eqb_type_refl. assumption.
  - rewrite IHe1. assumption.
  - rewrite IHe. simpl. induction H as [| [p e'] l Hp H IH]; auto.
    inversion_clear H0. apply IH in H2. simpl.
    rewrite Hp, H1. assumption.
  - rewrite IHe. simpl. induction H as [| [p e'] l Hp H IH]; auto.
    inversion_clear H0. apply IH in H2. simpl.
    rewrite Hp, H1. assumption.
  - rewrite IHe. apply eqb_r_refl.
  - rewrite IHe. apply eqb_gate_refl.
  - rewrite IHe1, IHe2. apply IHe3.
Qed.

Lemma eqb_expr_spec :
  forall e e', reflect (e = e') (eqb_expr e e').
Proof.
  intros e e'. apply iff_reflect.
  split; try (intros H; subst; apply eqb_expr_refl).
  generalize dependent e'.
  induction e using expr_ind'; simpl; intros [] H'; try discriminate.
  - reflexivity.
  - bdestruct (eqb_string x x0).
    + subst. reflexivity.
    + discriminate.
  - bdestruct (T =? T0); try discriminate.
    apply IHe in H'. subst. reflexivity.
  - bdestruct (T =? T0); try discriminate.
    apply IHe in H'. subst. reflexivity.
  - apply andb_prop in H' as [H1 H2].
    apply IHe1 in H1. apply IHe2 in H2. subst. reflexivity.
  - apply andb_prop in H' as [He Hl]. apply IHe in He. subst.
    replace l0 with l; auto.
    generalize dependent l0.
    induction l as [| [p1 e1] l1 IH];
    intros [| [p2 e2] l2]; try discriminate; auto.
    simpl. intros Hl.
    apply andb_prop in Hl as [Hp Hl]. apply andb_prop in Hp as [Hp He].
    inversion_clear H. apply H1 in Hp.
    inversion_clear H0. apply H in He.
    apply IH in Hl; auto. subst. reflexivity.
  - apply andb_prop in H' as [He Hl]. apply IHe in He. subst.
    replace l0 with l; auto.
    generalize dependent l0.
    induction l as [| [p1 e1] l1 IH];
    intros [| [p2 e2] l2]; try discriminate; auto.
    simpl. intros Hl.
    apply andb_prop in Hl as [Hp Hl]. apply andb_prop in Hp as [Hp He].
    inversion_clear H. apply H1 in Hp.
    inversion_clear H0. apply H in He.
    apply IH in Hl; auto. subst. reflexivity.
  - apply andb_prop in H' as [He Hg].
    apply IHe in He. bdestruct (eqb_r gamma gamma0); try discriminate.
    subst. reflexivity.
  - apply andb_prop in H' as [He Hg].
    apply IHe in He. bdestruct (eqb_gate g g0); try discriminate.
    subst. reflexivity.
  - apply andb_prop in H' as [H1 H3]. apply andb_prop in H1 as [H1 H2].
    apply IHe1 in H1. apply IHe2 in H2. apply IHe3 in H3. subst. reflexivity.
Qed.

#[export]
Hint Resolve eqb_expr_spec : bdestruct.

Lemma eqb_pat_match_spec :
  forall (m m' : pat_match),
  reflect (m = m') (eqb_pat_match m m').
Proof.
  intros [p e] [p' e']. simpl.
  bdestruct (eqb_expr p p'); bdestruct (eqb_expr e e');
  subst; constructor; congruence.
Qed.

#[export]
Hint Resolve eqb_pat_match_spec : bdestruct.

(* Convert a Coq pair to a pair expression *)
Definition of_pair '(e1, e2) := (| e1, e2 |).
