From Coq Require Export Reals.
From Coq Require Import Bool List Psatz RelationClasses.
From VFA Require Import Perm.
From Qunity Require Import BoolAux ListAux.
Import ListNotations.

Set Implicit Arguments.

(* Some additional property involving Reals *)

Open Scope R_scope.

(* Real equality as a bool *)
Definition eqb_r a a' := if Req_appart_dec a a' then true else false.

Lemma eqb_r_spec :
  reflect2 eq eqb_r.
Proof.
  intros a a'. unfold eqb_r.
  destruct (Req_appart_dec a a'); constructor; lra.
Qed.

#[export]
Hint Resolve eqb_r_spec : bdestruct.

Lemma eqb_r_refl :
  forall a, eqb_r a a = true.
Proof.
  intros a. bdestruct (eqb_r a a); congruence.
Qed.

Lemma Rmult_neg_1 :
  forall a, -1 * a = -a.
Proof.
  intros a. lra.
Qed.

Lemma Ropp_0_iff :
  forall a, a = - a <-> a = 0.
Proof.
  intros a. lra.
Qed.

Instance Rge_Reflexive :
  Reflexive Rge.
Proof.
  intros a. apply Rge_refl.
Qed.

Instance Rge_Transitive :
  Transitive Rge.
Proof.
  intros a b c Ha Hc. lra.
Qed.

Instance Rle_Reflexive :
  Reflexive Rle.
Proof.
  intros a. lra.
Qed.

Instance Rle_Transitive :
  Transitive Rle.
Proof.
  intros a b c Ha Hc. lra.
Qed.

Hint Rewrite <-
  plus_IZR mult_IZR Rplus_assoc IZR_NEG 
  Ropp_mult_distr_l Ropp_mult_distr_r :
  rsimpl.
Hint Rewrite
  Ropp_0 Ropp_0_iff Rmult_1_r Rmult_1_l Z_R_minus Ropp_involutive
  Rplus_opp_l Rplus_opp_r Rplus_0_l Rplus_0_r Ropp_plus_distr
  Rmult_neg_1 Rsqr_0 Rsqr_1 :
  rsimpl.

(* Simplify expressions involving real numbers *)
Ltac rsimpl :=
  repeat (autorewrite with rsimpl in *;
          try unfold Rminus;
          simpl in *;
          try easy).

(* Sum over f(e) for each e in b *)
Fixpoint rsum {A} f (b : set A) :=
  match b with
  | [] => 0
  | e :: b' => f e + rsum f b'
  end.

Lemma rsum_0 :
  forall A f (b : set A), Forall (fun e => f e = 0) b -> rsum f b = 0.
Proof.
  intros A f b H. induction H; rsimpl. lra.
Qed.

Lemma rsum_ge_0 :
  forall A f (b : set A), Forall (fun e => 0 <= f e) b -> 0 <= rsum f b.
Proof.
  intros A f b H. induction H; rsimpl. lra.
Qed.

Lemma rsum_0_Forall :
  forall A f (s : set A),
  Forall (fun e => 0 <= f e) s ->
  rsum f s = 0 -> Forall (fun e => f e = 0) s.
Proof.
  intros A f s H Hs.
  induction H as [| e s He H IH]; constructor; simpl in Hs;
  apply Rplus_eq_R0 in Hs as [H0 Hs]; auto using rsum_ge_0.
Qed.
