{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/refs/tags/21.05.tar.gz") {} }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    coq
    coqPackages.coquelicot
    (vim_configurable.customize {
      name = "vim";
      vimrcConfig = {
        customRC = ''
          set colorcolumn=81
          set backspace=indent,eol,start
          set mouse=a
        '';
        vam = {
          knownPlugins = pkgs.vimPlugins;
          pluginDictionaries = [
            { names = [ "Coqtail" ]; }
          ];
        };
      };
    })
  ];
}
