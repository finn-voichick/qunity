# Qunity: A Programming Language to Unify Quantum and Classical Programming

This project depends on Coq and Coquelicot.
With those installed, compiling should be as simple as running `make` in the
root directory.
